<?php

namespace console\controllers;

use common\helpers\XpathHelper;
use console\helpers\LuximmoHelper;
use console\helpers\N1Helper;
use console\helpers\PrianHelper;
use console\helpers\UtilityHelper;
use console\helpers\YandexHelper;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Class ParseController
 * @package console\controllers
 */
class ParseController extends Controller
{
    /**
     * @param int $limit
     * @param null $page
     */
    public function actionN1($limit = 1, $page = null)
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/n1");
        $cities = include(Yii::getAlias("@console") . "/export/n1/cities.php");

        $client = new Client();
        $headers = N1Helper::$_headers;
        foreach ($cities as $domain => $cityTitle) {
            Console::output("Loading city '$cityTitle'");
            $cityAlias = explode('.', $domain)[0];
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/n1/{$cityAlias}");
            $dataFilePath = Yii::getAlias("@console") . "/export/n1/{$cityAlias}/property-data.php";
            $oldData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach (N1Helper::$_propertyActions as $action) {
                Console::output("... action '$action'");
                foreach (N1Helper::$_propertyCategories as $category) {
                    Console::output("...... category '$category'");
                    $currentData = !empty($oldData[$action][$category]) ? $oldData[$action][$category] : [];
                    if ($page === null) {
                        $lastObjectIndex = count($currentData) ? max(array_keys($currentData)) : 1;
                        if (isset($currentData[$lastObjectIndex]) && count($currentData[$lastObjectIndex]) >= 100) {
                            $currentPage = $lastObjectIndex + 1;
                        }
                        else {
                            $currentPage = $lastObjectIndex;
                        }
                    }
                    else {
                        $currentPage = $page;
                    }
                    $params = [
                        "page" => $currentPage,
                        "limit" => 100,
                    ];
                    $actionUrl = ($action === 'snyat' && in_array($category, ['kvartiry', 'doma'], true)) ? 'snyat/dolgosrochno' : $action;
                    $selector = N1Helper::getSelector($category);
                    $headers[':authority'] = $domain;
                    $headers[':path'] = "/$actionUrl/$category/?" . http_build_query($params);

                    $data = $client->get("https://$domain/$actionUrl/$category", $params, $headers)->send();
                    $data = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data->content);
                    if ($data) {
                        Console::output('......... page #' . $currentPage);
                        libxml_use_internal_errors(true);
                        $dom = new DOMDocument();
                        $dom->loadHTML($data);

                        $xpath = new DomXPath($dom);

                        /** @var DOMNodeList $nodes */
                        $nodes = $xpath->query(XpathHelper::cssToXpath("//div.$selector-list-card__location/div.card-title/a.link"));
                        if (!$nodes->length) {
//                            Console::output("Haha! You have been captchured!");
//                            Yii::error("https://$domain/$actionUrl/$category");
                            continue;
                        }

                        $result = [];
                        $i = 0;
                        $loaded = 0;
                        foreach ($nodes as $node) {
                            if (isset($currentData[$currentPage][$i])) {
                                Console::output("............ item #$i exist - skipping");
                            }
                            else {
                                Console::output("............ Item #$i - loading");

                                /* @var DOMElement $node*/
                                $url = $node->getAttribute('href');
                                $title = '';

                                $titleNode = $node->getElementsByTagName('span');
                                if ($titleNode->length) {
                                    $title = $titleNode->item(0)->textContent;
                                }

                                $data = $client->get("https://$domain" . $url, [], array_replace($headers, [":path" => $url]))->send();
                                $data = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data->content);
                                if ($data) {
                                    $itemData = N1Helper::parseItem($data, $selector);
                                    if (!empty($itemData)) {
                                        $result[$action][$category][$currentPage][$i] = $itemData;
                                        $result[$action][$category][$currentPage][$i]['url'] = "https://$domain" . $url;
                                        $result[$action][$category][$currentPage][$i]['title'] = $title;
                                        $result[$action][$category][$currentPage][$i]['city'] = $cityTitle;
                                        $loaded++;
                                        if ($loaded >= $limit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            $i++;
                        }
                        $oldData = array_replace_recursive($oldData, $result);
                    }
                }
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($oldData) . ";");
        }
    }

    /**
     * @param int $limit
     * @param null $page
     */
    public function actionPrian($limit = 1, $page = null)
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/prian");
        $countries = include(Yii::getAlias("@console") . "/export/prian/countries.php");
        $lastCountryPath = Yii::getAlias("@console") . "/export/prian/last-country-id.php";
        $lastCountryId = file_exists($lastCountryPath) ? include($lastCountryPath) : -1;
        $countryId = $lastCountryId + 1;
        if ($countryId >= count($countries)) {
            $countryId = 0;
        }
        file_put_contents($lastCountryPath, "<?php \n return {$countryId};");

        if (isset($countries[$countryId])) {
            $client = new Client();
            $headers = PrianHelper::$_headers;
            $country = $countries[$countryId];
            if (empty($country['cities'])) {
                $country['cities'] = [
                    [
                        'url' => $country['url'],
                        'title' => 'Default',
                        'alias' => 'default'
                    ]
                ];
            }
            $countryAlias = $country['alias'];
            Console::output("Loading country '{$country['title']}'");
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/prian/{$countryAlias}");
            $dataFilePath = Yii::getAlias("@console") . "/export/prian/{$countryAlias}/property-data.php";
            $oldData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach($country['cities'] as $city){
                $cityAlias = $city['alias'];
                Console::output("... city '{$city['title']}'");
                foreach (PrianHelper::$_propertyActions as $action) {
                    Console::output("...... action '$action'");
                    foreach (PrianHelper::$_propertyCategories as $category) {
                        Console::output("......... category '$category'");
                        $currentData = !empty($oldData[$cityAlias][$action][$category]) ? $oldData[$cityAlias][$action][$category] : [];
                        if ($page === null) {
                            $lastObjectIndex = count($currentData) ? max(array_keys($currentData)) : 1;
                            if (isset($currentData[$lastObjectIndex]) && count($currentData[$lastObjectIndex]) >= 16) {
                                $currentPage = $lastObjectIndex + 1;
                            }
                            else {
                                $currentPage = $lastObjectIndex;
                            }
                        }
                        else {
                            $currentPage = $page;
                        }
                        $params = [
                            "next" => ($currentPage - 1) * 16,
                            "orderby" => 5,
                            "order" => 1,
                            'search_currency' => 3
                        ];
                        $actionUrl = $action === 'buy' ? "" : "$action/";
                        $url = "{$city['url']}{$actionUrl}{$category}/";
                        $headers[':path'] = preg_replace("/^https:\/\/prian\.ru/", "", "$url?" . http_build_query($params));

                        $data = $client->get($url, $params, $headers)->send();
                        $data = $data->content;
                        if ($data) {
                            Console::output('............ page #' . $currentPage);
                            libxml_use_internal_errors(true);
                            $dom = new DOMDocument();
                            $dom->loadHTML($data);

                            $xpath = new DomXPath($dom);

                            /** @var DOMNodeList $pageTitleNode */
                            $pageTitleNode = $xpath->query(XpathHelper::cssToXpath("//div.b-object-min/div.b-sort[1]/div.b-sort__total"));
                            if (!$pageTitleNode->length) {
//                            Console::output("Haha! You have been captchured!");
//                            Yii::error("https://$domain/$actionUrl/$category");
                                continue;
                            }
                            $pageTitle = $pageTitleNode->item(0)->textContent;
                            if (!preg_match("/Найдено \d*/", $pageTitle)) {
                                continue;
                            }

                            $nodes = $xpath->query(XpathHelper::cssToXpath("//div.b-object-min/div.list[1]/div.list_object.list_inline.list_object_xs/a"));

                            $result = [];
                            $i = 0;
                            $loaded = 0;
                            foreach ($nodes as $node) {
                                if (isset($currentData[$currentPage][$i])) {
                                    Console::output("............... item #$i exist - skipping");
                                }
                                else {
                                    Console::output("............... Item #$i - loading");

                                    /* @var DOMElement $node*/
                                    $itemUrl = "https:" . $node->getAttribute('href');
                                    $title = $node->getAttribute('title');
                                    $itemPath = preg_replace("/^\/\/prian\.ru/", "", $node->getAttribute('href'));

                                    $data = $client->get($itemUrl, ['search_currency' => 3], array_replace($headers, [":path" => $itemPath]))->send();
                                    $data = $data->content;
                                    if ($data) {
                                        $itemData = PrianHelper::parseItem($data);
                                        if (!empty($itemData)) {
                                            $result[$cityAlias][$action][$category][$currentPage][$i] = $itemData;
                                            $result[$cityAlias][$action][$category][$currentPage][$i]['url'] = $itemUrl;
                                            $result[$cityAlias][$action][$category][$currentPage][$i]['title'] = $title;
                                            $result[$cityAlias][$action][$category][$currentPage][$i]['city'] = $city['title'];
                                            $loaded++;
                                            if ($loaded >= $limit) {
                                                break;
                                            }
                                        }
                                    }
                                }
                                $i++;
                            }
                            $oldData = array_replace_recursive($oldData, $result);
                        }
                    }
                }
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($oldData) . ";");
        }
    }

    /**
     *
     */
    public function actionPrianCities()
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/prian");
        $dataFilePath = Yii::getAlias("@console") . "/export/prian/countries.php";
        $result = [];
        $client = new Client();
        $headers = PrianHelper::$_headers;
        $data = $client->get("https://prian\.ru/", [], $headers)->send();
        $data = $data->content;
        if ($data) {
            libxml_use_internal_errors(true);
            $dom = new DOMDocument();
            $dom->loadHTML($data);

            $xpath = new DomXPath($dom);

            /** @var DOMNodeList $nodes */
            $nodes = $xpath->query(XpathHelper::cssToXpath("//div.list-country/div.row/dl/dd/a"));
            if (!$nodes->length) {
                return;
            }

            foreach ($nodes as $node) {
                $item = [
                    'cities' => []
                ];

                /* @var DOMElement $node*/
                $path = preg_replace("/^\/\/prian\.ru/", "", $node->getAttribute('href'));
                $item['url'] = "https:" . $node->getAttribute('href');
                $item['title'] = preg_replace("/\s\(\d*\)/", "", $node->textContent);
                $item['alias'] = preg_replace("/^.*\/([^()\/]*)\/$/", "$1", $node->getAttribute('href'));

                $data = $client->get($item['url'], [], array_replace($headers, [":path" => $path]))->send();
                $data = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data->content);
                if ($data) {
                    $dom2 = new DOMDocument();
                    $dom2->loadHTML($data);

                    $xpath2 = new DomXPath($dom2);

                    /** @var DOMNodeList $nodes */
                    $nodes2 = $xpath2->query(XpathHelper::cssToXpath("//div#bl4/div.b-hide-bl/div#bl4_2/dl.list_link/dd/a"));
                    foreach ($nodes2 as $node2) {
                        /* @var DOMElement $node2*/
                        $item['cities'][] = [
                            'url' => "https:" . $node2->getAttribute('href'),
                            'title' => preg_replace("/\s\(\d*\)/", "", $node2->textContent),
                            'alias' => preg_replace("/^.*\/([^()\/]*)\/$/", "$1", $node2->getAttribute('href'))
                        ];
                    }
                }
                $result[] = $item;
            }
        }
        file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($result) . ";");
    }

    /**
     *
     */
    public function actionPrianFix()
    {
        $countries = FileHelper::findDirectories(Yii::getAlias("@console") . "/export/prian", ['recursive' => false]);
        foreach ($countries as $countryDir) {
            $country = basename($countryDir);
            Console::output("Fixing country '{$country}'");
            $dataFilePath = Yii::getAlias("@console") . "/export/prian/{$country}/property-data.php";
            $countryData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach ($countryData as $cityAlias => $cityData) {
                foreach ($cityData as $actionAlias => $actionData) {
                    foreach ($actionData as $categoryAlias => $categoryData) {
                        foreach ($categoryData as $page => $pageData) {
                            foreach ($pageData as $itemId => $item) {
                                if (!empty($item['lat']) && !empty($item['lng']) && (strlen($item['lat']) > 12 || strlen($item['lng']) > 12)) {
                                    Console::output("Haha!");
                                    $countryData[$cityAlias][$actionAlias][$categoryAlias][$page][$itemId]['lat'] = null;
                                    $countryData[$cityAlias][$actionAlias][$categoryAlias][$page][$itemId]['lng'] = null;
                                }
                            }
                        }
                    }
                }
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($countryData) . ";");
        }
    }

    /**
     * @param int $limit
     * @param null $page
     */
    public function actionLuximmo($limit = 1, $page = null)
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/luximmo");
        $client = new Client();
        $countries = LuximmoHelper::$_countries;
        $headers = LuximmoHelper::$_headers;

        $defaultParams = [
            "orderby" => 5,
            "srtby" => 5,
            'spredlog' => 'innear',
            'action' => 1,
            'cmd_form' => 'search',
            'searchform' => 1,
        ];
        foreach ($countries as $countryId => $country){
            $params['country_id'] = $countryId;
            Console::output("Loading country '{$country}'");
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/luximmo/{$country}");
            $dataFilePath = Yii::getAlias("@console") . "/export/luximmo/{$country}/property-data.php";
            $oldData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach (LuximmoHelper::$_propertyActions as $action => $actionFilters) {
                Console::output("... action '$action'");
                foreach (LuximmoHelper::$_propertyCategories as $category => $categoryFilters) {
                    Console::output("...... category '$category'");
                    $currentData = !empty($oldData[$action][$category]) ? $oldData[$action][$category] : [];
                    if ($page === null) {
                        $lastObjectIndex = count($currentData) ? max(array_keys($currentData)) : 1;
                        if (isset($currentData[$lastObjectIndex]) && count($currentData[$lastObjectIndex]) >= 15) {
                            $currentPage = $lastObjectIndex + 1;
                        }
                        else {
                            $currentPage = $lastObjectIndex;
                        }
                    }
                    else {
                        $currentPage = $page;
                    }
                    $params = array_merge(
                        $defaultParams,
                        ['country_id' => $countryId, "page" => $currentPage - 1],
                        $actionFilters,
                        $categoryFilters
                    );
                    $url = "https://www.luximmo.com/search/index.php";

                    $data = $client->get($url, $params, $headers)->send();
                    $data = mb_convert_encoding($data->content, 'utf-8', 'windows-1251');

                    if ($data) {
                        Console::output('......... page #' . $currentPage);
                        libxml_use_internal_errors(true);
                        $dom = new DOMDocument();
                        $dom->loadHTML($data);

                        $xpath = new DomXPath($dom);

                        $nodes = $xpath->query(XpathHelper::cssToXpath("//div.grid-container/div.grid-x/div.cell.large-cell-block-y/div.offer/a.offer-link"));

                        $result = [];
                        $i = 0;
                        $loaded = 0;
                        foreach ($nodes as $node) {
                            if (isset($currentData[$currentPage][$i])) {
                                Console::output("............ item #$i exist - skipping");
                            }
                            else {
                                Console::output("............ item #$i - loading");

                                /* @var DOMElement $node*/
                                $itemUrl = $node->getAttribute('href');
                                $data = $client->get($itemUrl, null, $headers)->send();
                                if (!$data->isOk) {
                                    Yii::error("\n Luximmo error url: " . $itemUrl . "\n");
                                }
                                $data = mb_convert_encoding($data->content, 'utf-8', 'windows-1251');
                                if ($data) {
                                    $itemData = LuximmoHelper::parseItem($data, $client, $itemUrl);
                                    if (!empty($itemData)) {
                                        $result[$action][$category][$currentPage][$i] = $itemData;
                                        $result[$action][$category][$currentPage][$i]['url'] = $itemUrl;
                                        $loaded++;
                                        if ($loaded >= $limit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            $i++;
                        }
                        $oldData = array_replace_recursive($oldData, $result);
                    }
                }
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($oldData) . ";");
        }
    }

    /**
     * @param int $limit
     * @param null $page
     */
    public function actionYandexBuildings($limit = 1, $page = null)
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex");
        $client = new Client();
        $cities = YandexHelper::$_cities;
        $headers = YandexHelper::$_headers;

        $defaultParams = [

        ];
        foreach ($cities as $city){
            Console::output("Loading city '{$city}'");
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex/{$city}");
            $dataFilePath = Yii::getAlias("@console") . "/export/yandex/{$city}/building-data.php";
            $oldData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            if ($page === null) {
                $lastObjectIndex = count($oldData) ? max(array_keys($oldData)) : 1;
                if (isset($oldData[$lastObjectIndex]) && count($oldData[$lastObjectIndex]) >= 12) {
                    $currentPage = $lastObjectIndex + 1;
                }
                else {
                    $currentPage = $lastObjectIndex;
                }
            }
            else {
                $currentPage = $page;
            }
            $params = array_merge(
                $defaultParams,
                ["page" => $currentPage - 1]
            );
            $url = "https://realty.yandex.ru/{$city}/kupit/novostrojka/";

            $data = $client->get($url, $params, $headers)->send();
            $data = $data->content;
            if ($data) {
                Console::output('... page #' . $currentPage);
                libxml_use_internal_errors(true);
                $dom = new DOMDocument();
                $dom->loadHTML($data);

                $xpath = new DomXPath($dom);

                $emptyNode = $xpath->query(XpathHelper::cssToXpath("//div.newbuilding-cap/div.newbuilding-cap__region-select"));
                if ($emptyNode->length) {
                    continue;
                }

                $nodes = $xpath->query(XpathHelper::cssToXpath("//div.newbuilding-results/div.newbuilding-results__row/div.newbuilding-results__cell/div.newbuilding-results-item/div.newbuilding-results-item__details/a.link"));

                $result = [];
                $i = 0;
                $loaded = 0;
                foreach ($nodes as $node) {
                    if (isset($oldData[$currentPage][$i])) {
                        Console::output("...... item #$i exist - skipping");
                    }
                    else {
                        Console::output("...... item #$i - loading");

                        /* @var DOMElement $node*/
                        $itemUrl = $node->getAttribute('href');
                        $data = $client->get("https://realty.yandex.ru$itemUrl", null, $headers)->send();
                        if (!$data->isOk) {
                            Yii::error("\n Yandex error url: " . $itemUrl . "\n");
                        }
                        $data = $data->content;
                        if ($data) {
                            $itemData = YandexHelper::parseBuilding($data, $client);
                            if (!empty($itemData)) {
                                $result[$currentPage][$i] = $itemData;
                                $result[$currentPage][$i]['url'] = $itemUrl;
                                $loaded++;
                                if ($loaded >= $limit) {
                                    break;
                                }
                            }
                        }
                    }
                    $i++;
                }
                $oldData = array_replace_recursive($oldData, $result);
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($oldData) . ";");
        }
    }

    /**
     * @param int $limit
     */
    public function actionYandexProperties($limit = 1)
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex");
        $client = new Client();
        $headers = YandexHelper::$_headers;

        libxml_use_internal_errors(true);
        foreach (YandexHelper::$_cities as $city){
            Console::output("Loading city '{$city}'");
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex/{$city}");
            $buildingFilePath = Yii::getAlias("@console") . "/export/yandex/{$city}/building-data.php";
            $buildingData = file_exists($buildingFilePath) ? include($buildingFilePath) : [];

            $dataFilePath = Yii::getAlias("@console") . "/export/yandex/{$city}/property-data.php";
            $propertyData = file_exists($dataFilePath) ? include($dataFilePath) : [];

            $nextBuildingPath = Yii::getAlias("@console") . "/export/yandex/{$city}/next-building.php";
            $nextSuccessfulBuilding = $nextBuilding = file_exists($nextBuildingPath) ? include($nextBuildingPath) : ['page' => 1, 'building' => 0];

            for ($i = 0; $i < $limit; $i++) {
                if (empty($buildingData[$nextBuilding['page']][$nextBuilding['building']]['propertyLinks']) || count($buildingData[$nextBuilding['page']][$nextBuilding['building']]['propertyLinks']) === count($propertyData[$nextBuilding['page']][$nextBuilding['building']] ?? [])) {
                    Console::output("... end of building");
                    $nextBuilding['building']++;
                }
                if (!isset($buildingData[$nextBuilding['page']][$nextBuilding['building']])) {
                    Console::output("... end of page");
                    $nextBuilding['page']++;
                    $nextBuilding['building'] = 0;
                }
                if (!isset($buildingData[$nextBuilding['page']])) {
                    Console::output("... no more data");
                    break;
                }
                $nextProperty = count($propertyData[$nextBuilding['page']][$nextBuilding['building']] ?? []);
                Console::output("... loading page #{$nextBuilding['page']} building #{$nextBuilding['building']} property #{$nextProperty}");
                if (empty($buildingData[$nextBuilding['page']][$nextBuilding['building']]['propertyLinks'][$nextProperty])) {
                    $i--;
                    continue;
                }
                $nextSuccessfulBuilding = $nextBuilding;
                $url = $buildingData[$nextBuilding['page']][$nextBuilding['building']]['propertyLinks'][$nextProperty];

                $data = $client->get($url, null, $headers)->send();
                $data = $data->content;

                if ($data) {
                    $itemData = YandexHelper::parseProperty($data, $buildingData[$nextBuilding['page']][$nextBuilding['building']]);
                    if (!empty($itemData)) {
                        $propertyData[$nextBuilding['page']][$nextBuilding['building']][$nextProperty] = $itemData;
                        $propertyData[$nextBuilding['page']][$nextBuilding['building']][$nextProperty]['url'] = $url;
                    }
                }
            }
            file_put_contents($nextBuildingPath, "<?php \n return " . VarDumper::export($nextSuccessfulBuilding) . ";");
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($propertyData) . ";");
        }
    }

    /**
     *
     */
    public function actionYandexProperties2()
    {
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export");
        UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex");
        $client = new Client();
        $headers = YandexHelper::$_headers;

        libxml_use_internal_errors(true);
        foreach (YandexHelper::$_cities as $city){
            Console::output("Loading city '{$city}'");
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/export/yandex/{$city}");
            $buildingFilePath = Yii::getAlias("@console") . "/export/yandex/{$city}/building-data.php";
            $buildingsData = file_exists($buildingFilePath) ? include($buildingFilePath) : [];

            $dataFilePath = Yii::getAlias("@console") . "/export/yandex/{$city}/property-data.php";
            $propertyData = file_exists($dataFilePath) ? include($dataFilePath) : [];

            foreach ($buildingsData as $page => $pageData) {
                foreach ($pageData as $building => $buildingData) {
                    $nextProperty = count($propertyData[$page][$building] ?? []);
                    if (!empty($buildingData['propertyLinks'][$nextProperty])) {
                        Console::output("... loading page #{$page} building #{$building} property #{$nextProperty}");

                        $url = $buildingData['propertyLinks'][$nextProperty];

                        $data = $client->get($url, null, $headers)->send();
                        $data = $data->content;

                        if ($data) {
                            $itemData = YandexHelper::parseProperty($data, $buildingData);
                            if (!empty($itemData)) {
                                $propertyData[$page][$building][$nextProperty] = $itemData;
                                $propertyData[$page][$building][$nextProperty]['url'] = $url;
                            }
                        }
                    }
                }
            }
            file_put_contents($dataFilePath, "<?php \n return " . VarDumper::export($propertyData) . ";");
        }
    }
}