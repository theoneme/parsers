<?php
 return [
    [
        'cities' => [],
        'url' => 'https://prian.ru/abkhazia/',
        'title' => 'Абхазия',
        'alias' => 'abkhazia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/australia/new-south-wales/canberra/',
                'title' => 'Канберра',
                'alias' => 'canberra',
            ],
            [
                'url' => 'https://prian.ru/australia/victoria/melbourne/',
                'title' => 'Мельбурн',
                'alias' => 'melbourne',
            ],
            [
                'url' => 'https://prian.ru/australia/new-south-wales/sydney/',
                'title' => 'Сидней',
                'alias' => 'sydney',
            ],
        ],
        'url' => 'https://prian.ru/australia/',
        'title' => 'Австралия',
        'alias' => 'australia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/austria/austria/berndorf-bei-salzburg/',
                'title' => 'Берндорф-бай-Зальцбург',
                'alias' => 'berndorf-bei-salzburg',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/bruck-an-der-mur/',
                'title' => 'Брукк-ан-дер-Мур',
                'alias' => 'bruck-an-der-mur',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/burgenland/',
                'title' => 'Бургенланд',
                'alias' => 'burgenland',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/vienna/',
                'title' => 'Вена',
                'alias' => 'vienna',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/gosau/',
                'title' => 'Гозау',
                'alias' => 'gosau',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/graz/',
                'title' => 'Грац',
                'alias' => 'graz',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/salzburg/',
                'title' => 'Зальцбург',
                'alias' => 'salzburg',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/innsbruck/',
                'title' => 'Инсбрук',
                'alias' => 'innsbruck',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/carinthia/',
                'title' => 'Каринтия',
                'alias' => 'carinthia',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/kitzbuhel/',
                'title' => 'Кицбюэль',
                'alias' => 'kitzbuhel',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/linz/',
                'title' => 'Линц',
                'alias' => 'linz',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/lower-austria/',
                'title' => 'Нижняя Австрия',
                'alias' => 'lower-austria',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/obertauern/',
                'title' => 'Обертауерн',
                'alias' => 'obertauern',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/tyrol/',
                'title' => 'Тироль',
                'alias' => 'tyrol',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/vorarlberg/',
                'title' => 'Форарльберг',
                'alias' => 'vorarlberg',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/hermagor/',
                'title' => 'Хермагор',
                'alias' => 'hermagor',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/hollersbach-im-pinzgau/',
                'title' => 'Холлерсбах-им-Пинцгау',
                'alias' => 'hollersbach-im-pinzgau',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/zell-am-see/',
                'title' => 'Целль-ам-Зе',
                'alias' => 'zell-am-see',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/styria/',
                'title' => 'Штирия',
                'alias' => 'styria',
            ],
            [
                'url' => 'https://prian.ru/austria/austria/eberstalzell/',
                'title' => 'Эберштальцелль',
                'alias' => 'eberstalzell',
            ],
        ],
        'url' => 'https://prian.ru/austria/',
        'title' => 'Австрия',
        'alias' => 'austria',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/azerbaijan/azerbaijan/baku/',
                'title' => 'Баку',
                'alias' => 'baku',
            ],
        ],
        'url' => 'https://prian.ru/azerbaijan/',
        'title' => 'Азербайджан',
        'alias' => 'azerbaijan',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/albania/albania/vlore/',
                'title' => 'Влёра',
                'alias' => 'vlore',
            ],
            [
                'url' => 'https://prian.ru/albania/albania/durres/',
                'title' => 'Дуррес',
                'alias' => 'durres',
            ],
            [
                'url' => 'https://prian.ru/albania/albania/saranda/',
                'title' => 'Саранда',
                'alias' => 'saranda',
            ],
        ],
        'url' => 'https://prian.ru/albania/',
        'title' => 'Албания',
        'alias' => 'albania',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/belorussia/',
        'title' => 'Беларусь',
        'alias' => 'belorussia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/belgium/vlaanderen/antwerpen/',
                'title' => 'Антверпен',
                'alias' => 'antwerpen',
            ],
            [
                'url' => 'https://prian.ru/belgium/vlaanderen/brugge/',
                'title' => 'Брюгге',
                'alias' => 'brugge',
            ],
            [
                'url' => 'https://prian.ru/belgium/bruxelles/bruxelles/',
                'title' => 'Брюссель',
                'alias' => 'bruxelles',
            ],
            [
                'url' => 'https://prian.ru/belgium/vlaanderen/gent/',
                'title' => 'Гент',
                'alias' => 'gent',
            ],
        ],
        'url' => 'https://prian.ru/belgium/',
        'title' => 'Бельгия',
        'alias' => 'belgium',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/albena/',
                'title' => 'Албена',
                'alias' => 'albena',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/aheloy/',
                'title' => 'Ахелой',
                'alias' => 'aheloy',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/ahtopol/',
                'title' => 'Ахтопол',
                'alias' => 'ahtopol',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/balchik/',
                'title' => 'Балчик',
                'alias' => 'balchik',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/banevo/',
                'title' => 'Банево',
                'alias' => 'banevo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/bansko/',
                'title' => 'Банско',
                'alias' => 'bansko',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/bata/',
                'title' => 'Бата',
                'alias' => 'bata',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/bezvoditsa/',
                'title' => 'Безводица',
                'alias' => 'bezvoditsa',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/benkovski/',
                'title' => 'Бенковски',
                'alias' => 'benkovski',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/blagoevgrad/',
                'title' => 'Благоевград',
                'alias' => 'blagoevgrad',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/bulgari/',
                'title' => 'Болгари',
                'alias' => 'bulgari',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/borovets/',
                'title' => 'Боровец',
                'alias' => 'borovets',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/boryana/',
                'title' => 'Боряна',
                'alias' => 'boryana',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/burgas/',
                'title' => 'Бургас',
                'alias' => 'burgas',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/burgas-area/',
                'title' => 'Бургасская область',
                'alias' => 'burgas-area',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/byala/',
                'title' => 'Бяла',
                'alias' => 'byala',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/varna/',
                'title' => 'Варна',
                'alias' => 'varna',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/varna-area/',
                'title' => 'Варненская область',
                'alias' => 'varna-area',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/velko-tarnovo/',
                'title' => 'Велико Тырново',
                'alias' => 'velko-tarnovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/velingrad/',
                'title' => 'Велинград',
                'alias' => 'velingrad',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/vetrino/',
                'title' => 'Ветрино',
                'alias' => 'vetrino',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/north-western-bulgaria/vidin/',
                'title' => 'Видин',
                'alias' => 'vidin',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/voivodino/',
                'title' => 'Войводино',
                'alias' => 'voivodino',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/north-western-bulgaria/vratsa/',
                'title' => 'Враца',
                'alias' => 'vratsa',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/gabrovo/',
                'title' => 'Габрово',
                'alias' => 'gabrovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/general-kantardjievo/',
                'title' => 'Генерал Кантарджиево',
                'alias' => 'general-kantardjievo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/general-kiselovo/',
                'title' => 'Генерал-Киселово',
                'alias' => 'general-kiselovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/general-toshevo/',
                'title' => 'Генерал-Тошево',
                'alias' => 'general-toshevo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/goritsa/',
                'title' => 'Горица',
                'alias' => 'goritsa',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/dobrich/',
                'title' => 'Добрич',
                'alias' => 'dobrich',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/dobrich-area/',
                'title' => 'Добричская область',
                'alias' => 'dobrich-area',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/durankulak/',
                'title' => 'Дуранкулак',
                'alias' => 'durankulak',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/elenite/',
                'title' => 'Елените',
                'alias' => 'elenite',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/yelkhovo/',
                'title' => 'Елхово',
                'alias' => 'yelkhovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/golden-sands/',
                'title' => 'Золотые Пески',
                'alias' => 'golden-sands',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/izgrev/',
                'title' => 'Изгрев',
                'alias' => 'izgrev',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kableshkovo/',
                'title' => 'Каблешково',
                'alias' => 'kableshkovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kavarna/',
                'title' => 'Каварна',
                'alias' => 'kavarna',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kamenbryag/',
                'title' => 'Камен-бряг',
                'alias' => 'kamenbryag',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kardam/',
                'title' => 'Кардам',
                'alias' => 'kardam',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kiten/',
                'title' => 'Китен',
                'alias' => 'kiten',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/north-western-bulgaria/kovachevtsi/',
                'title' => 'Ковачевци',
                'alias' => 'kovachevtsi',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kosharitsa/',
                'title' => 'Кошарица',
                'alias' => 'kosharitsa',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/kranevo/',
                'title' => 'Кранево',
                'alias' => 'kranevo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/lovech/',
                'title' => 'Ловеч',
                'alias' => 'lovech',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/lozenets/',
                'title' => 'Лозенец',
                'alias' => 'lozenets',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/malina/',
                'title' => 'Малина',
                'alias' => 'malina',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/malko-tarnovo/',
                'title' => 'Малко-Тырново',
                'alias' => 'malko-tarnovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/north-western-bulgaria/montana/',
                'title' => 'Монтана',
                'alias' => 'montana',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/nesebr/',
                'title' => 'Несебр',
                'alias' => 'nesebr',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/nikolaevka/',
                'title' => 'Николаевка',
                'alias' => 'nikolaevka',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/obzor/',
                'title' => 'Обзор',
                'alias' => 'obzor',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/pamporovo/',
                'title' => 'Пампорово',
                'alias' => 'pamporovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/pernik/',
                'title' => 'Перник',
                'alias' => 'pernik',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/pleven/',
                'title' => 'Плевен',
                'alias' => 'pleven',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/plenimir/',
                'title' => 'Пленимир',
                'alias' => 'plenimir',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/plovdiv/',
                'title' => 'Пловдив',
                'alias' => 'plovdiv',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/polskiizvor/',
                'title' => 'Полски-Извор',
                'alias' => 'polskiizvor',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/pomorie/',
                'title' => 'Поморие',
                'alias' => 'pomorie',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/popovo/',
                'title' => 'Попово',
                'alias' => 'popovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/primorsko/',
                'title' => 'Приморско',
                'alias' => 'primorsko',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/ravadinovo/',
                'title' => 'Равадиново',
                'alias' => 'ravadinovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/ravda/',
                'title' => 'Равда',
                'alias' => 'ravda',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/razgrad/',
                'title' => 'Разград',
                'alias' => 'razgrad',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/razlog/',
                'title' => 'Разлог',
                'alias' => 'razlog',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/rosen/',
                'title' => 'Росен',
                'alias' => 'rosen',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/russe/',
                'title' => 'Русе',
                'alias' => 'russe',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/sandanski/',
                'title' => 'Сандански',
                'alias' => 'sandanski',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sarafovo/',
                'title' => 'Сарафово',
                'alias' => 'sarafovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sveti-vlas/',
                'title' => 'Святой Влас',
                'alias' => 'sveti-vlas',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/constantine-and-helena/',
                'title' => 'Святые Константин и Елена',
                'alias' => 'constantine-and-helena',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/silistra/',
                'title' => 'Силистра',
                'alias' => 'silistra',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sinemorets/',
                'title' => 'Синеморец',
                'alias' => 'sinemorets',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/sliven/',
                'title' => 'Сливен',
                'alias' => 'sliven',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/mountain-area/smolyan/',
                'title' => 'Смолян',
                'alias' => 'smolyan',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sozopol/',
                'title' => 'Созополь',
                'alias' => 'sozopol',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sokolovo/',
                'title' => 'Соколово',
                'alias' => 'sokolovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/sunny-beach/',
                'title' => 'Солнечный берег',
                'alias' => 'sunny-beach',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/north-western-bulgaria/sofia/',
                'title' => 'София',
                'alias' => 'sofia',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/stara-zagora/',
                'title' => 'Стара Загора',
                'alias' => 'stara-zagora',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/terziysko/',
                'title' => 'Терзийско',
                'alias' => 'terziysko',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/topola/',
                'title' => 'Топола',
                'alias' => 'topola',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/trustikovo/',
                'title' => 'Трыстиково',
                'alias' => 'trustikovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/targovishte/',
                'title' => 'Тырговиште',
                'alias' => 'targovishte',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/khaskovo/',
                'title' => 'Хасково',
                'alias' => 'khaskovo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/tsarevo/',
                'title' => 'Царево',
                'alias' => 'tsarevo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/chernevo/',
                'title' => 'Чернево',
                'alias' => 'chernevo',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/chernomore/',
                'title' => 'Черно-Море',
                'alias' => 'chernomore',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/chernomorets/',
                'title' => 'Черноморец',
                'alias' => 'chernomorets',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/black-sea-area/shkorpilovtsi/',
                'title' => 'Шкорпиловци',
                'alias' => 'shkorpilovtsi',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/shumen/',
                'title' => 'Шумен',
                'alias' => 'shumen',
            ],
            [
                'url' => 'https://prian.ru/bulgaria/central-bulgaria/yambol/',
                'title' => 'Ямбол',
                'alias' => 'yambol',
            ],
        ],
        'url' => 'https://prian.ru/bulgaria/',
        'title' => 'Болгария',
        'alias' => 'bulgaria',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/brazil/',
        'title' => 'Бразилия',
        'alias' => 'brazil',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/great-britain/england/buckinghamshire/',
                'title' => 'Бакингемшир',
                'alias' => 'buckinghamshire',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/birmingham/',
                'title' => 'Бирмингем',
                'alias' => 'birmingham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/bridgwater/',
                'title' => 'Бриджуотер',
                'alias' => 'bridgwater',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/bristol/',
                'title' => 'Бристоль',
                'alias' => 'bristol',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/glasgow/',
                'title' => 'Глазго',
                'alias' => 'glasgow',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/durham/',
                'title' => 'Дарем',
                'alias' => 'durham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/doncaster/',
                'title' => 'Донкастер',
                'alias' => 'doncaster',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/inverness/',
                'title' => 'Инвернесс',
                'alias' => 'inverness',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/inverurie/',
                'title' => 'Инверури',
                'alias' => 'inverurie',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/york/',
                'title' => 'Йорк',
                'alias' => 'york',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/cambridge/',
                'title' => 'Кембридж',
                'alias' => 'cambridge',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/kendal/',
                'title' => 'Кендал',
                'alias' => 'kendal',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/kilmarnock/',
                'title' => 'Килмарнок',
                'alias' => 'kilmarnock',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/cobham/',
                'title' => 'Кобхем',
                'alias' => 'cobham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/leicester/',
                'title' => 'Лестер',
                'alias' => 'leicester',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/liverpool/',
                'title' => 'Ливерпуль',
                'alias' => 'liverpool',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/leeds/',
                'title' => 'Лидс',
                'alias' => 'leeds',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/london/',
                'title' => 'Лондон',
                'alias' => 'london',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/manchester/',
                'title' => 'Манчестер',
                'alias' => 'manchester',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/northampton/',
                'title' => 'Нортгемптон',
                'alias' => 'northampton',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/newcastle-upon-tyne/',
                'title' => 'Ньюкасл-апон-Тайн',
                'alias' => 'newcastle-upon-tyne',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/aldershot/',
                'title' => 'Олдершот',
                'alias' => 'aldershot',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/peterborough/',
                'title' => 'Питерборо',
                'alias' => 'peterborough',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/potters-bar/',
                'title' => 'Поттерс Бар',
                'alias' => 'potters-bar',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/poole/',
                'title' => 'Пул',
                'alias' => 'poole',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/redhill/',
                'title' => 'Редхилл',
                'alias' => 'redhill',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/richmond/',
                'title' => 'Ричмонд',
                'alias' => 'richmond',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/rotherham/',
                'title' => 'Ротерем',
                'alias' => 'rotherham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/sittingbourne/',
                'title' => 'Ситтингборн',
                'alias' => 'sittingbourne',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/scunthorpe/',
                'title' => 'Сканторп',
                'alias' => 'scunthorpe',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/swalcliffe/',
                'title' => 'Суолклифф',
                'alias' => 'swalcliffe',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/surrey/',
                'title' => 'Суррей',
                'alias' => 'surrey',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/teddington/',
                'title' => 'Теддингтон',
                'alias' => 'teddington',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/torquay/',
                'title' => 'Торки',
                'alias' => 'torquay',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/trowbridge/',
                'title' => 'Троубридж',
                'alias' => 'trowbridge',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/twickenham/',
                'title' => 'Туикенем',
                'alias' => 'twickenham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/wandsworth/',
                'title' => 'Уондсуэрт',
                'alias' => 'wandsworth',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/fareham/',
                'title' => 'Фархэм',
                'alias' => 'fareham',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/fort-william/',
                'title' => 'Форт-Уильям',
                'alias' => 'fort-william',
            ],
            [
                'url' => 'https://prian.ru/great-britain/wales/haverfordwest/',
                'title' => 'Хаверфордуэст',
                'alias' => 'haverfordwest',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/hampton/',
                'title' => 'Хамптон',
                'alias' => 'hampton',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/haywards-heath/',
                'title' => 'Хейвордс-Хит',
                'alias' => 'haywards-heath',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/chester/',
                'title' => 'Честер',
                'alias' => 'chester',
            ],
            [
                'url' => 'https://prian.ru/great-britain/england/sheffield/',
                'title' => 'Шеффилд',
                'alias' => 'sheffield',
            ],
            [
                'url' => 'https://prian.ru/great-britain/scotland/edinburgh/',
                'title' => 'Эдинбург',
                'alias' => 'edinburgh',
            ],
        ],
        'url' => 'https://prian.ru/great-britain/',
        'title' => 'Великобритания',
        'alias' => 'great-britain',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/hungary/hungary/badacsonytomaj/',
                'title' => 'Бадачоньтомай',
                'alias' => 'badacsonytomaj',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/balaton/',
                'title' => 'Балатон',
                'alias' => 'balaton',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/budapest/',
                'title' => 'Будапешт',
                'alias' => 'budapest',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/velence/',
                'title' => 'Веленце',
                'alias' => 'velence',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/gyenesdias/',
                'title' => 'Дьенешдиаш',
                'alias' => 'gyenesdias',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/gyor/',
                'title' => 'Дьёр',
                'alias' => 'gyor',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/zala/',
                'title' => 'Зала',
                'alias' => 'zala',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/zalakaros/',
                'title' => 'Залакарош',
                'alias' => 'zalakaros',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/szentendre/',
                'title' => 'Сентендре',
                'alias' => 'szentendre',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/hajduszoboszlo/',
                'title' => 'Хайдусобосло',
                'alias' => 'hajduszoboszlo',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/heviz/',
                'title' => 'Хевиз',
                'alias' => 'heviz',
            ],
            [
                'url' => 'https://prian.ru/hungary/hungary/sopron/',
                'title' => 'Шопрон',
                'alias' => 'sopron',
            ],
        ],
        'url' => 'https://prian.ru/hungary/',
        'title' => 'Венгрия',
        'alias' => 'hungary',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/vietnam/vietnam/nha-trang/',
                'title' => 'Нячанг',
                'alias' => 'nha-trang',
            ],
        ],
        'url' => 'https://prian.ru/vietnam/',
        'title' => 'Вьетнам',
        'alias' => 'vietnam',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/gambia/',
        'title' => 'Гамбия',
        'alias' => 'gambia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/germany/western-germany/aachen/',
                'title' => 'Аахен',
                'alias' => 'aachen',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/allgau/',
                'title' => 'Альгой',
                'alias' => 'allgau',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/ahrensburg/',
                'title' => 'Аренсбург',
                'alias' => 'ahrensburg',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/augsburg/',
                'title' => 'Аугсбург',
                'alias' => 'augsburg',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/aschaffenburg/',
                'title' => 'Ашаффенбург',
                'alias' => 'aschaffenburg',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/bayerischer-wald/',
                'title' => 'Баварский Лес',
                'alias' => 'bayerischer-wald',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/bad-wiessee/',
                'title' => 'Бад-Висзе',
                'alias' => 'bad-wiessee',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bad-duerkheim/',
                'title' => 'Бад-Дюркхайм',
                'alias' => 'bad-duerkheim',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bad-vilbel/',
                'title' => 'Бад-Фильбель',
                'alias' => 'bad-vilbel',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/baden-baden/',
                'title' => 'Баден-Баден',
                'alias' => 'baden-baden',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/baden-wuerttemberg/',
                'title' => 'Баден-Вюртемберг',
                'alias' => 'baden-wuerttemberg',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/badenweiler/',
                'title' => 'Баденвайлер',
                'alias' => 'badenweiler',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/bamberg/',
                'title' => 'Бамберг',
                'alias' => 'bamberg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bergheim/',
                'title' => 'Бергхайм',
                'alias' => 'bergheim',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/berlin/',
                'title' => 'Берлин',
                'alias' => 'berlin',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/biberach/',
                'title' => 'Биберах',
                'alias' => 'biberach',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bonn/',
                'title' => 'Бонн',
                'alias' => 'bonn',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bochum/',
                'title' => 'Бохум',
                'alias' => 'bochum',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/brandenburg/',
                'title' => 'Бранденбург',
                'alias' => 'brandenburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/bremen/',
                'title' => 'Бремен',
                'alias' => 'bremen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/wiesbaden/',
                'title' => 'Висбаден',
                'alias' => 'wiesbaden',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/wuppertal/',
                'title' => 'Вупперталь',
                'alias' => 'wuppertal',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/wuerzburg/',
                'title' => 'Вюрцбург',
                'alias' => 'wuerzburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/hamburg/',
                'title' => 'Гамбург',
                'alias' => 'hamburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/hannover/',
                'title' => 'Ганновер',
                'alias' => 'hannover',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/garmisch-partenkirchen/',
                'title' => 'Гармиш-Партенкирхен',
                'alias' => 'garmisch-partenkirchen',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/goerlitz/',
                'title' => 'Герлиц',
                'alias' => 'goerlitz',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/germering/',
                'title' => 'Гермеринг',
                'alias' => 'germering',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/hessen/',
                'title' => 'Гессен',
                'alias' => 'hessen',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/dachau/',
                'title' => 'Дахау',
                'alias' => 'dachau',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/dortmund/',
                'title' => 'Дортмунд',
                'alias' => 'dortmund',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/dresden/',
                'title' => 'Дрезден',
                'alias' => 'dresden',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/duisburg/',
                'title' => 'Дуйсбург',
                'alias' => 'duisburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/duesseldorf/',
                'title' => 'Дюссельдорф',
                'alias' => 'duesseldorf',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/solingen/',
                'title' => 'Золинген',
                'alias' => 'solingen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/uetersen/',
                'title' => 'Итерзен',
                'alias' => 'uetersen',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/jena/',
                'title' => 'Йена',
                'alias' => 'jena',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/karlsruhe/',
                'title' => 'Карлсруэ',
                'alias' => 'karlsruhe',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/kaarst/',
                'title' => 'Карст',
                'alias' => 'kaarst',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/koeln/',
                'title' => 'Кельн',
                'alias' => 'koeln',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/coburg/',
                'title' => 'Кобург',
                'alias' => 'coburg',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/konstanz/',
                'title' => 'Констанц',
                'alias' => 'konstanz',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/krefeld/',
                'title' => 'Крефельд',
                'alias' => 'krefeld',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/cuxhaven/',
                'title' => 'Куксхафен',
                'alias' => 'cuxhaven',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/leipzig/',
                'title' => 'Лейпциг',
                'alias' => 'leipzig',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/lubbenau/',
                'title' => 'Люббенау',
                'alias' => 'lubbenau',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/lubeck/',
                'title' => 'Любек',
                'alias' => 'lubeck',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/mainz/',
                'title' => 'Майнц',
                'alias' => 'mainz',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/mannheim/',
                'title' => 'Мангейм',
                'alias' => 'mannheim',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/mecklenburg-vorpommern/',
                'title' => 'Мекленбург-Передняя Померания',
                'alias' => 'mecklenburg-vorpommern',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/moenchengladbach/',
                'title' => 'Мёнхенгладбах',
                'alias' => 'moenchengladbach',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/muelheim-an-der-ruhr/',
                'title' => 'Мюльхайм-на-Руре',
                'alias' => 'muelheim-an-der-ruhr',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/muenster/',
                'title' => 'Мюнстер',
                'alias' => 'muenster',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/muenchen/',
                'title' => 'Мюнхен',
                'alias' => 'muenchen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/niedersachsen/',
                'title' => 'Нижняя Саксония',
                'alias' => 'niedersachsen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/neuss/',
                'title' => 'Нойс',
                'alias' => 'neuss',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/nuremberg/',
                'title' => 'Нюрнберг',
                'alias' => 'nuremberg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/oberhausen/',
                'title' => 'Оберхаузен',
                'alias' => 'oberhausen',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/oranienburg/',
                'title' => 'Ораниенбург',
                'alias' => 'oranienburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/osnabrueck/',
                'title' => 'Оснабрюк',
                'alias' => 'osnabrueck',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/passau/',
                'title' => 'Пассау',
                'alias' => 'passau',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/potsdam/',
                'title' => 'Потсдам',
                'alias' => 'potsdam',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/puchheim/',
                'title' => 'Пуххайм',
                'alias' => 'puchheim',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/pfronten/',
                'title' => 'Пфронтен',
                'alias' => 'pfronten',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/rheinland-pfalz/',
                'title' => 'Рейнланд-Пфальц',
                'alias' => 'rheinland-pfalz',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/rottenbuch/',
                'title' => 'Роттенбух',
                'alias' => 'rottenbuch',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/ruegen/',
                'title' => 'Рюген',
                'alias' => 'ruegen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/saarland/',
                'title' => 'Саарланд',
                'alias' => 'saarland',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/sachsen/',
                'title' => 'Саксония',
                'alias' => 'sachsen',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/sahsenanhalt/',
                'title' => 'Саксония-Анхальт',
                'alias' => 'sahsenanhalt',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/franken/',
                'title' => 'Северная Бавария (Франкония)',
                'alias' => 'franken',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/nordrhein-westfalen/',
                'title' => 'Северный Рейн-Вестфалия',
                'alias' => 'nordrhein-westfalen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/trier/',
                'title' => 'Трир',
                'alias' => 'trier',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/thuringen/',
                'title' => 'Тюрингия',
                'alias' => 'thuringen',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/ulm/',
                'title' => 'Ульм',
                'alias' => 'ulm',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/freiburg/',
                'title' => 'Фрайбург',
                'alias' => 'freiburg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/frankenthal/',
                'title' => 'Франкенталь',
                'alias' => 'frankenthal',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/frankfurt-am-main/',
                'title' => 'Франкфурт-на-Майне',
                'alias' => 'frankfurt-am-main',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/frankfurt/',
                'title' => 'Франкфурт-на-Одере',
                'alias' => 'frankfurt',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/freudenstadt/',
                'title' => 'Фройденштадт',
                'alias' => 'freudenstadt',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/furstenwalde/',
                'title' => 'Фюрстенвальде',
                'alias' => 'furstenwalde',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/fussen/',
                'title' => 'Фюссен',
                'alias' => 'fussen',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/hagen/',
                'title' => 'Хаген',
                'alias' => 'hagen',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/heilbronn/',
                'title' => 'Хайльбронн',
                'alias' => 'heilbronn',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/hallbergmoos/',
                'title' => 'Халльбергмос',
                'alias' => 'hallbergmoos',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/hamm/',
                'title' => 'Хамм',
                'alias' => 'hamm',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/chemnitz/',
                'title' => 'Хемниц',
                'alias' => 'chemnitz',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/zwickau/',
                'title' => 'Цвиккау',
                'alias' => 'zwickau',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/celle/',
                'title' => 'Целле',
                'alias' => 'celle',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/scheidegg/',
                'title' => 'Шайдегг',
                'alias' => 'scheidegg',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/schleswig-holstein/',
                'title' => 'Шлезвиг-Гольштейн',
                'alias' => 'schleswig-holstein',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/speyer/',
                'title' => 'Шпайер',
                'alias' => 'speyer',
            ],
            [
                'url' => 'https://prian.ru/germany/berlin/stralsund/',
                'title' => 'Штральзунд',
                'alias' => 'stralsund',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/stuttgart/',
                'title' => 'Штутгарт',
                'alias' => 'stuttgart',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/elmshorn/',
                'title' => 'Эльмсхорн',
                'alias' => 'elmshorn',
            ],
            [
                'url' => 'https://prian.ru/germany/eastern-germany/erfurt/',
                'title' => 'Эрфурт',
                'alias' => 'erfurt',
            ],
            [
                'url' => 'https://prian.ru/germany/western-germany/essen/',
                'title' => 'Эссен',
                'alias' => 'essen',
            ],
            [
                'url' => 'https://prian.ru/germany/southern-germany/sued-bayern/',
                'title' => 'Южная Бавария',
                'alias' => 'sued-bayern',
            ],
        ],
        'url' => 'https://prian.ru/germany/',
        'title' => 'Германия',
        'alias' => 'germany',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/agios-konstantinos/',
                'title' => 'Айос-Константинос',
                'alias' => 'agios-konstantinos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/agios-nikolaos/',
                'title' => 'Айос-Николаос',
                'alias' => 'agios-nikolaos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/agios-pavlos/',
                'title' => 'Айос-Павлос',
                'alias' => 'agios-pavlos',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/agios-stefanos/',
                'title' => 'Айос-Стефанос',
                'alias' => 'agios-stefanos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/agia-pelagia/',
                'title' => 'Айя-Пелагия',
                'alias' => 'agia-pelagia',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/akrata/',
                'title' => 'Акрата',
                'alias' => 'akrata',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/amaliapoli/',
                'title' => 'Амалиаполи',
                'alias' => 'amaliapoli',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/analipsi/',
                'title' => 'Аналипси',
                'alias' => 'analipsi',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/andros/',
                'title' => 'Андрос',
                'alias' => 'andros',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/anissaras/',
                'title' => 'Аниссарас',
                'alias' => 'anissaras',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/argolis/',
                'title' => 'Арголида',
                'alias' => 'argolis',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/attica/',
                'title' => 'Аттика',
                'alias' => 'attica',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/athens/',
                'title' => 'Афины',
                'alias' => 'athens',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/mount-athos/',
                'title' => 'Афон',
                'alias' => 'mount-athos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/bali/',
                'title' => 'Бали',
                'alias' => 'bali',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/boiothia/',
                'title' => 'Виотия',
                'alias' => 'boiothia',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/volos/',
                'title' => 'Волос',
                'alias' => 'volos',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/voula/',
                'title' => 'Вула',
                'alias' => 'voula',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/glyfada/',
                'title' => 'Глифада',
                'alias' => 'glyfada',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/pelion/',
                'title' => 'гора Пелион',
                'alias' => 'pelion',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/diakopto/',
                'title' => 'Диакопто',
                'alias' => 'diakopto',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/dilesi/',
                'title' => 'Дилеси',
                'alias' => 'dilesi',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/drama/',
                'title' => 'Драма',
                'alias' => 'drama',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/zakynthos/',
                'title' => 'Закинф',
                'alias' => 'zakynthos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/ierapetra/',
                'title' => 'Иерапетра',
                'alias' => 'ierapetra',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/imathia/',
                'title' => 'Иматия',
                'alias' => 'imathia',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/ionian-islands/',
                'title' => 'Ионические острова',
                'alias' => 'ionian-islands',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/heraklion/',
                'title' => 'Ираклион',
                'alias' => 'heraklion',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/ithaca/',
                'title' => 'Итака',
                'alias' => 'ithaca',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/kavala/',
                'title' => 'Кавала',
                'alias' => 'kavala',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/kassandra/',
                'title' => 'Кассандра',
                'alias' => 'kassandra',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/kea/',
                'title' => 'Кея',
                'alias' => 'kea',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/cyclades/',
                'title' => 'Киклады',
                'alias' => 'cyclades',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/kilkis/',
                'title' => 'Килкис',
                'alias' => 'kilkis',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/kozani/',
                'title' => 'Козани',
                'alias' => 'kozani',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/corinth/',
                'title' => 'Коринф',
                'alias' => 'corinth',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/corinthia/',
                'title' => 'Коринфия',
                'alias' => 'corinthia',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/corfu/',
                'title' => 'Корфу',
                'alias' => 'corfu',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/xanthi/',
                'title' => 'Ксанти',
                'alias' => 'xanthi',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/lagonisi/',
                'title' => 'Лагониси',
                'alias' => 'lagonisi',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/lakonia/',
                'title' => 'Лакония',
                'alias' => 'lakonia',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/lasithi/',
                'title' => 'Ласити',
                'alias' => 'lasithi',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/leukas/',
                'title' => 'Лефкас',
                'alias' => 'leukas',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/ligaria/',
                'title' => 'Лигарья',
                'alias' => 'ligaria',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/lutraki/',
                'title' => 'Лутраки',
                'alias' => 'lutraki',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/magnesia/',
                'title' => 'Магнисия',
                'alias' => 'magnesia',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/malia/',
                'title' => 'Малья',
                'alias' => 'malia',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/messini/',
                'title' => 'Мессини',
                'alias' => 'messini',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/mikonos/',
                'title' => 'Миконос',
                'alias' => 'mikonos',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/milatos/',
                'title' => 'Милатос',
                'alias' => 'milatos',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/naxos/',
                'title' => 'Наксос',
                'alias' => 'naxos',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/navplion/',
                'title' => 'Нафплион',
                'alias' => 'navplion',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/nea-makri/',
                'title' => 'Неа Макри',
                'alias' => 'nea-makri',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/nom-heraklion/',
                'title' => 'ном Ираклион',
                'alias' => 'nom-heraklion',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/nom-lasithi/',
                'title' => 'ном Ласити',
                'alias' => 'nom-lasithi',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/nom-rethymno/',
                'title' => 'ном Ретимно',
                'alias' => 'nom-rethymno',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/nom-chania/',
                'title' => 'ном Ханья',
                'alias' => 'nom-chania',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/olympiaki-akti/',
                'title' => 'Олимпиаки-Акти',
                'alias' => 'olympiaki-akti',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/dodecanese/',
                'title' => 'острова Додеканес',
                'alias' => 'dodecanese',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/paralia/',
                'title' => 'Паралия',
                'alias' => 'paralia',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/paros/',
                'title' => 'Парос',
                'alias' => 'paros',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/patras/',
                'title' => 'Патры',
                'alias' => 'patras',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/paiania/',
                'title' => 'Пеания',
                'alias' => 'paiania',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/pella/',
                'title' => 'Пелла',
                'alias' => 'pella',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/peloponnese/',
                'title' => 'Пелопоннес',
                'alias' => 'peloponnese',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/pieria/',
                'title' => 'Пиерия',
                'alias' => 'pieria',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/pireas/',
                'title' => 'Пирей',
                'alias' => 'pireas',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/poligiros/',
                'title' => 'Полигирос',
                'alias' => 'poligiros',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/poros/',
                'title' => 'Порос',
                'alias' => 'poros',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/porto-cheli/',
                'title' => 'Порто-Хели',
                'alias' => 'porto-cheli',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/rafina/',
                'title' => 'Рафина',
                'alias' => 'rafina',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/rethymno/',
                'title' => 'Ретимно',
                'alias' => 'rethymno',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/rhodes/',
                'title' => 'Родос',
                'alias' => 'rhodes',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/salamis/',
                'title' => 'Саламин',
                'alias' => 'salamis',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/thessaloniki/',
                'title' => 'Салоники',
                'alias' => 'thessaloniki',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/sani/',
                'title' => 'Сани',
                'alias' => 'sani',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/santorini/',
                'title' => 'Санторини',
                'alias' => 'santorini',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/saronida/',
                'title' => 'Саронида',
                'alias' => 'saronida',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/north-aegean-islands/',
                'title' => 'Северо-Эгейские острова',
                'alias' => 'north-aegean-islands',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/serres/',
                'title' => 'Серре',
                'alias' => 'serres',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/syvota/',
                'title' => 'Сивота',
                'alias' => 'syvota',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/syros/',
                'title' => 'Сирос',
                'alias' => 'syros',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/sisi/',
                'title' => 'Сиси',
                'alias' => 'sisi',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/sissi/',
                'title' => 'Сисси',
                'alias' => 'sissi',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/sithonia/',
                'title' => 'Ситония',
                'alias' => 'sithonia',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/sciathos/',
                'title' => 'Скиатос',
                'alias' => 'sciathos',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/skopelos/',
                'title' => 'Скопелос',
                'alias' => 'skopelos',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/spetses/',
                'title' => 'Спеце',
                'alias' => 'spetses',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/sporades/',
                'title' => 'Спорады',
                'alias' => 'sporades',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/thasos/',
                'title' => 'Тасос',
                'alias' => 'thasos',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/phthiotis/',
                'title' => 'Фтиотида',
                'alias' => 'phthiotis',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/chalcis/',
                'title' => 'Халкида',
                'alias' => 'chalcis',
            ],
            [
                'url' => 'https://prian.ru/greece/northern-greece/chalkidiki/',
                'title' => 'Халкидики',
                'alias' => 'chalkidiki',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/hania/',
                'title' => 'Ханья',
                'alias' => 'hania',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/hersonissos/',
                'title' => 'Херсониссос',
                'alias' => 'hersonissos',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/chios/',
                'title' => 'Хиос',
                'alias' => 'chios',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/evia/',
                'title' => 'Эвбея',
                'alias' => 'evia',
            ],
            [
                'url' => 'https://prian.ru/greece/aegean-and-ionic-islands/aegina/',
                'title' => 'Эгина',
                'alias' => 'aegina',
            ],
            [
                'url' => 'https://prian.ru/greece/crete/elounda/',
                'title' => 'Элунда',
                'alias' => 'elounda',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/epidaurus/',
                'title' => 'Эпидавр',
                'alias' => 'epidaurus',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/eretria/',
                'title' => 'Эретрия',
                'alias' => 'eretria',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/ermioni/',
                'title' => 'Эрмиони',
                'alias' => 'ermioni',
            ],
            [
                'url' => 'https://prian.ru/greece/central-greece-and-peloponnese/aetolia-acarnania/',
                'title' => 'Этолия и Акарнания',
                'alias' => 'aetolia-acarnania',
            ],
        ],
        'url' => 'https://prian.ru/greece/',
        'title' => 'Греция',
        'alias' => 'greece',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/georgia/samtskhe-javakheti/bakuriani/',
                'title' => 'Бакуриани',
                'alias' => 'bakuriani',
            ],
            [
                'url' => 'https://prian.ru/georgia/adjara/batumi/',
                'title' => 'Батуми',
                'alias' => 'batumi',
            ],
            [
                'url' => 'https://prian.ru/georgia/adjara/kobuleti/',
                'title' => 'Кобулети',
                'alias' => 'kobuleti',
            ],
            [
                'url' => 'https://prian.ru/georgia/tbilisi/tbilisi/',
                'title' => 'Тбилиси',
                'alias' => 'tbilisi',
            ],
            [
                'url' => 'https://prian.ru/georgia/adjara/shekhvetili/',
                'title' => 'Шекветили',
                'alias' => 'shekhvetili',
            ],
        ],
        'url' => 'https://prian.ru/georgia/',
        'title' => 'Грузия',
        'alias' => 'georgia',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/denmark/',
        'title' => 'Дания',
        'alias' => 'denmark',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/bavaro/',
                'title' => 'Баваро',
                'alias' => 'bavaro',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/bayahibe/',
                'title' => 'Байяибе',
                'alias' => 'bayahibe',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/gaspar-hernandez/',
                'title' => 'Гаспар Эрнандес',
                'alias' => 'gaspar-hernandez',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/cabarete/',
                'title' => 'Кабарете',
                'alias' => 'cabarete',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/las-terrenas/',
                'title' => 'Лас Терренас',
                'alias' => 'las-terrenas',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/punta-cana/',
                'title' => 'Пунта-Кана',
                'alias' => 'punta-cana',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/puerto-plata/',
                'title' => 'Пуэрто-Плата',
                'alias' => 'puerto-plata',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/santo-domingo/',
                'title' => 'Санто-Доминго',
                'alias' => 'santo-domingo',
            ],
            [
                'url' => 'https://prian.ru/dominican-republic/dominican-republic/sosua/',
                'title' => 'Сосуа',
                'alias' => 'sosua',
            ],
        ],
        'url' => 'https://prian.ru/dominican-republic/',
        'title' => 'Доминиканская Республика',
        'alias' => 'dominican-republic',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/egypt/egypt/cairo/',
                'title' => 'Каир',
                'alias' => 'cairo',
            ],
            [
                'url' => 'https://prian.ru/egypt/egypt/hurghada/',
                'title' => 'Хургада',
                'alias' => 'hurghada',
            ],
        ],
        'url' => 'https://prian.ru/egypt/',
        'title' => 'Египет',
        'alias' => 'egypt',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/israel/southern-district/ashdod/',
                'title' => 'Ашдод',
                'alias' => 'ashdod',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/bat-yam/',
                'title' => 'Бат-Ям',
                'alias' => 'bat-yam',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/bnei-brak/',
                'title' => 'Бней-Брак',
                'alias' => 'bnei-brak',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/herzliya/',
                'title' => 'Герцлия',
                'alias' => 'herzliya',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/givatayim/',
                'title' => 'Гиватаим',
                'alias' => 'givatayim',
            ],
            [
                'url' => 'https://prian.ru/israel/jerusalem-district/jerusalem/',
                'title' => 'Иерусалим',
                'alias' => 'jerusalem',
            ],
            [
                'url' => 'https://prian.ru/israel/haifa-district/caesarea/',
                'title' => 'Кейсария',
                'alias' => 'caesarea',
            ],
            [
                'url' => 'https://prian.ru/israel/central-district/netanya/',
                'title' => 'Нетания',
                'alias' => 'netanya',
            ],
            [
                'url' => 'https://prian.ru/israel/central-district/raanana/',
                'title' => 'Раанана',
                'alias' => 'raanana',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/ramat-gan/',
                'title' => 'Рамат-Ган',
                'alias' => 'ramat-gan',
            ],
            [
                'url' => 'https://prian.ru/israel/central-district/rehovot/',
                'title' => 'Реховот',
                'alias' => 'rehovot',
            ],
            [
                'url' => 'https://prian.ru/israel/central-district/rishon-le-zion/',
                'title' => 'Ришон-ле-Цион',
                'alias' => 'rishon-le-zion',
            ],
            [
                'url' => 'https://prian.ru/israel/tel-aviv-district/tel-aviv/',
                'title' => 'Тель-Авив',
                'alias' => 'tel-aviv',
            ],
            [
                'url' => 'https://prian.ru/israel/haifa-district/haifa/',
                'title' => 'Хайфа',
                'alias' => 'haifa',
            ],
        ],
        'url' => 'https://prian.ru/israel/',
        'title' => 'Израиль',
        'alias' => 'israel',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/indonesia/bali/sanur/',
                'title' => 'Санур',
                'alias' => 'sanur',
            ],
            [
                'url' => 'https://prian.ru/indonesia/bali/candidasa/',
                'title' => 'Чандидаса',
                'alias' => 'candidasa',
            ],
        ],
        'url' => 'https://prian.ru/indonesia/',
        'title' => 'Индонезия',
        'alias' => 'indonesia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/ireland/ireland/dublin/',
                'title' => 'Дублин',
                'alias' => 'dublin',
            ],
        ],
        'url' => 'https://prian.ru/ireland/',
        'title' => 'Ирландия',
        'alias' => 'ireland',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/iceland/',
        'title' => 'Исландия',
        'alias' => 'iceland',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/spain/canary-islands/adeje/',
                'title' => 'Адехе',
                'alias' => 'adeje',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/alicante/',
                'title' => 'Аликанте',
                'alias' => 'alicante',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/altea-hills/',
                'title' => 'Алтьеа-Хиллс',
                'alias' => 'altea-hills',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/albatera/',
                'title' => 'Альбатера',
                'alias' => 'albatera',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/alboraya/',
                'title' => 'Альборая',
                'alias' => 'alboraya',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/algorfa/',
                'title' => 'Альгорфа',
                'alias' => 'algorfa',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/almeria/',
                'title' => 'Альмерия',
                'alias' => 'almeria',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/almoradi/',
                'title' => 'Альморади',
                'alias' => 'almoradi',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/altea/',
                'title' => 'Альтеа',
                'alias' => 'altea',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/alfas-del-pi/',
                'title' => 'Альфас-дель-Пи',
                'alias' => 'alfas-del-pi',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/arenals-del-sol/',
                'title' => 'Ареналес-дель-Соль',
                'alias' => 'arenals-del-sol',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/arenys-de-mar/',
                'title' => 'Ареньс-де-Мар',
                'alias' => 'arenys-de-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/badalona/',
                'title' => 'Бадалона',
                'alias' => 'badalona',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/baqueira-beret/',
                'title' => 'Бакейра Берет',
                'alias' => 'baqueira-beret',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/barcelona/',
                'title' => 'Барселона',
                'alias' => 'barcelona',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/begur/',
                'title' => 'Бегур',
                'alias' => 'begur',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/benahavis/',
                'title' => 'Бенаавис',
                'alias' => 'benahavis',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/benalmadena/',
                'title' => 'Бенальмадена',
                'alias' => 'benalmadena',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/beniarbeig/',
                'title' => 'Бениарбеч',
                'alias' => 'beniarbeig',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/benidorm/',
                'title' => 'Бенидорм',
                'alias' => 'benidorm',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/benissa/',
                'title' => 'Бениса',
                'alias' => 'benissa',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/benitachell/',
                'title' => 'Бенитачель',
                'alias' => 'benitachell',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/benijofar/',
                'title' => 'Бенихофар',
                'alias' => 'benijofar',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/bigastro/',
                'title' => 'Бигастро',
                'alias' => 'bigastro',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/busot/',
                'title' => 'Бусот',
                'alias' => 'busot',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/valencia/',
                'title' => 'Валенсия',
                'alias' => 'valencia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/vergel/',
                'title' => 'Верхель',
                'alias' => 'vergel',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/villanueva-y-geltru/',
                'title' => 'Виланова-и-ла-Желтру',
                'alias' => 'villanueva-y-geltru',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/vilasar-de-dalt/',
                'title' => 'Вилассар-де-Дальт',
                'alias' => 'vilasar-de-dalt',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/villamartin/',
                'title' => 'Вильямартин',
                'alias' => 'villamartin',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/villamarchante/',
                'title' => 'Вильямарчанте',
                'alias' => 'villamarchante',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/villajoyosa/',
                'title' => 'Вильяхойоса',
                'alias' => 'villajoyosa',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/gava/',
                'title' => 'Гава',
                'alias' => 'gava',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/gava-mar/',
                'title' => 'Гава Мар',
                'alias' => 'gava-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/gandia/',
                'title' => 'Гандия',
                'alias' => 'gandia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/gata-de-gorgos/',
                'title' => 'Гата-де-Горгос',
                'alias' => 'gata-de-gorgos',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/godelleta/',
                'title' => 'Годельета',
                'alias' => 'godelleta',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/gran-alacant/',
                'title' => 'Гран-Алакант',
                'alias' => 'gran-alacant',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/guardamar-del-segura/',
                'title' => 'Гуардамар-дель-Сегура',
                'alias' => 'guardamar-del-segura',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/daya-vieja/',
                'title' => 'Дайя-Вьеха',
                'alias' => 'daya-vieja',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/daya-nueva/',
                'title' => 'Дайя-Нуэва',
                'alias' => 'daya-nueva',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/denia/',
                'title' => 'Дения',
                'alias' => 'denia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/dehesa-de-campoamor/',
                'title' => 'Деэса-де-Кампоамор',
                'alias' => 'dehesa-de-campoamor',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/dolores/',
                'title' => 'Долорес',
                'alias' => 'dolores',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/girona/',
                'title' => 'Жирона',
                'alias' => 'girona',
            ],
            [
                'url' => 'https://prian.ru/spain/balearic-islands/ibiza/',
                'title' => 'Ивиса',
                'alias' => 'ibiza',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/cabo-roig/',
                'title' => 'Кабо Роч',
                'alias' => 'cabo-roig',
            ],
            [
                'url' => 'https://prian.ru/spain/balearic-islands/cabrera/',
                'title' => 'Кабрера',
                'alias' => 'cabrera',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/cabrera-de-mar/',
                'title' => 'Кабрера-де-Мар',
                'alias' => 'cabrera-de-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/cabrils/',
                'title' => 'Кабрильс',
                'alias' => 'cabrils',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/calella-de-palafrugell/',
                'title' => 'Калелья-де-Палафружель',
                'alias' => 'calella-de-palafrugell',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/callosa-de-segura/',
                'title' => 'Кальоса-де-Сегура',
                'alias' => 'callosa-de-segura',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/calp/',
                'title' => 'Кальп',
                'alias' => 'calp',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/cambrils/',
                'title' => 'Камбрильс',
                'alias' => 'cambrils',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/el-campello/',
                'title' => 'Кампельо',
                'alias' => 'el-campello',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/campoamor/',
                'title' => 'Кампоамор',
                'alias' => 'campoamor',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/campoverde/',
                'title' => 'Камповерде',
                'alias' => 'campoverde',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/canet-d-en-berenguer/',
                'title' => 'Канет-де-Беренгер',
                'alias' => 'canet-d-en-berenguer',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/castellon/',
                'title' => 'Кастельон',
                'alias' => 'castellon',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/quesada/',
                'title' => 'Кесада',
                'alias' => 'quesada',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/costa-adeje/',
                'title' => 'Коста-Адехе',
                'alias' => 'costa-adeje',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/costa-azahar/',
                'title' => 'Коста-Асаар',
                'alias' => 'costa-azahar',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/costa-blanca/',
                'title' => 'Коста-Бланка',
                'alias' => 'costa-blanca',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/costa-brava/',
                'title' => 'Коста-Брава',
                'alias' => 'costa-brava',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/costa-del-garraf/',
                'title' => 'Коста-дель-Гарраф',
                'alias' => 'costa-del-garraf',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/costa-del-maresme/',
                'title' => 'Коста-дель-Маресме',
                'alias' => 'costa-del-maresme',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/costa-del-sol/',
                'title' => 'Коста-дель-Соль',
                'alias' => 'costa-del-sol',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/costa-daurada/',
                'title' => 'Коста-Дорада',
                'alias' => 'costa-daurada',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/costa-calida/',
                'title' => 'Коста-Калида',
                'alias' => 'costa-calida',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/costa-tropical/',
                'title' => 'Коста-Тропикаль',
                'alias' => 'costa-tropical',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/cubelles/',
                'title' => 'Кубельес',
                'alias' => 'cubelles',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/cullera/',
                'title' => 'Кульера',
                'alias' => 'cullera',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/cumbre-del-sol/',
                'title' => 'Кумбре дель Соль',
                'alias' => 'cumbre-del-sol',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/cunit/',
                'title' => 'Кунит',
                'alias' => 'cunit',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/lalbir/',
                'title' => 'Л\'Альбир',
                'alias' => 'lalbir',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/la-zenia/',
                'title' => 'Ла Cения',
                'alias' => 'la-zenia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/la-marina/',
                'title' => 'Ла Марина',
                'alias' => 'la-marina',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/la-mata/',
                'title' => 'Ла Мата',
                'alias' => 'la-mata',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/la-cala-de-mijas/',
                'title' => 'Ла-Кала-де-Михас',
                'alias' => 'la-cala-de-mijas',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/la-caleta/',
                'title' => 'Ла-Калета',
                'alias' => 'la-caleta',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/la-nucia/',
                'title' => 'Ла-Нусиа',
                'alias' => 'la-nucia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/la-eliana/',
                'title' => 'Ла-Эльяна',
                'alias' => 'la-eliana',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/lerida/',
                'title' => 'Лерида',
                'alias' => 'lerida',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/los-alcazares/',
                'title' => 'Лос Алькасарес',
                'alias' => 'los-alcazares',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/los-montesinos/',
                'title' => 'Лос Монтесинос',
                'alias' => 'los-montesinos',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/los-cristianos/',
                'title' => 'Лос-Кристианос',
                'alias' => 'los-cristianos',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/lloret-de-mar/',
                'title' => 'Льорет-де-Мар',
                'alias' => 'lloret-de-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/madrid/',
                'title' => 'Мадрид',
                'alias' => 'madrid',
            ],
            [
                'url' => 'https://prian.ru/spain/balearic-islands/mallorca/',
                'title' => 'Майорка',
                'alias' => 'mallorca',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/malaga/',
                'title' => 'Малага',
                'alias' => 'malaga',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/marbella/',
                'title' => 'Марбелья',
                'alias' => 'marbella',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/mataro/',
                'title' => 'Матаро',
                'alias' => 'mataro',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/miami-platja/',
                'title' => 'Миами-Плайя',
                'alias' => 'miami-platja',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/mil-palmeras/',
                'title' => 'Миль-Пальмерас',
                'alias' => 'mil-palmeras',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/montroi/',
                'title' => 'Монтрой',
                'alias' => 'montroi',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/monforte-del-cid/',
                'title' => 'Монфорте-дель-Сид',
                'alias' => 'monforte-del-cid',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/moraira/',
                'title' => 'Морайра',
                'alias' => 'moraira',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/muro-de-alcoy/',
                'title' => 'Муро-де-Алькой',
                'alias' => 'muro-de-alcoy',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/murcia/',
                'title' => 'Мурсия',
                'alias' => 'murcia',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/mutxamel/',
                'title' => 'Мучамьель',
                'alias' => 'mutxamel',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/oliva/',
                'title' => 'Олива',
                'alias' => 'oliva',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/hondon-de-las-nieves/',
                'title' => 'Ондон-де-лас-Ньевес',
                'alias' => 'hondon-de-las-nieves',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/orba/',
                'title' => 'Орба',
                'alias' => 'orba',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/orihuela/',
                'title' => 'Ориуэла',
                'alias' => 'orihuela',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/orihuela-costa/',
                'title' => 'Ориуэла Коста',
                'alias' => 'orihuela-costa',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/cabo-de-palos/',
                'title' => 'Палос',
                'alias' => 'cabo-de-palos',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/palm-mar/',
                'title' => 'Пальм-Мар',
                'alias' => 'palm-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/lapalma/',
                'title' => 'Пальма',
                'alias' => 'lapalma',
            ],
            [
                'url' => 'https://prian.ru/spain/balearic-islands/palma-de-mallorca/',
                'title' => 'Пальма-де-Майорка',
                'alias' => 'palma-de-mallorca',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/parcent/',
                'title' => 'Парсент',
                'alias' => 'parcent',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/pego/',
                'title' => 'Пего',
                'alias' => 'pego',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/pedreguer/',
                'title' => 'Педрегер',
                'alias' => 'pedreguer',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/picassent/',
                'title' => 'Пикасент',
                'alias' => 'picassent',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/pilar-de-la-horadada/',
                'title' => 'Пилар-де-ла-Орадада',
                'alias' => 'pilar-de-la-horadada',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/platja-daro/',
                'title' => 'Плайя де Аро',
                'alias' => 'platja-daro',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/playa-san-juan/',
                'title' => 'Плайя де Сан-Хуан',
                'alias' => 'playa-san-juan',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/polop-de-la-marina/',
                'title' => 'Полоп',
                'alias' => 'polop-de-la-marina',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/premia-de-dalt/',
                'title' => 'Премия-де-Дальт',
                'alias' => 'premia-de-dalt',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/puerto-banus/',
                'title' => 'Пуэрто-Банус',
                'alias' => 'puerto-banus',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/el-rafol-d-almunia/',
                'title' => 'Рафоль-де-Альмуниа',
                'alias' => 'el-rafol-d-almunia',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/roses/',
                'title' => 'Росас',
                'alias' => 'roses',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/rossell/',
                'title' => 'Росель',
                'alias' => 'rossell',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/rojales/',
                'title' => 'Рохалес',
                'alias' => 'rojales',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/sabadell/',
                'title' => 'Сабадель',
                'alias' => 'sabadell',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/sagaro/',
                'title' => 'Сагаро',
                'alias' => 'sagaro',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/salou/',
                'title' => 'Салоу',
                'alias' => 'salou',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/san-andreu-de-llavaneras/',
                'title' => 'Сан-Андреу-де-Льяванерас',
                'alias' => 'san-andreu-de-llavaneras',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/sant-antoni-de-calonge/',
                'title' => 'Сан-Антони-де-Калонже',
                'alias' => 'sant-antoni-de-calonge',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/san-vicente-del-raspeig/',
                'title' => 'Сан-Висенте-дель-Распеч',
                'alias' => 'san-vicente-del-raspeig',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/san-cayetano/',
                'title' => 'Сан-Кайетано',
                'alias' => 'san-cayetano',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/san-miguel-de-salinas/',
                'title' => 'Сан-Мигель де Салинас',
                'alias' => 'san-miguel-de-salinas',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/san-pedro-del-pinatar/',
                'title' => 'Сан-Педро-дель-Пинатар',
                'alias' => 'san-pedro-del-pinatar',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/san-roque/',
                'title' => 'Сан-Роке',
                'alias' => 'san-roque',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/san-sebastian/',
                'title' => 'Сан-Себастьян',
                'alias' => 'san-sebastian',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/san-feliu-de-guixols/',
                'title' => 'Сан-Фелиу-де-Гишольс',
                'alias' => 'san-feliu-de-guixols',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/san-fulgencio/',
                'title' => 'Сан-Фульхенсио',
                'alias' => 'san-fulgencio',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/sant-joan-d-alacant/',
                'title' => 'Сан-Хуан-де-Аликанте',
                'alias' => 'sant-joan-d-alacant',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/san-juan-de-los-terreros/',
                'title' => 'Сан-Хуан-де-лос-Террерос',
                'alias' => 'san-juan-de-los-terreros',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/sanet-y-negrals/',
                'title' => 'Санет-и-Негральс',
                'alias' => 'sanet-y-negrals',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/sant-vicenc-de-montalt/',
                'title' => 'Сант-Висенс-де-Монтальт',
                'alias' => 'sant-vicenc-de-montalt',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/santa-cristina-d-aro/',
                'title' => 'Санта-Кристина-де-Аро',
                'alias' => 'santa-cristina-d-aro',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/santa-pola/',
                'title' => 'Санта-Пола',
                'alias' => 'santa-pola',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/santa-susanna/',
                'title' => 'Санта-Сусанна',
                'alias' => 'santa-susanna',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/santander/',
                'title' => 'Сантандер',
                'alias' => 'santander',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/santiago-de-la-ribera/',
                'title' => 'Сантьяго де ла Рибера',
                'alias' => 'santiago-de-la-ribera',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/segur-de-calafell/',
                'title' => 'Сегюр-де-Калафель',
                'alias' => 'segur-de-calafell',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/sitges/',
                'title' => 'Сиджес',
                'alias' => 'sitges',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/basque-country/',
                'title' => 'Странa Басков',
                'alias' => 'basque-country',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/sucina/',
                'title' => 'Сусина',
                'alias' => 'sucina',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/sierra-blanca/',
                'title' => 'Сьерра-Бланка',
                'alias' => 'sierra-blanca',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/ciudad-quesada/',
                'title' => 'Сьюдад-Кесада',
                'alias' => 'ciudad-quesada',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/tarragona/',
                'title' => 'Таррагона',
                'alias' => 'tarragona',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/teya/',
                'title' => 'Тейа',
                'alias' => 'teya',
            ],
            [
                'url' => 'https://prian.ru/spain/canary-islands/tenerife/',
                'title' => 'Тенерифе',
                'alias' => 'tenerife',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/teulada/',
                'title' => 'Теулада',
                'alias' => 'teulada',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/tiana/',
                'title' => 'Тиана',
                'alias' => 'tiana',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/tordera/',
                'title' => 'Тордера',
                'alias' => 'tordera',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/torre-de-la-horadada/',
                'title' => 'Торре де ла Орадада',
                'alias' => 'torre-de-la-horadada',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/torre-pacheco/',
                'title' => 'Торре-Пачеко',
                'alias' => 'torre-pacheco',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/torrevieja/',
                'title' => 'Торревьеха',
                'alias' => 'torrevieja',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/torrent/',
                'title' => 'Торренте',
                'alias' => 'torrent',
            ],
            [
                'url' => 'https://prian.ru/spain/catalonia/tossa-de-mar/',
                'title' => 'Тосса-де-Мар',
                'alias' => 'tossa-de-mar',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/finestrat/',
                'title' => 'Финестрат',
                'alias' => 'finestrat',
            ],
            [
                'url' => 'https://prian.ru/spain/central-and-northern-spain/formigal/',
                'title' => 'Формигал',
                'alias' => 'formigal',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/javea/',
                'title' => 'Хавея',
                'alias' => 'javea',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/jalon/',
                'title' => 'Халон',
                'alias' => 'jalon',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/chiva/',
                'title' => 'Чива',
                'alias' => 'chiva',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/el-raso/',
                'title' => 'Эль-Расо',
                'alias' => 'el-raso',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/els-poblets/',
                'title' => 'Эльс-Поблетс',
                'alias' => 'els-poblets',
            ],
            [
                'url' => 'https://prian.ru/spain/valencia-and-murcia/elche/',
                'title' => 'Эльче',
                'alias' => 'elche',
            ],
            [
                'url' => 'https://prian.ru/spain/andalusia/estepona/',
                'title' => 'Эстепона',
                'alias' => 'estepona',
            ],
        ],
        'url' => 'https://prian.ru/spain/',
        'title' => 'Испания',
        'alias' => 'spain',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/italy/central-italy/abruzzo/',
                'title' => 'Абруццо',
                'alias' => 'abruzzo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/alassio/',
                'title' => 'Алассио',
                'alias' => 'alassio',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/alessandria/',
                'title' => 'Алессандрия',
                'alias' => 'alessandria',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/albenga/',
                'title' => 'Альбенга',
                'alias' => 'albenga',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/amalfi/',
                'title' => 'Амальфи',
                'alias' => 'amalfi',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/ancona/',
                'title' => 'Анкона',
                'alias' => 'ancona',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/anzio/',
                'title' => 'Анцио',
                'alias' => 'anzio',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/apricale/',
                'title' => 'Априкале',
                'alias' => 'apricale',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/arezzo/',
                'title' => 'Ареццо',
                'alias' => 'arezzo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/arma-di-taggia/',
                'title' => 'Арма ди Таджа',
                'alias' => 'arma-di-taggia',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/ascoli-piceno/',
                'title' => 'Асколи Пичено',
                'alias' => 'ascoli-piceno',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/asti/',
                'title' => 'Асти',
                'alias' => 'asti',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/bari/',
                'title' => 'Бари',
                'alias' => 'bari',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/belvedere-marittimo/',
                'title' => 'Бельведере-Мариттимо',
                'alias' => 'belvedere-marittimo',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/bergamo/',
                'title' => 'Бергамо',
                'alias' => 'bergamo',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/biella/',
                'title' => 'Биелла',
                'alias' => 'biella',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/bordighera/',
                'title' => 'Бордигера',
                'alias' => 'bordighera',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/brescia/',
                'title' => 'Брешия',
                'alias' => 'brescia',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/busto-arsizio/',
                'title' => 'Бусто-Арсицио',
                'alias' => 'busto-arsizio',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/vallebona/',
                'title' => 'Валлебона',
                'alias' => 'vallebona',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/vallecrosia/',
                'title' => 'Валлекрозия',
                'alias' => 'vallecrosia',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/varese/',
                'title' => 'Варесе',
                'alias' => 'varese',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/veneto/',
                'title' => 'Венето',
                'alias' => 'veneto',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/venice/',
                'title' => 'Венеция',
                'alias' => 'venice',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/ventimiglia/',
                'title' => 'Вентимилья',
                'alias' => 'ventimiglia',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/verbania/',
                'title' => 'Вербания',
                'alias' => 'verbania',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/verona/',
                'title' => 'Верона',
                'alias' => 'verona',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/versilia/',
                'title' => 'Версилия',
                'alias' => 'versilia',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/viareggio/',
                'title' => 'Виареджо',
                'alias' => 'viareggio',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/vibo-valentia/',
                'title' => 'Вибо Валентия',
                'alias' => 'vibo-valentia',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/villasimius/',
                'title' => 'Виллазимиус',
                'alias' => 'villasimius',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/villafranca/',
                'title' => 'Виллафранка',
                'alias' => 'villafranca',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/volterra/',
                'title' => 'Вольтерра',
                'alias' => 'volterra',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lago-garda/',
                'title' => 'Гарда',
                'alias' => 'lago-garda',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/gardone-riviera/',
                'title' => 'Гардоне-Ривьера',
                'alias' => 'gardone-riviera',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/genova/',
                'title' => 'Генуя',
                'alias' => 'genova',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/golfo-aranci/',
                'title' => 'Гольфо-Аранчи',
                'alias' => 'golfo-aranci',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/grosseto/',
                'title' => 'Гроссето',
                'alias' => 'grosseto',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/guardistallo/',
                'title' => 'Гуардисталло',
                'alias' => 'guardistallo',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/zambrone/',
                'title' => 'Дзамброне',
                'alias' => 'zambrone',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/diamante/',
                'title' => 'Диаманте',
                'alias' => 'diamante',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/diano-marina/',
                'title' => 'Диано-Марина',
                'alias' => 'diano-marina',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/dolceacqua/',
                'title' => 'Дольчеаккуа',
                'alias' => 'dolceacqua',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lago-iseo/',
                'title' => 'Изео',
                'alias' => 'lago-iseo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/imperia/',
                'title' => 'Империя',
                'alias' => 'imperia',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/ischia/',
                'title' => 'Искья',
                'alias' => 'ischia',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/cagliari/',
                'title' => 'Кальяри',
                'alias' => 'cagliari',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/camaiore/',
                'title' => 'Камайоре',
                'alias' => 'camaiore',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/campione-d-italia/',
                'title' => 'Кампионе-д\'Италия',
                'alias' => 'campione-d-italia',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/cannigione/',
                'title' => 'Канниджионе',
                'alias' => 'cannigione',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/capri/',
                'title' => 'Капри',
                'alias' => 'capri',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/castelsardo/',
                'title' => 'Кастельсардо',
                'alias' => 'castelsardo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/castiglion-fiorentino/',
                'title' => 'Кастильон-Фиорентино',
                'alias' => 'castiglion-fiorentino',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/castiglione-della-pescaia/',
                'title' => 'Кастильоне-делла-Пеская',
                'alias' => 'castiglione-della-pescaia',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/castiglioncello/',
                'title' => 'Кастильончелло',
                'alias' => 'castiglioncello',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/catania/',
                'title' => 'Катания',
                'alias' => 'catania',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lago-como/',
                'title' => 'Комо',
                'alias' => 'lago-como',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/cortina-d-ampezzo/',
                'title' => 'Кортина Д\'Ампеццо',
                'alias' => 'cortina-d-ampezzo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/cortona/',
                'title' => 'Кортона',
                'alias' => 'cortona',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/costa-smeralda/',
                'title' => 'Коста-Смеральда',
                'alias' => 'costa-smeralda',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/crespina/',
                'title' => 'Креспина',
                'alias' => 'crespina',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/crotone/',
                'title' => 'Кротоне',
                'alias' => 'crotone',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/chianti/',
                'title' => 'Кьянти',
                'alias' => 'chianti',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/chianciano-terme/',
                'title' => 'Кьянчано-Терме',
                'alias' => 'chianciano-terme',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/lamaddalena/',
                'title' => 'Ла-Маддалена',
                'alias' => 'lamaddalena',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/latina/',
                'title' => 'Латина',
                'alias' => 'latina',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/lazio/',
                'title' => 'Лацио',
                'alias' => 'lazio',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/levanto/',
                'title' => 'Леванто',
                'alias' => 'levanto',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/lerici/',
                'title' => 'Леричи',
                'alias' => 'lerici',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/lecce/',
                'title' => 'Лечче',
                'alias' => 'lecce',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/livorno/',
                'title' => 'Ливорно',
                'alias' => 'livorno',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lido-di-jesolo/',
                'title' => 'Лидо ди Езоло',
                'alias' => 'lido-di-jesolo',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lignano-sabbiaboro/',
                'title' => 'Линьяно-Саббьядоро',
                'alias' => 'lignano-sabbiaboro',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/lipari/',
                'title' => 'Липари',
                'alias' => 'lipari',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lombardy/',
                'title' => 'Ломбардия',
                'alias' => 'lombardy',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lugano/',
                'title' => 'Лугано',
                'alias' => 'lugano',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/lucca/',
                'title' => 'Лукка',
                'alias' => 'lucca',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/lago-maggiore/',
                'title' => 'Маджоре',
                'alias' => 'lago-maggiore',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/pietrasanta/',
                'title' => 'Марина ди Пьетрасанта',
                'alias' => 'pietrasanta',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/marina-di-massa/',
                'title' => 'Марина-ди-Масса',
                'alias' => 'marina-di-massa',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/marche/',
                'title' => 'Марке',
                'alias' => 'marche',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/marsala/',
                'title' => 'Марсала',
                'alias' => 'marsala',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/massa/',
                'title' => 'Масса',
                'alias' => 'massa',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/menaggio/',
                'title' => 'Менаджо',
                'alias' => 'menaggio',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/menfi/',
                'title' => 'Менфи',
                'alias' => 'menfi',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/messina/',
                'title' => 'Мессина',
                'alias' => 'messina',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/milan/',
                'title' => 'Милан',
                'alias' => 'milan',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/molise/',
                'title' => 'Молизе',
                'alias' => 'molise',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/moniga-del-garda/',
                'title' => 'Монига-дель-Гарда',
                'alias' => 'moniga-del-garda',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/monopoli/',
                'title' => 'Монополи',
                'alias' => 'monopoli',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/monselice/',
                'title' => 'Монселиче',
                'alias' => 'monselice',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/montalcino/',
                'title' => 'Монтальчино',
                'alias' => 'montalcino',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/monte-argentario/',
                'title' => 'Монте-Арджентарио',
                'alias' => 'monte-argentario',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/montepulciano/',
                'title' => 'Монтепульчано',
                'alias' => 'montepulciano',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/montescudaio/',
                'title' => 'Монтескудайо',
                'alias' => 'montescudaio',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/noto/',
                'title' => 'Ното',
                'alias' => 'noto',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/nuoro/',
                'title' => 'Нуоро',
                'alias' => 'nuoro',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/olbia/',
                'title' => 'Ольбия',
                'alias' => 'olbia',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/olbia-tempio/',
                'title' => 'Ольбия-Темпьо',
                'alias' => 'olbia-tempio',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/ogliastra/',
                'title' => 'Ольястра',
                'alias' => 'ogliastra',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/orbetello/',
                'title' => 'Орбетелло',
                'alias' => 'orbetello',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/orvieto/',
                'title' => 'Орвието',
                'alias' => 'orvieto',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/oristano/',
                'title' => 'Ористано',
                'alias' => 'oristano',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/ospedaletti/',
                'title' => 'Оспедалетти',
                'alias' => 'ospedaletti',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/ostuni/',
                'title' => 'Остуни',
                'alias' => 'ostuni',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/padua/',
                'title' => 'Падуя',
                'alias' => 'padua',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/panicale/',
                'title' => 'Паникале',
                'alias' => 'panicale',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/parghelia/',
                'title' => 'Паргелия',
                'alias' => 'parghelia',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/pesaro/',
                'title' => 'Пезаро',
                'alias' => 'pesaro',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/perugia/',
                'title' => 'Перуджа',
                'alias' => 'perugia',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/pescara/',
                'title' => 'Пескара',
                'alias' => 'pescara',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/pisa/',
                'title' => 'Пиза',
                'alias' => 'pisa',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/pizzo/',
                'title' => 'Пиццо',
                'alias' => 'pizzo',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/ponte-di-legno/',
                'title' => 'Понте-ди-Леньо',
                'alias' => 'ponte-di-legno',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/porto-santo-stefano/',
                'title' => 'Порто Санто-Стефано',
                'alias' => 'porto-santo-stefano',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/porto-cervo/',
                'title' => 'Порто Черво',
                'alias' => 'porto-cervo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/portovenere/',
                'title' => 'Портовенере',
                'alias' => 'portovenere',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/portofino/',
                'title' => 'Портофино',
                'alias' => 'portofino',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/praia-a-mare/',
                'title' => 'Прая-а-Маре',
                'alias' => 'praia-a-mare',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/puglia/',
                'title' => 'Пулья',
                'alias' => 'puglia',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/punta-ala/',
                'title' => 'Пунта-Ала',
                'alias' => 'punta-ala',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/piedmont/',
                'title' => 'Пьемонт',
                'alias' => 'piedmont',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/pienza/',
                'title' => 'Пьенца',
                'alias' => 'pienza',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/piombino/',
                'title' => 'Пьомбино',
                'alias' => 'piombino',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/ragusa/',
                'title' => 'Рагуза',
                'alias' => 'ragusa',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/rapallo/',
                'title' => 'Рапалло',
                'alias' => 'rapallo',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/reggio-calabria/',
                'title' => 'Реджо-ди-Калабрия',
                'alias' => 'reggio-calabria',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/ricadi/',
                'title' => 'Рикади',
                'alias' => 'ricadi',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/rome/',
                'title' => 'Рим',
                'alias' => 'rome',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/rimini/',
                'title' => 'Римини',
                'alias' => 'rimini',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/rosignano-marittimo/',
                'title' => 'Розиньяно Мариттимо',
                'alias' => 'rosignano-marittimo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/roccamare/',
                'title' => 'Роккамаре',
                'alias' => 'roccamare',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/savona/',
                'title' => 'Савона',
                'alias' => 'savona',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/san-vincenzo/',
                'title' => 'Сан-Винченцо',
                'alias' => 'san-vincenzo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/san-gimignano/',
                'title' => 'Сан-Джиминьяно',
                'alias' => 'san-gimignano',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/san-nicola-arcella/',
                'title' => 'Сан-Никола-Арчелла',
                'alias' => 'san-nicola-arcella',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/san-remo/',
                'title' => 'Сан-Ремо',
                'alias' => 'san-remo',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/san-felice-circeo/',
                'title' => 'Сан-Феличе-Чирчео',
                'alias' => 'san-felice-circeo',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/santa-domenica-talao/',
                'title' => 'Санта-Доменика-Талао',
                'alias' => 'santa-domenica-talao',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/santa-margherita-ligure/',
                'title' => 'Санта-Маргерита-Лигуре',
                'alias' => 'santa-margherita-ligure',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/santa-maria-del-cedro/',
                'title' => 'Санта-Мария дель Чедро',
                'alias' => 'santa-maria-del-cedro',
            ],
            [
                'url' => 'https://prian.ru/italy/sardinia/sassari/',
                'title' => 'Сассари',
                'alias' => 'sassari',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/siena/',
                'title' => 'Сиена',
                'alias' => 'siena',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/siracusa/',
                'title' => 'Сиракузы',
                'alias' => 'siracusa',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/scalea/',
                'title' => 'Скалея',
                'alias' => 'scalea',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/soverato/',
                'title' => 'Соверато',
                'alias' => 'soverato',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/la-spezia/',
                'title' => 'Специя',
                'alias' => 'la-spezia',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/stresa/',
                'title' => 'Стреза',
                'alias' => 'stresa',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/teramo/',
                'title' => 'Терамо',
                'alias' => 'teramo',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/terni/',
                'title' => 'Терни',
                'alias' => 'terni',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/terracina/',
                'title' => 'Террачина',
                'alias' => 'terracina',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/tortora/',
                'title' => 'Тортора',
                'alias' => 'tortora',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/trapani/',
                'title' => 'Трапани',
                'alias' => 'trapani',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/trentino-alto-adige/',
                'title' => 'Трентино - Альто-Адидже',
                'alias' => 'trentino-alto-adige',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/trieste/',
                'title' => 'Триест',
                'alias' => 'trieste',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/tropea/',
                'title' => 'Тропея',
                'alias' => 'tropea',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/turin/',
                'title' => 'Турин',
                'alias' => 'turin',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/tuscania/',
                'title' => 'Тускания',
                'alias' => 'tuscania',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/umbria/',
                'title' => 'Умбрия',
                'alias' => 'umbria',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/favignana/',
                'title' => 'Фавиньяна',
                'alias' => 'favignana',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/fermo/',
                'title' => 'Фермо',
                'alias' => 'fermo',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/ferrara/',
                'title' => 'Феррара',
                'alias' => 'ferrara',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/florence/',
                'title' => 'Флоренция',
                'alias' => 'florence',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/forte-dei-marmi/',
                'title' => 'Форте деи Марми',
                'alias' => 'forte-dei-marmi',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/fuscaldo/',
                'title' => 'Фускальдо',
                'alias' => 'fuscaldo',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/ceriana/',
                'title' => 'Чериана',
                'alias' => 'ceriana',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/cecina/',
                'title' => 'Чечина',
                'alias' => 'cecina',
            ],
            [
                'url' => 'https://prian.ru/italy/central-italy/civitanova-marche/',
                'title' => 'Чивитанова-Марке',
                'alias' => 'civitanova-marche',
            ],
            [
                'url' => 'https://prian.ru/italy/calabria/cirella/',
                'title' => 'Чирелла',
                'alias' => 'cirella',
            ],
            [
                'url' => 'https://prian.ru/italy/liguria-and-tuscany/elba/',
                'title' => 'Эльба',
                'alias' => 'elba',
            ],
            [
                'url' => 'https://prian.ru/italy/northern-italy/emilia-romagna/',
                'title' => 'Эмилия-Романья',
                'alias' => 'emilia-romagna',
            ],
            [
                'url' => 'https://prian.ru/italy/sicily/enna/',
                'title' => 'Энна',
                'alias' => 'enna',
            ],
        ],
        'url' => 'https://prian.ru/italy/',
        'title' => 'Италия',
        'alias' => 'italy',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/cape-verde/',
        'title' => 'Кабо-Верде',
        'alias' => 'cape-verde',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/kazakhstan/',
        'title' => 'Казахстан',
        'alias' => 'kazakhstan',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/cambodia/',
        'title' => 'Камбоджа',
        'alias' => 'cambodia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/canada/quebec/montreal/',
                'title' => 'Монреаль',
                'alias' => 'montreal',
            ],
            [
                'url' => 'https://prian.ru/canada/quebec/aurora/',
                'title' => 'Орора',
                'alias' => 'aurora',
            ],
            [
                'url' => 'https://prian.ru/canada/ontario/ottawa/',
                'title' => 'Оттава',
                'alias' => 'ottawa',
            ],
            [
                'url' => 'https://prian.ru/canada/quebec/salaberry-de-valleyfield/',
                'title' => 'Салаберри-де-Валлифилд',
                'alias' => 'salaberry-de-valleyfield',
            ],
            [
                'url' => 'https://prian.ru/canada/ontario/toronto/',
                'title' => 'Торонто',
                'alias' => 'toronto',
            ],
        ],
        'url' => 'https://prian.ru/canada/',
        'title' => 'Канада',
        'alias' => 'canada',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/caribbean-islands/caribbean-islands/anguilla/',
                'title' => 'Ангилья',
                'alias' => 'anguilla',
            ],
            [
                'url' => 'https://prian.ru/caribbean-islands/caribbean-islands/antigua-and-barbuda/',
                'title' => 'Антигуа и Барбуда',
                'alias' => 'antigua-and-barbuda',
            ],
            [
                'url' => 'https://prian.ru/caribbean-islands/caribbean-islands/grenada/',
                'title' => 'Гренада',
                'alias' => 'grenada',
            ],
            [
                'url' => 'https://prian.ru/caribbean-islands/caribbean-islands/roatan/',
                'title' => 'Роатан',
                'alias' => 'roatan',
            ],
            [
                'url' => 'https://prian.ru/caribbean-islands/caribbean-islands/saint-kitts-and-nevis/',
                'title' => 'Сент-Китс и Невис',
                'alias' => 'saint-kitts-and-nevis',
            ],
        ],
        'url' => 'https://prian.ru/caribbean-islands/',
        'title' => 'Карибские острова',
        'alias' => 'caribbean-islands',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/cyprus/cyprus/agia-napa/',
                'title' => 'Айя-Напа',
                'alias' => 'agia-napa',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/argaka/',
                'title' => 'Аргака',
                'alias' => 'argaka',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/konia/',
                'title' => 'Кония',
                'alias' => 'konia',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/coral-bay/',
                'title' => 'Коралловый залив',
                'alias' => 'coral-bay',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/larnaca/',
                'title' => 'Ларнака',
                'alias' => 'larnaca',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/latchi/',
                'title' => 'Латхи',
                'alias' => 'latchi',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/limassol/',
                'title' => 'Лимассол',
                'alias' => 'limassol',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/mesa-chorio/',
                'title' => 'Меса Хорье',
                'alias' => 'mesa-chorio',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/neo-chorio/',
                'title' => 'Нео Хорио',
                'alias' => 'neo-chorio',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/nicosia/',
                'title' => 'Никосия',
                'alias' => 'nicosia',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/paralimni/',
                'title' => 'Паралимни',
                'alias' => 'paralimni',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/paphos/',
                'title' => 'Пафос',
                'alias' => 'paphos',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/peyia/',
                'title' => 'Пейя',
                'alias' => 'peyia',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/pissouri/',
                'title' => 'Писсури',
                'alias' => 'pissouri',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/polis/',
                'title' => 'Полис',
                'alias' => 'polis',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/protaras/',
                'title' => 'Протарас',
                'alias' => 'protaras',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/tala/',
                'title' => 'Тала',
                'alias' => 'tala',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/tremithousa/',
                'title' => 'Тремитуса',
                'alias' => 'tremithousa',
            ],
            [
                'url' => 'https://prian.ru/cyprus/cyprus/tsada/',
                'title' => 'Тсада',
                'alias' => 'tsada',
            ],
        ],
        'url' => 'https://prian.ru/cyprus/',
        'title' => 'Кипр',
        'alias' => 'cyprus',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/china/',
        'title' => 'Китай',
        'alias' => 'china',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/kyrgyzatan/',
        'title' => 'Кыргызстан',
        'alias' => 'kyrgyzatan',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/latvia/latvia/adazi-district/',
                'title' => 'Адажский край',
                'alias' => 'adazi-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/aizkraukle-district/',
                'title' => 'Айзкраукльский край',
                'alias' => 'aizkraukle-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/aluksne-district/',
                'title' => 'Алуксненский край',
                'alias' => 'aluksne-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/babite/',
                'title' => 'Бабите',
                'alias' => 'babite',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/bauska-district/',
                'title' => 'Бауский край',
                'alias' => 'bauska-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/valmiera-district/',
                'title' => 'Валмиерский край',
                'alias' => 'valmiera-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/ventspils/',
                'title' => 'Вентспилс',
                'alias' => 'ventspils',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/garkalne-district/',
                'title' => 'Гаркалнский край',
                'alias' => 'garkalne-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/gulbene-district/',
                'title' => 'Гулбенский край',
                'alias' => 'gulbene-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/daugavpils-district/',
                'title' => 'Даугавпилсский край',
                'alias' => 'daugavpils-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/jelgava/',
                'title' => 'Елгава',
                'alias' => 'jelgava',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/kuldiga-district/',
                'title' => 'Кулдигский край',
                'alias' => 'kuldiga-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/liepaja-district/',
                'title' => 'Лиепайский край',
                'alias' => 'liepaja-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/liepaja/',
                'title' => 'Лиепая',
                'alias' => 'liepaja',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/limbazi-district/',
                'title' => 'Лимбажский край',
                'alias' => 'limbazi-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/ludza-district/',
                'title' => 'Лудзенский край',
                'alias' => 'ludza-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/marupe/',
                'title' => 'Марупе',
                'alias' => 'marupe',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/marupe-district/',
                'title' => 'Марупский край',
                'alias' => 'marupe-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/ogre/',
                'title' => 'Огре',
                'alias' => 'ogre',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/ogre-district/',
                'title' => 'Огрский край',
                'alias' => 'ogre-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/ozolnieki-district/',
                'title' => 'Озолниекский край',
                'alias' => 'ozolnieki-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/pinki/',
                'title' => 'Пиньки',
                'alias' => 'pinki',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/riga/',
                'title' => 'Рига',
                'alias' => 'riga',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/riga-district/',
                'title' => 'Рижский край',
                'alias' => 'riga-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/roja/',
                'title' => 'Роя',
                'alias' => 'roja',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/salaspils/',
                'title' => 'Саласпилс',
                'alias' => 'salaspils',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/salaspils-district/',
                'title' => 'Саласпилсский край',
                'alias' => 'salaspils-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/saulkrasti/',
                'title' => 'Саулкрасты',
                'alias' => 'saulkrasti',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/sigulda/',
                'title' => 'Сигулда',
                'alias' => 'sigulda',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/stopini-district/',
                'title' => 'Стопиньский край',
                'alias' => 'stopini-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/talsi-district/',
                'title' => 'Талсинский край',
                'alias' => 'talsi-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/carnikava/',
                'title' => 'Царникава',
                'alias' => 'carnikava',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/cesis-district/',
                'title' => 'Цесисский край',
                'alias' => 'cesis-district',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/jumprava-parish/',
                'title' => 'Юмправская волость',
                'alias' => 'jumprava-parish',
            ],
            [
                'url' => 'https://prian.ru/latvia/latvia/jurmala/',
                'title' => 'Юрмала',
                'alias' => 'jurmala',
            ],
        ],
        'url' => 'https://prian.ru/latvia/',
        'title' => 'Латвия',
        'alias' => 'latvia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/lithuania/lithuania/vilnius/',
                'title' => 'Вильнюс',
                'alias' => 'vilnius',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/ignalina/',
                'title' => 'Игналина',
                'alias' => 'ignalina',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/kaunas/',
                'title' => 'Каунас',
                'alias' => 'kaunas',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/klaipeda/',
                'title' => 'Клайпеда',
                'alias' => 'klaipeda',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/moletai/',
                'title' => 'Молетай',
                'alias' => 'moletai',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/palanga/',
                'title' => 'Паланга',
                'alias' => 'palanga',
            ],
            [
                'url' => 'https://prian.ru/lithuania/lithuania/trakai/',
                'title' => 'Тракай',
                'alias' => 'trakai',
            ],
        ],
        'url' => 'https://prian.ru/lithuania/',
        'title' => 'Литва',
        'alias' => 'lithuania',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/luxembourg/luxembourg/derenbach/',
                'title' => 'Деренбах',
                'alias' => 'derenbach',
            ],
            [
                'url' => 'https://prian.ru/luxembourg/luxembourg/dikrech/',
                'title' => 'Дикирх',
                'alias' => 'dikrech',
            ],
            [
                'url' => 'https://prian.ru/luxembourg/luxembourg/lellig/',
                'title' => 'Леллиг',
                'alias' => 'lellig',
            ],
        ],
        'url' => 'https://prian.ru/luxembourg/',
        'title' => 'Люксембург',
        'alias' => 'luxembourg',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/mauritius/',
        'title' => 'Маврикий',
        'alias' => 'mauritius',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/maldives/',
        'title' => 'Мальдивы',
        'alias' => 'maldives',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/malta/malta/ghajnsielem/',
                'title' => 'Айнсилем',
                'alias' => 'ghajnsielem',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/qawra/',
                'title' => 'Аура',
                'alias' => 'qawra',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/birgu/',
                'title' => 'Биргу',
                'alias' => 'birgu',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/valetta/',
                'title' => 'Валлетта',
                'alias' => 'valetta',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/zurrieq/',
                'title' => 'Зуррик',
                'alias' => 'zurrieq',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/madliena/',
                'title' => 'Мадлиена',
                'alias' => 'madliena',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/mellieha/',
                'title' => 'Меллиха',
                'alias' => 'mellieha',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/naxxar/',
                'title' => 'Нашшар',
                'alias' => 'naxxar',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/san-lawrenz/',
                'title' => 'Сан-Лоренц',
                'alias' => 'san-lawrenz',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/swieqi/',
                'title' => 'Свийи',
                'alias' => 'swieqi',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/st-julians/',
                'title' => 'Сент-Джулианс',
                'alias' => 'st-julians',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/sliema/',
                'title' => 'Слима',
                'alias' => 'sliema',
            ],
            [
                'url' => 'https://prian.ru/malta/malta/xemxija/',
                'title' => 'Шемшия',
                'alias' => 'xemxija',
            ],
        ],
        'url' => 'https://prian.ru/malta/',
        'title' => 'Мальта',
        'alias' => 'malta',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/morocco/morocco/agadir/',
                'title' => 'Агадир',
                'alias' => 'agadir',
            ],
        ],
        'url' => 'https://prian.ru/morocco/',
        'title' => 'Марокко',
        'alias' => 'morocco',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/mexico/',
        'title' => 'Мексика',
        'alias' => 'mexico',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/moldavia/',
        'title' => 'Молдавия',
        'alias' => 'moldavia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/monaco/monaco/la-condamine/',
                'title' => 'Ла-Кондамин',
                'alias' => 'la-condamine',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/les-revoires/',
                'title' => 'Ле-Ревуар',
                'alias' => 'les-revoires',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/monaco/',
                'title' => 'Монако',
                'alias' => 'monaco',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/les-moneghetti/',
                'title' => 'Монегетти',
                'alias' => 'les-moneghetti',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/monte-carlo/',
                'title' => 'Монте Карло',
                'alias' => 'monte-carlo',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/saint-roman/',
                'title' => 'Сен-Роман',
                'alias' => 'saint-roman',
            ],
            [
                'url' => 'https://prian.ru/monaco/monaco/fontvieille/',
                'title' => 'Фонвьей',
                'alias' => 'fontvieille',
            ],
        ],
        'url' => 'https://prian.ru/monaco/',
        'title' => 'Монако',
        'alias' => 'monaco',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/netherlands/north-holland/amsterdam/',
                'title' => 'Амстердам',
                'alias' => 'amsterdam',
            ],
            [
                'url' => 'https://prian.ru/netherlands/south-holland/den-haag/',
                'title' => 'Гаага',
                'alias' => 'den-haag',
            ],
            [
                'url' => 'https://prian.ru/netherlands/north-holland/zaandam/',
                'title' => 'Зандам',
                'alias' => 'zaandam',
            ],
            [
                'url' => 'https://prian.ru/netherlands/north-holland/naarden/',
                'title' => 'Нарден',
                'alias' => 'naarden',
            ],
            [
                'url' => 'https://prian.ru/netherlands/south-holland/rotterdam/',
                'title' => 'Роттердам',
                'alias' => 'rotterdam',
            ],
            [
                'url' => 'https://prian.ru/netherlands/north-holland/hilversum/',
                'title' => 'Хилверсюм',
                'alias' => 'hilversum',
            ],
        ],
        'url' => 'https://prian.ru/netherlands/',
        'title' => 'Нидерланды',
        'alias' => 'netherlands',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/norway/norway/lofoten/',
                'title' => 'Лофотенские острова',
                'alias' => 'lofoten',
            ],
            [
                'url' => 'https://prian.ru/norway/norway/oslo/',
                'title' => 'Осло',
                'alias' => 'oslo',
            ],
        ],
        'url' => 'https://prian.ru/norway/',
        'title' => 'Норвегия',
        'alias' => 'norway',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/united-arab-emirates/united-arab-emirates/abu-dhabi/',
                'title' => 'Абу-Даби',
                'alias' => 'abu-dhabi',
            ],
            [
                'url' => 'https://prian.ru/united-arab-emirates/united-arab-emirates/dubai/',
                'title' => 'Дубай',
                'alias' => 'dubai',
            ],
            [
                'url' => 'https://prian.ru/united-arab-emirates/united-arab-emirates/ras-al-khaimah/',
                'title' => 'Рас-эль-Хайма',
                'alias' => 'ras-al-khaimah',
            ],
            [
                'url' => 'https://prian.ru/united-arab-emirates/united-arab-emirates/sharjah/',
                'title' => 'Шарджа',
                'alias' => 'sharjah',
            ],
        ],
        'url' => 'https://prian.ru/united-arab-emirates/',
        'title' => 'ОАЭ',
        'alias' => 'united-arab-emirates',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/panama/',
        'title' => 'Панама',
        'alias' => 'panama',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/poland/masovian-voivodeship/warszawa/',
                'title' => 'Варшава',
                'alias' => 'warszawa',
            ],
            [
                'url' => 'https://prian.ru/poland/lower-silesian-voivodeship/wroclaw/',
                'title' => 'Вроцлав',
                'alias' => 'wroclaw',
            ],
            [
                'url' => 'https://prian.ru/poland/masovian-voivodeship/lodz/',
                'title' => 'Лодзь',
                'alias' => 'lodz',
            ],
        ],
        'url' => 'https://prian.ru/poland/',
        'title' => 'Польша',
        'alias' => 'poland',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/portugal/portugal/aveiro/',
                'title' => 'Авейру',
                'alias' => 'aveiro',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/albufeira/',
                'title' => 'Албуфейра',
                'alias' => 'albufeira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/alvor/',
                'title' => 'Алвор',
                'alias' => 'alvor',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/algarve/',
                'title' => 'Алгарве',
                'alias' => 'algarve',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/almancil/',
                'title' => 'Алмансил',
                'alias' => 'almancil',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/amora/',
                'title' => 'Амора',
                'alias' => 'amora',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/beja/',
                'title' => 'Бежа',
                'alias' => 'beja',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/braga/',
                'title' => 'Брага',
                'alias' => 'braga',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/vilamoura/',
                'title' => 'Виламора',
                'alias' => 'vilamoura',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/caldas-da-rainha/',
                'title' => 'Калдаш-да-Раинья',
                'alias' => 'caldas-da-rainha',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/carvoeiro/',
                'title' => 'Карвоэйро',
                'alias' => 'carvoeiro',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/carcavelos/',
                'title' => 'Каркавелуш',
                'alias' => 'carcavelos',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/quarteira/',
                'title' => 'Картейра',
                'alias' => 'quarteira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/cascais/',
                'title' => 'Кашкайш',
                'alias' => 'cascais',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/comporta/',
                'title' => 'Компорта',
                'alias' => 'comporta',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/lagoa/',
                'title' => 'Лагоа',
                'alias' => 'lagoa',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/lagos/',
                'title' => 'Лагуш',
                'alias' => 'lagos',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/lisbon/',
                'title' => 'Лиссабон',
                'alias' => 'lisbon',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/loule/',
                'title' => 'Лоле',
                'alias' => 'loule',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/madeira/',
                'title' => 'Мадейра',
                'alias' => 'madeira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/mira/',
                'title' => 'Мира',
                'alias' => 'mira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/nazare/',
                'title' => 'Назаре',
                'alias' => 'nazare',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/obidos/',
                'title' => 'Обидуш',
                'alias' => 'obidos',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/olhos-de-agua/',
                'title' => 'Ольюш-де-Агуа',
                'alias' => 'olhos-de-agua',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/paul-do-mar/',
                'title' => 'Паул-ду-Мар',
                'alias' => 'paul-do-mar',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/peniche/',
                'title' => 'Пениши',
                'alias' => 'peniche',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/portimao/',
                'title' => 'Портиман',
                'alias' => 'portimao',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/porto/',
                'title' => 'Порту',
                'alias' => 'porto',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/praia-do-carvoeiro/',
                'title' => 'Прая-ду-Карвоэйру',
                'alias' => 'praia-do-carvoeiro',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/setubal/',
                'title' => 'Сетубал',
                'alias' => 'setubal',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/silves/',
                'title' => 'Силвеш',
                'alias' => 'silves',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/sintra/',
                'title' => 'Синтра',
                'alias' => 'sintra',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/tavira/',
                'title' => 'Тавира',
                'alias' => 'tavira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/faro/',
                'title' => 'Фару',
                'alias' => 'faro',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/funchal/',
                'title' => 'Фуншал',
                'alias' => 'funchal',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/ericeira/',
                'title' => 'Эрисейра',
                'alias' => 'ericeira',
            ],
            [
                'url' => 'https://prian.ru/portugal/portugal/estoril/',
                'title' => 'Эшторил',
                'alias' => 'estoril',
            ],
        ],
        'url' => 'https://prian.ru/portugal/',
        'title' => 'Португалия',
        'alias' => 'portugal',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/roumania/',
        'title' => 'Румыния',
        'alias' => 'roumania',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/serbia/belgrade/beograd/',
                'title' => 'Белград',
                'alias' => 'beograd',
            ],
            [
                'url' => 'https://prian.ru/serbia/vojvodina/novi-sad/',
                'title' => 'Нови-Сад',
                'alias' => 'novi-sad',
            ],
        ],
        'url' => 'https://prian.ru/serbia/',
        'title' => 'Сербия',
        'alias' => 'serbia',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/singapore/',
        'title' => 'Сингапур',
        'alias' => 'singapore',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/slovakia/bratislava-region/bratislava/',
                'title' => 'Братислава',
                'alias' => 'bratislava',
            ],
            [
                'url' => 'https://prian.ru/slovakia/kosice-region/kosice/',
                'title' => 'Кошице',
                'alias' => 'kosice',
            ],
            [
                'url' => 'https://prian.ru/slovakia/zilina-region/liptovsky-mikulas/',
                'title' => 'Липтовски-Микулаш',
                'alias' => 'liptovsky-mikulas',
            ],
        ],
        'url' => 'https://prian.ru/slovakia/',
        'title' => 'Словакия',
        'alias' => 'slovakia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ajdovscina/',
                'title' => 'Айдовшчина',
                'alias' => 'ajdovscina',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ankaran/',
                'title' => 'Анкаран',
                'alias' => 'ankaran',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/bezigrad/',
                'title' => 'Бежиград',
                'alias' => 'bezigrad',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/beltinci/',
                'title' => 'Белтинци',
                'alias' => 'beltinci',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/bled/',
                'title' => 'Блед',
                'alias' => 'bled',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/bovec/',
                'title' => 'Бовец',
                'alias' => 'bovec',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/bohinj/',
                'title' => 'Бохинь',
                'alias' => 'bohinj',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/brezice/',
                'title' => 'Брежице',
                'alias' => 'brezice',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/brezovica/',
                'title' => 'Брезовица',
                'alias' => 'brezovica',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/velenje/',
                'title' => 'Веленье',
                'alias' => 'velenje',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/vipava/',
                'title' => 'Випава',
                'alias' => 'vipava',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/vodice/',
                'title' => 'Водице',
                'alias' => 'vodice',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/vransko/',
                'title' => 'Вранско',
                'alias' => 'vransko',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/vrhnika/',
                'title' => 'Врхника',
                'alias' => 'vrhnika',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/gornja-radgona/',
                'title' => 'Горня-Радгона',
                'alias' => 'gornja-radgona',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/grosuplje/',
                'title' => 'Гросупле',
                'alias' => 'grosuplje',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/domzale/',
                'title' => 'Домжале',
                'alias' => 'domzale',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/jesenice/',
                'title' => 'Есенице',
                'alias' => 'jesenice',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/zalec/',
                'title' => 'Жалец',
                'alias' => 'zalec',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/zgornja-kungota/',
                'title' => 'Згорня Кунгота',
                'alias' => 'zgornja-kungota',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/zrece/',
                'title' => 'Зрече',
                'alias' => 'zrece',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/izola/',
                'title' => 'Изола',
                'alias' => 'izola',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/kamnik/',
                'title' => 'Камник',
                'alias' => 'kamnik',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/kozina/',
                'title' => 'Козина',
                'alias' => 'kozina',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/koper/',
                'title' => 'Копер',
                'alias' => 'koper',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/kocevje/',
                'title' => 'Кочевье',
                'alias' => 'kocevje',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/kranj/',
                'title' => 'Крань',
                'alias' => 'kranj',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/kranjska-gora/',
                'title' => 'Краньска Гора',
                'alias' => 'kranjska-gora',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/krsko/',
                'title' => 'Кршко',
                'alias' => 'krsko',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/lasko/',
                'title' => 'Лашко',
                'alias' => 'lasko',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/lenart/',
                'title' => 'Ленарт',
                'alias' => 'lenart',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/lendava/',
                'title' => 'Лендава',
                'alias' => 'lendava',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/litija/',
                'title' => 'Лития',
                'alias' => 'litija',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/logatec/',
                'title' => 'Логатец',
                'alias' => 'logatec',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/lukovica-pri-domzalah/',
                'title' => 'Луковица при Домжалах',
                'alias' => 'lukovica-pri-domzalah',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/lucija/',
                'title' => 'Луция',
                'alias' => 'lucija',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ljubljana/',
                'title' => 'Любляна',
                'alias' => 'ljubljana',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ljutomer/',
                'title' => 'Лютомер',
                'alias' => 'ljutomer',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/malecnik/',
                'title' => 'Малечник',
                'alias' => 'malecnik',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/maribor/',
                'title' => 'Марибор',
                'alias' => 'maribor',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/medvode/',
                'title' => 'Медводе',
                'alias' => 'medvode',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/moravske-toplice/',
                'title' => 'Моравске-Топлице',
                'alias' => 'moravske-toplice',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/murska-sobota/',
                'title' => 'Мурска-Собота',
                'alias' => 'murska-sobota',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/muta/',
                'title' => 'Мута',
                'alias' => 'muta',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/nova-gorica/',
                'title' => 'Нова-Горица',
                'alias' => 'nova-gorica',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/novo-mesto/',
                'title' => 'Ново-Место',
                'alias' => 'novo-mesto',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ormoz/',
                'title' => 'Ормож',
                'alias' => 'ormoz',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/pekre/',
                'title' => 'Пекре',
                'alias' => 'pekre',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/pivka/',
                'title' => 'Пивка',
                'alias' => 'pivka',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/piran/',
                'title' => 'Пиран',
                'alias' => 'piran',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/podcetrtek/',
                'title' => 'Подчетртек',
                'alias' => 'podcetrtek',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/portoroz/',
                'title' => 'Порторож',
                'alias' => 'portoroz',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/postojna/',
                'title' => 'Постойна',
                'alias' => 'postojna',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/preddvor/',
                'title' => 'Преддвор',
                'alias' => 'preddvor',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ptuj/',
                'title' => 'Птуй',
                'alias' => 'ptuj',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/radenci/',
                'title' => 'Раденцы',
                'alias' => 'radenci',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/radovljica/',
                'title' => 'Радовлица',
                'alias' => 'radovljica',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/rogatec/',
                'title' => 'Рогатец',
                'alias' => 'rogatec',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/rogaska-slatina/',
                'title' => 'Рогашка-Слатина',
                'alias' => 'rogaska-slatina',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/ruse/',
                'title' => 'Руше',
                'alias' => 'ruse',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/sevnica/',
                'title' => 'Севница',
                'alias' => 'sevnica',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/sezana/',
                'title' => 'Сежана',
                'alias' => 'sezana',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/slovenska-bistrica/',
                'title' => 'Словенска-Бистрица',
                'alias' => 'slovenska-bistrica',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/slovenske-konjice/',
                'title' => 'Словенске-Конице',
                'alias' => 'slovenske-konjice',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/slovenj-gradec/',
                'title' => 'Словень-Градец',
                'alias' => 'slovenj-gradec',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/spodnja-kungota/',
                'title' => 'Сподня Кунгота',
                'alias' => 'spodnja-kungota',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/tolmin/',
                'title' => 'Толмин',
                'alias' => 'tolmin',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/trbovlje/',
                'title' => 'Трбовле',
                'alias' => 'trbovlje',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/trzic/',
                'title' => 'Тржич',
                'alias' => 'trzic',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/trzin/',
                'title' => 'Трзин',
                'alias' => 'trzin',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/celje/',
                'title' => 'Целе',
                'alias' => 'celje',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/crnomelj/',
                'title' => 'Чрномель',
                'alias' => 'crnomelj',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/sentjur/',
                'title' => 'Шентюр',
                'alias' => 'sentjur',
            ],
            [
                'url' => 'https://prian.ru/slovenia/slovenia/skofja-loka/',
                'title' => 'Шкофья-Лока',
                'alias' => 'skofja-loka',
            ],
        ],
        'url' => 'https://prian.ru/slovenia/',
        'title' => 'Словения',
        'alias' => 'slovenia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/usa/florida/aventura/',
                'title' => 'Авентура',
                'alias' => 'aventura',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/atlanta/',
                'title' => 'Атланта',
                'alias' => 'atlanta',
            ],
            [
                'url' => 'https://prian.ru/usa/maryland/baltimore/',
                'title' => 'Балтимор',
                'alias' => 'baltimore',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/bay-harbor-islands/',
                'title' => 'Бей-Харбор-Айлендс',
                'alias' => 'bay-harbor-islands',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/boynton-beach/',
                'title' => 'Бойнтон-Бич',
                'alias' => 'boynton-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/boca-raton/',
                'title' => 'Бока-Ратон',
                'alias' => 'boca-raton',
            ],
            [
                'url' => 'https://prian.ru/usa/new-york/brooklyn/',
                'title' => 'Бруклин',
                'alias' => 'brooklyn',
            ],
            [
                'url' => 'https://prian.ru/usa/new-york/buffalo/',
                'title' => 'Буффало',
                'alias' => 'buffalo',
            ],
            [
                'url' => 'https://prian.ru/usa/california/hollywood/',
                'title' => 'Голливуд',
                'alias' => 'hollywood',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/hollywood-florida/',
                'title' => 'Голливуд (Флорида)',
                'alias' => 'hollywood-florida',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/dania-beach/',
                'title' => 'Дания Бич',
                'alias' => 'dania-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/davie/',
                'title' => 'Дейви',
                'alias' => 'davie',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/delray-beach/',
                'title' => 'Делрей-Бич',
                'alias' => 'delray-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/colorado/denver/',
                'title' => 'Денвер',
                'alias' => 'denver',
            ],
            [
                'url' => 'https://prian.ru/usa/illinois/des-plaines/',
                'title' => 'Дес-Плейнс',
                'alias' => 'des-plaines',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/detroit/',
                'title' => 'Детройт',
                'alias' => 'detroit',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/deerfield-beach/',
                'title' => 'Дирфилд-Бич',
                'alias' => 'deerfield-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/doral/',
                'title' => 'Дорал',
                'alias' => 'doral',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/yonkers/',
                'title' => 'Йонкерс',
                'alias' => 'yonkers',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/cutler-bay/',
                'title' => 'Катлер-Бей',
                'alias' => 'cutler-bay',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/kendall/',
                'title' => 'Кендалл',
                'alias' => 'kendall',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/cleveland/',
                'title' => 'Кливленд',
                'alias' => 'cleveland',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/coconut-creek/',
                'title' => 'Коконат Крик',
                'alias' => 'coconut-creek',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/coral-gables/',
                'title' => 'Корал Гейблс',
                'alias' => 'coral-gables',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/coral-springs/',
                'title' => 'Корал-Спрингс',
                'alias' => 'coral-springs',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/cooper-city/',
                'title' => 'Купер-Сити',
                'alias' => 'cooper-city',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/lighthouse-point/',
                'title' => 'Лайтхаус-Пойнт',
                'alias' => 'lighthouse-point',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/las-vegas/',
                'title' => 'Лас-Вегас',
                'alias' => 'las-vegas',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/lake-worth/',
                'title' => 'Лейк Уорт',
                'alias' => 'lake-worth',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/lauderhill/',
                'title' => 'Лодерхилл',
                'alias' => 'lauderhill',
            ],
            [
                'url' => 'https://prian.ru/usa/texas/longview/',
                'title' => 'Лонгвью',
                'alias' => 'longview',
            ],
            [
                'url' => 'https://prian.ru/usa/california/los-angeles/',
                'title' => 'Лос-Анджелес',
                'alias' => 'los-angeles',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/miami/',
                'title' => 'Майами',
                'alias' => 'miami',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/miami-beach/',
                'title' => 'Майами-Бич',
                'alias' => 'miami-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/miami-lakes/',
                'title' => 'Майами-Лейкс',
                'alias' => 'miami-lakes',
            ],
            [
                'url' => 'https://prian.ru/usa/new-york/manhattan/',
                'title' => 'Манхэттен',
                'alias' => 'manhattan',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/margate/',
                'title' => 'Маргейт',
                'alias' => 'margate',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/merritt-island/',
                'title' => 'Мерритт-Айленд',
                'alias' => 'merritt-island',
            ],
            [
                'url' => 'https://prian.ru/usa/arizona/mesa/',
                'title' => 'Меса',
                'alias' => 'mesa',
            ],
            [
                'url' => 'https://prian.ru/usa/minnesota/minneapolis/',
                'title' => 'Миннеаполис',
                'alias' => 'minneapolis',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/miramar/',
                'title' => 'Мирамар',
                'alias' => 'miramar',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/north-miami-beach/',
                'title' => 'Норт-Майами-Бич',
                'alias' => 'north-miami-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/new-york/new-york-city/',
                'title' => 'Нью-Йорк',
                'alias' => 'new-york-city',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/oakland-park/',
                'title' => 'Окленд-Парк',
                'alias' => 'oakland-park',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/orlando/',
                'title' => 'Орландо',
                'alias' => 'orlando',
            ],
            [
                'url' => 'https://prian.ru/usa/colorado/aurora/',
                'title' => 'Орора',
                'alias' => 'aurora',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/pinecrest/',
                'title' => 'Пайнкрест',
                'alias' => 'pinecrest',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/palm-beach/',
                'title' => 'Палм-Бич',
                'alias' => 'palm-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/palm-beach-gardens/',
                'title' => 'Палм-Бич-Гарденс',
                'alias' => 'palm-beach-gardens',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/palmetto-bay/',
                'title' => 'Палметто Бэй',
                'alias' => 'palmetto-bay',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/parkland/',
                'title' => 'Паркленд',
                'alias' => 'parkland',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/pembroke-pines/',
                'title' => 'Пемброк-Пайнс',
                'alias' => 'pembroke-pines',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/plantation/',
                'title' => 'Плантейшен',
                'alias' => 'plantation',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/pompano-beach/',
                'title' => 'Помпано Бич',
                'alias' => 'pompano-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/riviera-beach/',
                'title' => 'Ривьера Бич',
                'alias' => 'riviera-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/california/san-diego/',
                'title' => 'Сан-Диего',
                'alias' => 'san-diego',
            ],
            [
                'url' => 'https://prian.ru/usa/california/san-francisco/',
                'title' => 'Сан-Франциско',
                'alias' => 'san-francisco',
            ],
            [
                'url' => 'https://prian.ru/usa/california/san-jose/',
                'title' => 'Сан-Хосе',
                'alias' => 'san-jose',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/sunny-isles-beach/',
                'title' => 'Санни-Айлс-Бич',
                'alias' => 'sunny-isles-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/sunrise/',
                'title' => 'Санрайз',
                'alias' => 'sunrise',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/north-lauderdale/',
                'title' => 'Север Лодердейл',
                'alias' => 'north-lauderdale',
            ],
            [
                'url' => 'https://prian.ru/usa/washington/seattle/',
                'title' => 'Сиэтл',
                'alias' => 'seattle',
            ],
            [
                'url' => 'https://prian.ru/usa/other-regions-of-usa/strongsville/',
                'title' => 'Стронгсвилл',
                'alias' => 'strongsville',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/tamarac/',
                'title' => 'Тамарак',
                'alias' => 'tamarac',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/tequesta/',
                'title' => 'Теквеста',
                'alias' => 'tequesta',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/west-palm-beach/',
                'title' => 'Уэст-Палм-Бич',
                'alias' => 'west-palm-beach',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/weston/',
                'title' => 'Уэстон',
                'alias' => 'weston',
            ],
            [
                'url' => 'https://prian.ru/usa/arizona/phoenix/',
                'title' => 'Финикс',
                'alias' => 'phoenix',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/fisher-island/',
                'title' => 'Фишер-Айленд',
                'alias' => 'fisher-island',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/fort-lauderdale/',
                'title' => 'Форт-Лодердейл',
                'alias' => 'fort-lauderdale',
            ],
            [
                'url' => 'https://prian.ru/usa/texas/fort-worth/',
                'title' => 'Форт-Уэрт',
                'alias' => 'fort-worth',
            ],
            [
                'url' => 'https://prian.ru/usa/wyoming/hudson/',
                'title' => 'Хадсон',
                'alias' => 'hudson',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/hialeah/',
                'title' => 'Хайалиа',
                'alias' => 'hialeah',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/hialeah-gardens/',
                'title' => 'Хайалиа Гарденс',
                'alias' => 'hialeah-gardens',
            ],
            [
                'url' => 'https://prian.ru/usa/illinois/highwood/',
                'title' => 'Хайвуд',
                'alias' => 'highwood',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/hallandale/',
                'title' => 'Халландейл',
                'alias' => 'hallandale',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/homestead/',
                'title' => 'Хомстед',
                'alias' => 'homestead',
            ],
            [
                'url' => 'https://prian.ru/usa/texas/houston/',
                'title' => 'Хьюстон',
                'alias' => 'houston',
            ],
            [
                'url' => 'https://prian.ru/usa/illinois/chicago/',
                'title' => 'Чикаго',
                'alias' => 'chicago',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/southwest-ranches/',
                'title' => 'Юго-Запад Ранчес',
                'alias' => 'southwest-ranches',
            ],
            [
                'url' => 'https://prian.ru/usa/florida/jupiter/',
                'title' => 'Юпитер',
                'alias' => 'jupiter',
            ],
            [
                'url' => 'https://prian.ru/usa/washington/yakima/',
                'title' => 'Якима',
                'alias' => 'yakima',
            ],
        ],
        'url' => 'https://prian.ru/usa/',
        'title' => 'США',
        'alias' => 'usa',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/thailand/thailand/bangkok/',
                'title' => 'Бангкок',
                'alias' => 'bangkok',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/pattaya/',
                'title' => 'Паттайя',
                'alias' => 'pattaya',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/phetchaburi/',
                'title' => 'Пхетбури',
                'alias' => 'phetchaburi',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/phuket/',
                'title' => 'Пхукет',
                'alias' => 'phuket',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/rayong/',
                'title' => 'Районг',
                'alias' => 'rayong',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/koh-samui/',
                'title' => 'Самуи',
                'alias' => 'koh-samui',
            ],
            [
                'url' => 'https://prian.ru/thailand/thailand/chumphon/',
                'title' => 'Чумпхон',
                'alias' => 'chumphon',
            ],
        ],
        'url' => 'https://prian.ru/thailand/',
        'title' => 'Таиланд',
        'alias' => 'thailand',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/alanya/',
                'title' => 'Аланья',
                'alias' => 'alanya',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/antalya/',
                'title' => 'Анталия',
                'alias' => 'antalya',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/belek/',
                'title' => 'Белек',
                'alias' => 'belek',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/bodrum/',
                'title' => 'Бодрум',
                'alias' => 'bodrum',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/gazipasa/',
                'title' => 'Газипаша',
                'alias' => 'gazipasa',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/didim/',
                'title' => 'Дидим',
                'alias' => 'didim',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/izmir/',
                'title' => 'Измир',
                'alias' => 'izmir',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/kalkan/',
                'title' => 'Калкан',
                'alias' => 'kalkan',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/cas/',
                'title' => 'Каш',
                'alias' => 'cas',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/kemer/',
                'title' => 'Кемер',
                'alias' => 'kemer',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/konakli/',
                'title' => 'Конаклы',
                'alias' => 'konakli',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/konyaalti/',
                'title' => 'Коньяалты',
                'alias' => 'konyaalti',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/kusadasi/',
                'title' => 'Кушадасы',
                'alias' => 'kusadasi',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/lara/',
                'title' => 'Лара',
                'alias' => 'lara',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/marmaris/',
                'title' => 'Мармарис',
                'alias' => 'marmaris',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/mahmutlar/',
                'title' => 'Махмутлар',
                'alias' => 'mahmutlar',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/mersin/',
                'title' => 'Мерсин',
                'alias' => 'mersin',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/side/',
                'title' => 'Сиде',
                'alias' => 'side',
            ],
            [
                'url' => 'https://prian.ru/turkey/other-regions-of-turkey/istanbul/',
                'title' => 'Стамбул',
                'alias' => 'istanbul',
            ],
            [
                'url' => 'https://prian.ru/turkey/mediterranean-coast/fethiye/',
                'title' => 'Фетхие',
                'alias' => 'fethiye',
            ],
            [
                'url' => 'https://prian.ru/turkey/aegean-coast/cesme/',
                'title' => 'Чешме',
                'alias' => 'cesme',
            ],
            [
                'url' => 'https://prian.ru/turkey/other-regions-of-turkey/eskisehir/',
                'title' => 'Эскишехир',
                'alias' => 'eskisehir',
            ],
        ],
        'url' => 'https://prian.ru/turkey/',
        'title' => 'Турция',
        'alias' => 'turkey',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/uzbekistan/',
        'title' => 'Узбекистан',
        'alias' => 'uzbekistan',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/ukraine/',
        'title' => 'Украина',
        'alias' => 'ukraine',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/philippines/',
        'title' => 'Филиппины',
        'alias' => 'philippines',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/anttola/',
                'title' => 'Анттола',
                'alias' => 'anttola',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/anjalankoski/',
                'title' => 'Аньяланкоски',
                'alias' => 'anjalankoski',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/asikkala/',
                'title' => 'Асиккала',
                'alias' => 'asikkala',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/ahtari/',
                'title' => 'Ахтари',
                'alias' => 'ahtari',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/vainikkala/',
                'title' => 'Ваиниккала',
                'alias' => 'vainikkala',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/vantaa/',
                'title' => 'Вантаа',
                'alias' => 'vantaa',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/varkaus/',
                'title' => 'Варкаус',
                'alias' => 'varkaus',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/vaasa/',
                'title' => 'Васа',
                'alias' => 'vaasa',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/vehmersalmi/',
                'title' => 'Вехмерсалми',
                'alias' => 'vehmersalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/viitasaari/',
                'title' => 'Виитасаари',
                'alias' => 'viitasaari',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/virrat/',
                'title' => 'Виррат',
                'alias' => 'virrat',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/vihti/',
                'title' => 'Вихти',
                'alias' => 'vihti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/iisalmi/',
                'title' => 'Иисалми',
                'alias' => 'iisalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/iitti/',
                'title' => 'Иити',
                'alias' => 'iitti',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/ikaalinen/',
                'title' => 'Икаалинен',
                'alias' => 'ikaalinen',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/ilomantsi/',
                'title' => 'Иломантси',
                'alias' => 'ilomantsi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/imatra/',
                'title' => 'Иматра',
                'alias' => 'imatra',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/inari/',
                'title' => 'Инари',
                'alias' => 'inari',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/ihamaniemi/',
                'title' => 'Ихаманиеми',
                'alias' => 'ihamaniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/joroinen/',
                'title' => 'Йороинен',
                'alias' => 'joroinen',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/joensuu/',
                'title' => 'Йоэнсуу',
                'alias' => 'joensuu',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kaavi/',
                'title' => 'Каави',
                'alias' => 'kaavi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kallislahti/',
                'title' => 'Каллислахти',
                'alias' => 'kallislahti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kangasniemi/',
                'title' => 'Кангасниеми',
                'alias' => 'kangasniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/karstula/',
                'title' => 'Карстула',
                'alias' => 'karstula',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kauhava/',
                'title' => 'Каухава',
                'alias' => 'kauhava',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kajaani/',
                'title' => 'Каяани',
                'alias' => 'kajaani',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/keitele/',
                'title' => 'Кейтеле',
                'alias' => 'keitele',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/kemi/',
                'title' => 'Кеми',
                'alias' => 'kemi',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/kemijarvi/',
                'title' => 'Кемиярви',
                'alias' => 'kemijarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kerimaki/',
                'title' => 'Керимяки',
                'alias' => 'kerimaki',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kerma/',
                'title' => 'Керма',
                'alias' => 'kerma',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kesalahti/',
                'title' => 'Кесялахти',
                'alias' => 'kesalahti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/keuruu/',
                'title' => 'Кеуру',
                'alias' => 'keuruu',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kivijarvi/',
                'title' => 'Кивиярви',
                'alias' => 'kivijarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kirkkonummi/',
                'title' => 'Кирконумми',
                'alias' => 'kirkkonummi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kitee/',
                'title' => 'Китее',
                'alias' => 'kitee',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/kittila/',
                'title' => 'Киттилэ',
                'alias' => 'kittila',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/klamila/',
                'title' => 'Кламила',
                'alias' => 'klamila',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kokkola/',
                'title' => 'Коккола',
                'alias' => 'kokkola',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/konnevesi/',
                'title' => 'Конневеси',
                'alias' => 'konnevesi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kontiolahti/',
                'title' => 'Контиолахти',
                'alias' => 'kontiolahti',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/korppoo/',
                'title' => 'Корппоо',
                'alias' => 'korppoo',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kotka/',
                'title' => 'Котка',
                'alias' => 'kotka',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kouvola/',
                'title' => 'Коувола',
                'alias' => 'kouvola',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kruunupyy/',
                'title' => 'Круунупюю',
                'alias' => 'kruunupyy',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kuokkaniemi/',
                'title' => 'Кукканиеми',
                'alias' => 'kuokkaniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kuopio/',
                'title' => 'Куопио',
                'alias' => 'kuopio',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kuortti/',
                'title' => 'Куортти',
                'alias' => 'kuortti',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kuohu/',
                'title' => 'Куоху',
                'alias' => 'kuohu',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kuusamo/',
                'title' => 'Куусамо',
                'alias' => 'kuusamo',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kuusankoski/',
                'title' => 'Куусанкоски',
                'alias' => 'kuusankoski',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/kuhmo/',
                'title' => 'Кухмо',
                'alias' => 'kuhmo',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/kuhmoinen/',
                'title' => 'Кухмоинен',
                'alias' => 'kuhmoinen',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/kymenlaakso/',
                'title' => 'Кюмменлааксо',
                'alias' => 'kymenlaakso',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/lammi/',
                'title' => 'Ламми',
                'alias' => 'lammi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/lapinjarvi/',
                'title' => 'Лапинъярви',
                'alias' => 'lapinjarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/lappeenranta/',
                'title' => 'Лаппеенранта',
                'alias' => 'lappeenranta',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/laukaa/',
                'title' => 'Лаукаа',
                'alias' => 'laukaa',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/lauteala/',
                'title' => 'Лаутеала',
                'alias' => 'lauteala',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/lahti/',
                'title' => 'Лахти',
                'alias' => 'lahti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/lemi/',
                'title' => 'Леми',
                'alias' => 'lemi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/leppavirta/',
                'title' => 'Леппявирта',
                'alias' => 'leppavirta',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/lieksa/',
                'title' => 'Лиекса',
                'alias' => 'lieksa',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/loviisa/',
                'title' => 'Ловииса',
                'alias' => 'loviisa',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/loppi/',
                'title' => 'Лоппи',
                'alias' => 'loppi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/lohja/',
                'title' => 'Лохъя',
                'alias' => 'lohja',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/luumaki/',
                'title' => 'Луумяки',
                'alias' => 'luumaki',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/luhanka/',
                'title' => 'Луханка',
                'alias' => 'luhanka',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/langelmaki/',
                'title' => 'Лянгельмяки',
                'alias' => 'langelmaki',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/merijarvi/',
                'title' => 'Мериярви',
                'alias' => 'merijarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/miehikkala/',
                'title' => 'Миехиккяля',
                'alias' => 'miehikkala',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/mikkeli/',
                'title' => 'Миккели',
                'alias' => 'mikkeli',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/muonio/',
                'title' => 'Муонио',
                'alias' => 'muonio',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/muurame/',
                'title' => 'Муураме',
                'alias' => 'muurame',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/myrskyla/',
                'title' => 'Мюрскюла',
                'alias' => 'myrskyla',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/mantsala/',
                'title' => 'Мянтсяля',
                'alias' => 'mantsala',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/mantta/',
                'title' => 'Мянття',
                'alias' => 'mantta',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/mantyharju/',
                'title' => 'Мянтюхарью',
                'alias' => 'mantyharju',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/naantali/',
                'title' => 'Наантали',
                'alias' => 'naantali',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/naarajarvi/',
                'title' => 'Наараярви',
                'alias' => 'naarajarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/nilsia/',
                'title' => 'Нильсия',
                'alias' => 'nilsia',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/orimattila/',
                'title' => 'Ориматтила',
                'alias' => 'orimattila',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/otava/',
                'title' => 'Отава',
                'alias' => 'otava',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/oulu/',
                'title' => 'Оулу',
                'alias' => 'oulu',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/padasjoki/',
                'title' => 'Падасйоки',
                'alias' => 'padasjoki',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/paltamo/',
                'title' => 'Палтамо',
                'alias' => 'paltamo',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/parikkala/',
                'title' => 'Париккала',
                'alias' => 'parikkala',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/pertunmaa/',
                'title' => 'Пертунмаа',
                'alias' => 'pertunmaa',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/perho/',
                'title' => 'Перхо',
                'alias' => 'perho',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/pieksamaki/',
                'title' => 'Пиексямяки',
                'alias' => 'pieksamaki',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/pielavesi/',
                'title' => 'Пиелавеси',
                'alias' => 'pielavesi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/porvoo/',
                'title' => 'Порво',
                'alias' => 'porvoo',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/pori/',
                'title' => 'Пори',
                'alias' => 'pori',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/pornainen/',
                'title' => 'Порнайнен',
                'alias' => 'pornainen',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/posio/',
                'title' => 'Посио',
                'alias' => 'posio',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/pudasjarvi/',
                'title' => 'Пудасярви',
                'alias' => 'pudasjarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/punkaharju/',
                'title' => 'Пункахарью',
                'alias' => 'punkaharju',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/puumala/',
                'title' => 'Пуумала',
                'alias' => 'puumala',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/pyhtaa/',
                'title' => 'Пюхтяя',
                'alias' => 'pyhtaa',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/rantasalmi/',
                'title' => 'Рантасалми',
                'alias' => 'rantasalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/ranua/',
                'title' => 'Рануа',
                'alias' => 'ranua',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/rautalampi/',
                'title' => 'Рауталампи',
                'alias' => 'rautalampi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/rautjarvi/',
                'title' => 'Раутъярви',
                'alias' => 'rautjarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/rauha/',
                'title' => 'Рауха',
                'alias' => 'rauha',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/rahikkala/',
                'title' => 'Рахиккала',
                'alias' => 'rahikkala',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/rahula/',
                'title' => 'Рахула',
                'alias' => 'rahula',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/riihimaki/',
                'title' => 'Риихимяки',
                'alias' => 'riihimaki',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/rovaniemi/',
                'title' => 'Рованиеми',
                'alias' => 'rovaniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/ruovesi/',
                'title' => 'Руовеси',
                'alias' => 'ruovesi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/ruokolahti/',
                'title' => 'Руоколахти',
                'alias' => 'ruokolahti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/saari/',
                'title' => 'Саари',
                'alias' => 'saari',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/saarijarvi/',
                'title' => 'Саариярви',
                'alias' => 'saarijarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/savitaipale/',
                'title' => 'Савитайпале',
                'alias' => 'savitaipale',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/savonlinna/',
                'title' => 'Савонлинна',
                'alias' => 'savonlinna',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/savonranta/',
                'title' => 'Савонранта',
                'alias' => 'savonranta',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/salo/',
                'title' => 'Сало',
                'alias' => 'salo',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/sammatti/',
                'title' => 'Саммати',
                'alias' => 'sammatti',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/saukonsaari/',
                'title' => 'Сауконсаари',
                'alias' => 'saukonsaari',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/seinajoki/',
                'title' => 'Сейняйоки',
                'alias' => 'seinajoki',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/siilinjarvi/',
                'title' => 'Сийлинъярви',
                'alias' => 'siilinjarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/simpele/',
                'title' => 'Симпеле',
                'alias' => 'simpele',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/sipoo/',
                'title' => 'Сипоо',
                'alias' => 'sipoo',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/siuntio/',
                'title' => 'Сиунтио',
                'alias' => 'siuntio',
            ],
            [
                'url' => 'https://prian.ru/finland/lapland/sodankyla/',
                'title' => 'Соданкюла',
                'alias' => 'sodankyla',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/somero/',
                'title' => 'Сомеро',
                'alias' => 'somero',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/sotkamo/',
                'title' => 'Соткамо',
                'alias' => 'sotkamo',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/suinula/',
                'title' => 'Суинула',
                'alias' => 'suinula',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/sulkava/',
                'title' => 'Сулкава',
                'alias' => 'sulkava',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/suomenniemi/',
                'title' => 'Суоменниеми',
                'alias' => 'suomenniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/suomussalmi/',
                'title' => 'Суомуссалми',
                'alias' => 'suomussalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/suonenjoki/',
                'title' => 'Суоненйоки',
                'alias' => 'suonenjoki',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/sysma/',
                'title' => 'Сюсмя',
                'alias' => 'sysma',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/taavetti/',
                'title' => 'Тааветти',
                'alias' => 'taavetti',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/taalintehdas/',
                'title' => 'Таалинтехдас',
                'alias' => 'taalintehdas',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/taipalsaari/',
                'title' => 'Тайпалсаари',
                'alias' => 'taipalsaari',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/tampere/',
                'title' => 'Тампере',
                'alias' => 'tampere',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/teuva/',
                'title' => 'Теува',
                'alias' => 'teuva',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/toivakka/',
                'title' => 'Тоивакка',
                'alias' => 'toivakka',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/tohmajarvi/',
                'title' => 'Тохмаярви',
                'alias' => 'tohmajarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/turku/',
                'title' => 'Турку',
                'alias' => 'turku',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/tuulos/',
                'title' => 'Туулос',
                'alias' => 'tuulos',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/tuusniemi/',
                'title' => 'Туусниеми',
                'alias' => 'tuusniemi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/tuusula/',
                'title' => 'Туусула',
                'alias' => 'tuusula',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/tyrnava/',
                'title' => 'Тюрнава',
                'alias' => 'tyrnava',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/uimaharju/',
                'title' => 'Уймахарью',
                'alias' => 'uimaharju',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/urjala/',
                'title' => 'Урьяла',
                'alias' => 'urjala',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/forssa/',
                'title' => 'Форсса',
                'alias' => 'forssa',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/halikko/',
                'title' => 'Халикко',
                'alias' => 'halikko',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/hamina/',
                'title' => 'Хамина',
                'alias' => 'hamina',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/hangastenmaa/',
                'title' => 'Хангастенмаа',
                'alias' => 'hangastenmaa',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/hankasalmi/',
                'title' => 'Ханкасалми',
                'alias' => 'hankasalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/hanko/',
                'title' => 'Ханко',
                'alias' => 'hanko',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/hartola/',
                'title' => 'Хартола',
                'alias' => 'hartola',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/heinola/',
                'title' => 'Хейнола',
                'alias' => 'heinola',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/heinavesi/',
                'title' => 'Хейнявеси',
                'alias' => 'heinavesi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/helsinki/',
                'title' => 'Хельсинки',
                'alias' => 'helsinki',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/hirvensalmi/',
                'title' => 'Хирвенсалми',
                'alias' => 'hirvensalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/huittinen/',
                'title' => 'Хуитинен',
                'alias' => 'huittinen',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/huutokoski/',
                'title' => 'Хуутокоски',
                'alias' => 'huutokoski',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/hyrynsalmi/',
                'title' => 'Хюрюнсалми',
                'alias' => 'hyrynsalmi',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/hameenlinna/',
                'title' => 'Хямеэнлинна',
                'alias' => 'hameenlinna',
            ],
            [
                'url' => 'https://prian.ru/finland/western-and-central-finland/evijarvi/',
                'title' => 'Эвиярви',
                'alias' => 'evijarvi',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/enonkoski/',
                'title' => 'Энонкоски',
                'alias' => 'enonkoski',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/espoo/',
                'title' => 'Эспоо',
                'alias' => 'espoo',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/juva/',
                'title' => 'Юва',
                'alias' => 'juva',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/jyvaskyla/',
                'title' => 'Ювяскюля',
                'alias' => 'jyvaskyla',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/ylamaa/',
                'title' => 'Юлямаа',
                'alias' => 'ylamaa',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/juuka/',
                'title' => 'Юука',
                'alias' => 'juuka',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/aanekoski/',
                'title' => 'Яанекоски',
                'alias' => 'aanekoski',
            ],
            [
                'url' => 'https://prian.ru/finland/southeastern-finland/jamsa/',
                'title' => 'Ямся',
                'alias' => 'jamsa',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/jappila/',
                'title' => 'Яппиля',
                'alias' => 'jappila',
            ],
            [
                'url' => 'https://prian.ru/finland/southern-finland/jarvenpaa/',
                'title' => 'Ярвенпяа',
                'alias' => 'jarvenpaa',
            ],
        ],
        'url' => 'https://prian.ru/finland/',
        'title' => 'Финляндия',
        'alias' => 'finland',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/france/azure-coast/avignon/',
                'title' => 'Авиньон',
                'alias' => 'avignon',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/aquitaine/',
                'title' => 'Аквитания',
                'alias' => 'aquitaine',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/annecy/',
                'title' => 'Анси',
                'alias' => 'annecy',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/antibes/',
                'title' => 'Антиб',
                'alias' => 'antibes',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/arles/',
                'title' => 'Арль',
                'alias' => 'arles',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/pyrenees-atlantiques/',
                'title' => 'Атлантические Пиренеи',
                'alias' => 'pyrenees-atlantiques',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/bandol/',
                'title' => 'Бандоль',
                'alias' => 'bandol',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/biarritz/',
                'title' => 'Биарриц',
                'alias' => 'biarritz',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/biot/',
                'title' => 'Биот',
                'alias' => 'biot',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/beausoleil/',
                'title' => 'Боcолей',
                'alias' => 'beausoleil',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/beaulieu-sur-mer/',
                'title' => 'Больё-сюр-Мер',
                'alias' => 'beaulieu-sur-mer',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/bordeaux/',
                'title' => 'Бордо',
                'alias' => 'bordeaux',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/bretagne/',
                'title' => 'Бретань',
                'alias' => 'bretagne',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/burgundy/',
                'title' => 'Бургундия',
                'alias' => 'burgundy',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/bourges/',
                'title' => 'Бурж',
                'alias' => 'bourges',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/vallauris/',
                'title' => 'Валлорис',
                'alias' => 'vallauris',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/var/',
                'title' => 'Вар',
                'alias' => 'var',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/hautes-pyrenees/',
                'title' => 'Верхние Пиренеи',
                'alias' => 'hautes-pyrenees',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/haute-garonne/',
                'title' => 'Верхняя Гаронь',
                'alias' => 'haute-garonne',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/haute-savoie/',
                'title' => 'Верхняя Савойя',
                'alias' => 'haute-savoie',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/villeneuve-loubet/',
                'title' => 'Вильнёв-Лубе',
                'alias' => 'villeneuve-loubet',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/villefranche-sur-mer/',
                'title' => 'Вильфранш-сюр-Мер',
                'alias' => 'villefranche-sur-mer',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/pyrenees-orientales/',
                'title' => 'Восточные Пиренеи',
                'alias' => 'pyrenees-orientales',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/golfe-juan/',
                'title' => 'Гольф-Жуан',
                'alias' => 'golfe-juan',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/grasse/',
                'title' => 'Грас',
                'alias' => 'grasse',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/grimaud/',
                'title' => 'Гримо',
                'alias' => 'grimaud',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/divonne-les-bains/',
                'title' => 'Дивон-ле-Бен',
                'alias' => 'divonne-les-bains',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/dordogne/',
                'title' => 'Дордонь',
                'alias' => 'dordogne',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/draguignan/',
                'title' => 'Драгиньян',
                'alias' => 'draguignan',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/lake-geneva/',
                'title' => 'Женевское Озеро',
                'alias' => 'lake-geneva',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/gers/',
                'title' => 'Жер',
                'alias' => 'gers',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/gironde/',
                'title' => 'Жиронда',
                'alias' => 'gironde',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/juan-les-pins/',
                'title' => 'Жюан-ле-Пен',
                'alias' => 'juan-les-pins',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/pays-de-la-loire/',
                'title' => 'земли Луары',
                'alias' => 'pays-de-la-loire',
            ],
            [
                'url' => 'https://prian.ru/france/paris-and-isle-de-france/ile-de-france/',
                'title' => 'Иль-де-Франс',
                'alias' => 'ile-de-france',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/calais/',
                'title' => 'Кале',
                'alias' => 'calais',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/cannes/',
                'title' => 'Канны',
                'alias' => 'cannes',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/cagnes-sur-mer/',
                'title' => 'Кань-Сюр-Мер',
                'alias' => 'cagnes-sur-mer',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/cap-d-ail/',
                'title' => 'Кап-Д\'ай',
                'alias' => 'cap-d-ail',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/cap-martin/',
                'title' => 'Кап-Мартен',
                'alias' => 'cap-martin',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/cap-ferrat/',
                'title' => 'Кап-Ферра',
                'alias' => 'cap-ferrat',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/combloux/',
                'title' => 'Комблу',
                'alias' => 'combloux',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/corse/',
                'title' => 'Корсика',
                'alias' => 'corse',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/courchevel/',
                'title' => 'Куршевель',
                'alias' => 'courchevel',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/la-plagne/',
                'title' => 'Ла Плань',
                'alias' => 'la-plagne',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/languedoc-roussillon/',
                'title' => 'Лангедок-Руссильон',
                'alias' => 'languedoc-roussillon',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/landes/',
                'title' => 'Ланды',
                'alias' => 'landes',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/limoges/',
                'title' => 'Лимож',
                'alias' => 'limoges',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/limousin/',
                'title' => 'Лимузен',
                'alias' => 'limousin',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/lyon/',
                'title' => 'Лион',
                'alias' => 'lyon',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/lot-et-garonne/',
                'title' => 'Ло и Гаронна',
                'alias' => 'lot-et-garonne',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/mandelieu-la-napoule/',
                'title' => 'Мандельё-ла-Напуль',
                'alias' => 'mandelieu-la-napoule',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/megeve/',
                'title' => 'Межев',
                'alias' => 'megeve',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/menton/',
                'title' => 'Ментон',
                'alias' => 'menton',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/meribel/',
                'title' => 'Мерибель',
                'alias' => 'meribel',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/modane/',
                'title' => 'Модан',
                'alias' => 'modane',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/mont-de-lans/',
                'title' => 'Мон-де-Ланс',
                'alias' => 'mont-de-lans',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/montpellier/',
                'title' => 'Монпелье',
                'alias' => 'montpellier',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/montriond/',
                'title' => 'Монрьон',
                'alias' => 'montriond',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/montauban/',
                'title' => 'Монтобан',
                'alias' => 'montauban',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/morzine/',
                'title' => 'Морзин',
                'alias' => 'morzine',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/mouans-sartoux/',
                'title' => 'Муан-Сарту',
                'alias' => 'mouans-sartoux',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/mougins/',
                'title' => 'Мужен',
                'alias' => 'mougins',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/narbonne/',
                'title' => 'Нарбонна',
                'alias' => 'narbonne',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/nimes/',
                'title' => 'Ним',
                'alias' => 'nimes',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/nice/',
                'title' => 'Ницца',
                'alias' => 'nice',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/nord-pas-de-calais/',
                'title' => 'Нор — Па-де-Кале',
                'alias' => 'nord-pas-de-calais',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/normandie/',
                'title' => 'Нормандия',
                'alias' => 'normandie',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/auvergne/',
                'title' => 'Овернь',
                'alias' => 'auvergne',
            ],
            [
                'url' => 'https://prian.ru/france/paris-and-isle-de-france/paris/',
                'title' => 'Париж',
                'alias' => 'paris',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/perpignan/',
                'title' => 'Перпиньян',
                'alias' => 'perpignan',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/picardie/',
                'title' => 'Пикардия',
                'alias' => 'picardie',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/poitou-charentes/',
                'title' => 'Пуату — Шаранта',
                'alias' => 'poitou-charentes',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/ramatuelle/',
                'title' => 'Раматюель',
                'alias' => 'ramatuelle',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/region-centre/',
                'title' => 'регион Центр',
                'alias' => 'region-centre',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/roquebrune-cap-martin/',
                'title' => 'Рокбрюн-Кап-Мартен',
                'alias' => 'roquebrune-cap-martin',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/rhone-alpes/',
                'title' => 'Рона-Альпы',
                'alias' => 'rhone-alpes',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-jean-cap-ferrat/',
                'title' => 'Сен-Жан-Кап-Ферра',
                'alias' => 'saint-jean-cap-ferrat',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-maxime/',
                'title' => 'Сен-Максим',
                'alias' => 'saint-maxime',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-paul-de-vence/',
                'title' => 'Сен-Поль-де-Ванс',
                'alias' => 'saint-paul-de-vence',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-raphael/',
                'title' => 'Сен-Рафаэль',
                'alias' => 'saint-raphael',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-tropez/',
                'title' => 'Сен-Тропе',
                'alias' => 'saint-tropez',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/saint-aygulf/',
                'title' => 'Сен-Эжюльф',
                'alias' => 'saint-aygulf',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/strasbourg/',
                'title' => 'Страсбург',
                'alias' => 'strasbourg',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/theoule-sur-mer/',
                'title' => 'Теуль-сюр-Мер',
                'alias' => 'theoule-sur-mer',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/thonon-les-bains/',
                'title' => 'Тонон-ле-Бен',
                'alias' => 'thonon-les-bains',
            ],
            [
                'url' => 'https://prian.ru/france/southern-france/toulouse/',
                'title' => 'Тулуза',
                'alias' => 'toulouse',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/tours/',
                'title' => 'Тур',
                'alias' => 'tours',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/franche-comte/',
                'title' => 'Франш-Конте',
                'alias' => 'franche-comte',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/frejus/',
                'title' => 'Фрежюс',
                'alias' => 'frejus',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/chamonix/',
                'title' => 'Шамони',
                'alias' => 'chamonix',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/champagne/',
                'title' => 'Шампань',
                'alias' => 'champagne',
            ],
            [
                'url' => 'https://prian.ru/france/central-and-northern-france/charente/',
                'title' => 'Шаранта',
                'alias' => 'charente',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/evian/',
                'title' => 'Эвиан',
                'alias' => 'evian',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/eze/',
                'title' => 'Эз',
                'alias' => 'eze',
            ],
            [
                'url' => 'https://prian.ru/france/azure-coast/aix-en-provence/',
                'title' => 'Экс-ан-Прованс',
                'alias' => 'aix-en-provence',
            ],
            [
                'url' => 'https://prian.ru/france/french-alps/aix-les-bains/',
                'title' => 'Экс-ле-Бен',
                'alias' => 'aix-les-bains',
            ],
        ],
        'url' => 'https://prian.ru/france/',
        'title' => 'Франция',
        'alias' => 'france',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/croatia/dalmatia/biograd-na-moru/',
                'title' => 'Биоград-на-Мору',
                'alias' => 'biograd-na-moru',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/brac-island/',
                'title' => 'Брач',
                'alias' => 'brac-island',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/buje/',
                'title' => 'Буйе',
                'alias' => 'buje',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/vis/',
                'title' => 'Вис',
                'alias' => 'vis',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/vodice/',
                'title' => 'Водице',
                'alias' => 'vodice',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/vodnjan/',
                'title' => 'Воднян',
                'alias' => 'vodnjan',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/vrbovsko/',
                'title' => 'Врбовско',
                'alias' => 'vrbovsko',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/vrsar/',
                'title' => 'Врсар',
                'alias' => 'vrsar',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/dubrovnik/',
                'title' => 'Дубровник',
                'alias' => 'dubrovnik',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/zminj/',
                'title' => 'Жминь',
                'alias' => 'zminj',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/zargeb/',
                'title' => 'Загреб',
                'alias' => 'zargeb',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/zadar/',
                'title' => 'Задар',
                'alias' => 'zadar',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/icici/',
                'title' => 'Ичичи',
                'alias' => 'icici',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/karlobag/',
                'title' => 'Карлобаг',
                'alias' => 'karlobag',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/krk/',
                'title' => 'Крк',
                'alias' => 'krk',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/krsan/',
                'title' => 'Кршан',
                'alias' => 'krsan',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/labin/',
                'title' => 'Лабин',
                'alias' => 'labin',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/liznjan/',
                'title' => 'Лижнян',
                'alias' => 'liznjan',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/lovran/',
                'title' => 'Ловран',
                'alias' => 'lovran',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/makarska/',
                'title' => 'Макарска',
                'alias' => 'makarska',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/mali-losinj/',
                'title' => 'Мали-Лошинь',
                'alias' => 'mali-losinj',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/medulin/',
                'title' => 'Медулин',
                'alias' => 'medulin',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/novi-vinodolski/',
                'title' => 'Нови-Винодольски',
                'alias' => 'novi-vinodolski',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/novigrad/',
                'title' => 'Новиград',
                'alias' => 'novigrad',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/omis/',
                'title' => 'Омиш',
                'alias' => 'omis',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/opatija/',
                'title' => 'Опатия',
                'alias' => 'opatija',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/oprtalj/',
                'title' => 'Опрталь',
                'alias' => 'oprtalj',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/peljesac-peninsula/',
                'title' => 'п-ов Пелешац',
                'alias' => 'peljesac-peninsula',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/pag/',
                'title' => 'Паг',
                'alias' => 'pag',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/porec/',
                'title' => 'Пореч',
                'alias' => 'porec',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/premantura/',
                'title' => 'Премантура',
                'alias' => 'premantura',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/primosten/',
                'title' => 'Примоштен',
                'alias' => 'primosten',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/pula/',
                'title' => 'Пула',
                'alias' => 'pula',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/rab/',
                'title' => 'Раб',
                'alias' => 'rab',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/rabac/',
                'title' => 'Рабац',
                'alias' => 'rabac',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/rijeka/',
                'title' => 'Риека',
                'alias' => 'rijeka',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/rovinj/',
                'title' => 'Ровинь',
                'alias' => 'rovinj',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/rogoznica/',
                'title' => 'Рогозница',
                'alias' => 'rogoznica',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/svetvincenat/',
                'title' => 'Светвинченат',
                'alias' => 'svetvincenat',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/selce/',
                'title' => 'Сельце',
                'alias' => 'selce',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/split/',
                'title' => 'Сплит',
                'alias' => 'split',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/middle-dalmatia/',
                'title' => 'Средняя Далмация',
                'alias' => 'middle-dalmatia',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/tinjan/',
                'title' => 'Тинян',
                'alias' => 'tinjan',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/trogir/',
                'title' => 'Трогир',
                'alias' => 'trogir',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/umag/',
                'title' => 'Умаг',
                'alias' => 'umag',
            ],
            [
                'url' => 'https://prian.ru/croatia/istria/fazana/',
                'title' => 'Фажана',
                'alias' => 'fazana',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/hvar/',
                'title' => 'Хвар',
                'alias' => 'hvar',
            ],
            [
                'url' => 'https://prian.ru/croatia/other-parts-of-croatia/crikvenica/',
                'title' => 'Цриквеница',
                'alias' => 'crikvenica',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/ciovo/',
                'title' => 'Чиово',
                'alias' => 'ciovo',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/sibenic/',
                'title' => 'Шибеник',
                'alias' => 'sibenic',
            ],
            [
                'url' => 'https://prian.ru/croatia/dalmatia/southern-dalmatia/',
                'title' => 'Южная Далмация',
                'alias' => 'southern-dalmatia',
            ],
        ],
        'url' => 'https://prian.ru/croatia/',
        'title' => 'Хорватия',
        'alias' => 'croatia',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/montenegro/montenegro/ada-bojana/',
                'title' => 'Ада Бояна',
                'alias' => 'ada-bojana',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/baosici/',
                'title' => 'Баошичи',
                'alias' => 'baosici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bar/',
                'title' => 'Бар',
                'alias' => 'bar',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/becici/',
                'title' => 'Бечичи',
                'alias' => 'becici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bigova/',
                'title' => 'Бигово',
                'alias' => 'bigova',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bijela/',
                'title' => 'Биела',
                'alias' => 'bijela',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bijelo-pole/',
                'title' => 'Биело-Поле',
                'alias' => 'bijelo-pole',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/blizikuce/',
                'title' => 'Близикуче',
                'alias' => 'blizikuce',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bogisici/',
                'title' => 'Богишичи',
                'alias' => 'bogisici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/budva/',
                'title' => 'Будва',
                'alias' => 'budva',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/buljarica/',
                'title' => 'Булярица',
                'alias' => 'buljarica',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/bjelila/',
                'title' => 'Бьелила',
                'alias' => 'bjelila',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/valdanos/',
                'title' => 'Валданос',
                'alias' => 'valdanos',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/veslo/',
                'title' => 'Весло',
                'alias' => 'veslo',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/vidicovac/',
                'title' => 'Видиковац',
                'alias' => 'vidicovac',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/virpazar/',
                'title' => 'Вирпазар',
                'alias' => 'virpazar',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/grahovo/',
                'title' => 'Грахово',
                'alias' => 'grahovo',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/danilovgrad/',
                'title' => 'Даниловград',
                'alias' => 'danilovgrad',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/denovici/',
                'title' => 'Дженовичи',
                'alias' => 'denovici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/durasevici/',
                'title' => 'Джурашевичи',
                'alias' => 'durasevici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/dobra-voda/',
                'title' => 'Добра Вода',
                'alias' => 'dobra-voda',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/dobrota/',
                'title' => 'Доброта',
                'alias' => 'dobrota',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/zabljak/',
                'title' => 'Жабляк',
                'alias' => 'zabljak',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/zanjic/',
                'title' => 'Жанице',
                'alias' => 'zanjic',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/zabrde/',
                'title' => 'Забрдже',
                'alias' => 'zabrde',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/zelenika/',
                'title' => 'Зеленика',
                'alias' => 'zelenika',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/igalo/',
                'title' => 'Игало',
                'alias' => 'igalo',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kavac/',
                'title' => 'Кавач',
                'alias' => 'kavac',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kamenari/',
                'title' => 'Каменари',
                'alias' => 'kamenari',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kolasin/',
                'title' => 'Колашин',
                'alias' => 'kolasin',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kostanjica/',
                'title' => 'Костаница',
                'alias' => 'kostanjica',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kotor/',
                'title' => 'Котор',
                'alias' => 'kotor',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/krasici/',
                'title' => 'Крашичи',
                'alias' => 'krasici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/krimovica/',
                'title' => 'Кримовица',
                'alias' => 'krimovica',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/krtole/',
                'title' => 'Кртоле',
                'alias' => 'krtole',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/krute/',
                'title' => 'Круче',
                'alias' => 'krute',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kumbor/',
                'title' => 'Кумбор',
                'alias' => 'kumbor',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/kunje/',
                'title' => 'Кунье',
                'alias' => 'kunje',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/lastva/',
                'title' => 'Ластва',
                'alias' => 'lastva',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/lepetane/',
                'title' => 'Лепетань',
                'alias' => 'lepetane',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/ljuta/',
                'title' => 'Люта',
                'alias' => 'ljuta',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/marcovici/',
                'title' => 'Марковичи',
                'alias' => 'marcovici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/meljine/',
                'title' => 'Мельине',
                'alias' => 'meljine',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/mirista/',
                'title' => 'Миришта',
                'alias' => 'mirista',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/morinj/',
                'title' => 'Моринь',
                'alias' => 'morinj',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/muo/',
                'title' => 'Муо',
                'alias' => 'muo',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/niksic/',
                'title' => 'Никшич',
                'alias' => 'niksic',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/lake-piva/',
                'title' => 'озеро Пива',
                'alias' => 'lake-piva',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/opatovo/',
                'title' => 'Опатово',
                'alias' => 'opatovo',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/orahovac/',
                'title' => 'Ораховац',
                'alias' => 'orahovac',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/lustica-peninsula/',
                'title' => 'п-ов Луштица',
                'alias' => 'lustica-peninsula',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/perast/',
                'title' => 'Пераст',
                'alias' => 'perast',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/petrovac/',
                'title' => 'Петровац',
                'alias' => 'petrovac',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/podgorica/',
                'title' => 'Подгорица',
                'alias' => 'podgorica',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/przno/',
                'title' => 'Пржно',
                'alias' => 'przno',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/prcanj/',
                'title' => 'Прчань',
                'alias' => 'prcanj',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/radanovici/',
                'title' => 'Радановичи',
                'alias' => 'radanovici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/radovici/',
                'title' => 'Радовичи',
                'alias' => 'radovici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/rafailovici/',
                'title' => 'Рафаиловичи',
                'alias' => 'rafailovici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/rezevici/',
                'title' => 'Режевичи',
                'alias' => 'rezevici',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/risan/',
                'title' => 'Рисан',
                'alias' => 'risan',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/rose/',
                'title' => 'Розе',
                'alias' => 'rose',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/sveti-stefan/',
                'title' => 'Святой Стефан',
                'alias' => 'sveti-stefan',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/seoce/',
                'title' => 'Сеоце',
                'alias' => 'seoce',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/lake-skadar/',
                'title' => 'Скадарское озеро',
                'alias' => 'lake-skadar',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/stoliv/',
                'title' => 'Столив',
                'alias' => 'stoliv',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/sutomore/',
                'title' => 'Сутоморе',
                'alias' => 'sutomore',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/tivat/',
                'title' => 'Тиват',
                'alias' => 'tivat',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/ulcinj/',
                'title' => 'Улцинь',
                'alias' => 'ulcinj',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/utjeha/',
                'title' => 'Утеха',
                'alias' => 'utjeha',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/herceg-novi/',
                'title' => 'Херцег Нови',
                'alias' => 'herceg-novi',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/cetinje/',
                'title' => 'Цетине',
                'alias' => 'cetinje',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/canj/',
                'title' => 'Чань',
                'alias' => 'canj',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/skaljari/',
                'title' => 'Шкальяри',
                'alias' => 'skaljari',
            ],
            [
                'url' => 'https://prian.ru/montenegro/montenegro/susanj/',
                'title' => 'Шушань',
                'alias' => 'susanj',
            ],
        ],
        'url' => 'https://prian.ru/montenegro/',
        'title' => 'Черногория',
        'alias' => 'montenegro',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/czech/prague/benesov/',
                'title' => 'Бенешов',
                'alias' => 'benesov',
            ],
            [
                'url' => 'https://prian.ru/czech/prague/beroun/',
                'title' => 'Бероун',
                'alias' => 'beroun',
            ],
            [
                'url' => 'https://prian.ru/czech/northern-czech/bilina/',
                'title' => 'Билина',
                'alias' => 'bilina',
            ],
            [
                'url' => 'https://prian.ru/czech/northern-czech/hradec-kralove/',
                'title' => 'Градец Кралове',
                'alias' => 'hradec-kralove',
            ],
            [
                'url' => 'https://prian.ru/czech/western-czech/karlovy-vary/',
                'title' => 'Карловы-Вары',
                'alias' => 'karlovy-vary',
            ],
            [
                'url' => 'https://prian.ru/czech/prague/kladno/',
                'title' => 'Кладно',
                'alias' => 'kladno',
            ],
            [
                'url' => 'https://prian.ru/czech/prague/lany/',
                'title' => 'Ланы',
                'alias' => 'lany',
            ],
            [
                'url' => 'https://prian.ru/czech/prague/lysa-nad-labem/',
                'title' => 'Лиса-над-Лабем',
                'alias' => 'lysa-nad-labem',
            ],
            [
                'url' => 'https://prian.ru/czech/western-czech/marianske-lazne/',
                'title' => 'Марианске-Лазне',
                'alias' => 'marianske-lazne',
            ],
            [
                'url' => 'https://prian.ru/czech/northern-czech/most/',
                'title' => 'Мост',
                'alias' => 'most',
            ],
            [
                'url' => 'https://prian.ru/czech/southern-and-eastern-czech/ostrava/',
                'title' => 'Острава',
                'alias' => 'ostrava',
            ],
            [
                'url' => 'https://prian.ru/czech/western-czech/plzen/',
                'title' => 'Пльзень',
                'alias' => 'plzen',
            ],
            [
                'url' => 'https://prian.ru/czech/prague/prague-city/',
                'title' => 'Прага',
                'alias' => 'prague-city',
            ],
            [
                'url' => 'https://prian.ru/czech/western-czech/sokolov/',
                'title' => 'Соколов',
                'alias' => 'sokolov',
            ],
            [
                'url' => 'https://prian.ru/czech/northern-czech/teplice/',
                'title' => 'Теплице',
                'alias' => 'teplice',
            ],
            [
                'url' => 'https://prian.ru/czech/southern-and-eastern-czech/cesky-krumlov/',
                'title' => 'Чески Крумлов',
                'alias' => 'cesky-krumlov',
            ],
            [
                'url' => 'https://prian.ru/czech/southern-and-eastern-czech/south-bohemia/',
                'title' => 'Южночешский Край',
                'alias' => 'south-bohemia',
            ],
        ],
        'url' => 'https://prian.ru/czech/',
        'title' => 'Чехия',
        'alias' => 'czech',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/switzerland/switzerland/basel/',
                'title' => 'Базель',
                'alias' => 'basel',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/bern/',
                'title' => 'Берн',
                'alias' => 'bern',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/brusino-arsizio/',
                'title' => 'Брусино Арсизио',
                'alias' => 'brusino-arsizio',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/valais/',
                'title' => 'Вале',
                'alias' => 'valais',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/verbier/',
                'title' => 'Вербье',
                'alias' => 'verbier',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/vaud/',
                'title' => 'Во',
                'alias' => 'vaud',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/gersau/',
                'title' => 'Герзау',
                'alias' => 'gersau',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/graubunden/',
                'title' => 'Граубюнден',
                'alias' => 'graubunden',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/grindelwald/',
                'title' => 'Гриндельвальд',
                'alias' => 'grindelwald',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/gstaad/',
                'title' => 'Гштад',
                'alias' => 'gstaad',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/geneva/',
                'title' => 'Женева',
                'alias' => 'geneva',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/interlaken/',
                'title' => 'Интерлакен',
                'alias' => 'interlaken',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/crans-montana/',
                'title' => 'Кран-Монтана',
                'alias' => 'crans-montana',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/kreuzlingen/',
                'title' => 'Кройцлинген',
                'alias' => 'kreuzlingen',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/lausanne/',
                'title' => 'Лозанна',
                'alias' => 'lausanne',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/lugano/',
                'title' => 'Лугано',
                'alias' => 'lugano',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/luzern/',
                'title' => 'Люцерн',
                'alias' => 'luzern',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/montreux/',
                'title' => 'Монтрё',
                'alias' => 'montreux',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/ticino/',
                'title' => 'Тичино',
                'alias' => 'ticino',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/fribourg/',
                'title' => 'Фрибур',
                'alias' => 'fribourg',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/zuoz/',
                'title' => 'Цуоц',
                'alias' => 'zuoz',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/zurich/',
                'title' => 'Цюрих',
                'alias' => 'zurich',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/schwyz/',
                'title' => 'Швиц',
                'alias' => 'schwyz',
            ],
            [
                'url' => 'https://prian.ru/switzerland/switzerland/stein-am-rhein/',
                'title' => 'Штайн-ам-Райн',
                'alias' => 'stein-am-rhein',
            ],
        ],
        'url' => 'https://prian.ru/switzerland/',
        'title' => 'Швейцария',
        'alias' => 'switzerland',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/sweden/vasternorrland_county/solleftea/',
                'title' => 'Соллефтео',
                'alias' => 'solleftea',
            ],
        ],
        'url' => 'https://prian.ru/sweden/',
        'title' => 'Швеция',
        'alias' => 'sweden',
    ],
    [
        'cities' => [],
        'url' => 'https://prian.ru/sri-lanka/',
        'title' => 'Шри-Ланка',
        'alias' => 'sri-lanka',
    ],
    [
        'cities' => [
            [
                'url' => 'https://prian.ru/estonia/estonia/alajoe/',
                'title' => 'Алайыэ',
                'alias' => 'alajoe',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/viljandimaa/',
                'title' => 'Вильяндимаа',
                'alias' => 'viljandimaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/vihula/',
                'title' => 'Вихула',
                'alias' => 'vihula',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/ida-virumaa/',
                'title' => 'Ида-Вирумаа',
                'alias' => 'ida-virumaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/kehra/',
                'title' => 'Кехра',
                'alias' => 'kehra',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/kohtla-jarve/',
                'title' => 'Кохтла-Ярве',
                'alias' => 'kohtla-jarve',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/kuressaare/',
                'title' => 'Курессааре',
                'alias' => 'kuressaare',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/laane-virumaa/',
                'title' => 'Ляэне-Вирумаа',
                'alias' => 'laane-virumaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/laanemaa/',
                'title' => 'Ляэнемаа',
                'alias' => 'laanemaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/narva/',
                'title' => 'Нарва',
                'alias' => 'narva',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/narva-joesuu/',
                'title' => 'Нарва-Йыэсуу',
                'alias' => 'narva-joesuu',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/pussi/',
                'title' => 'Пюсси',
                'alias' => 'pussi',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/paite/',
                'title' => 'Пяйте',
                'alias' => 'paite',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/parnu/',
                'title' => 'Пярну',
                'alias' => 'parnu',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/rakvere/',
                'title' => 'Раквере',
                'alias' => 'rakvere',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/raplamaa/',
                'title' => 'Рапламаа',
                'alias' => 'raplamaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/saaremaa/',
                'title' => 'Сааремаа',
                'alias' => 'saaremaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/tallinn/',
                'title' => 'Таллин',
                'alias' => 'tallinn',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/tartu/',
                'title' => 'Тарту',
                'alias' => 'tartu',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/toila/',
                'title' => 'Тойла',
                'alias' => 'toila',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/haapsalu/',
                'title' => 'Хаапсалу',
                'alias' => 'haapsalu',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/harjumaa/',
                'title' => 'Харьюмаа',
                'alias' => 'harjumaa',
            ],
            [
                'url' => 'https://prian.ru/estonia/estonia/hiiumaa/',
                'title' => 'Хийумаа',
                'alias' => 'hiiumaa',
            ],
        ],
        'url' => 'https://prian.ru/estonia/',
        'title' => 'Эстония',
        'alias' => 'estonia',
    ],
];