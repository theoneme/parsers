<?php
 return [
    'moskva.n1.ru' => 'Москва',
    'novosibirsk.n1.ru' => 'Новосибирск',
    'krasnodar.n1.ru' => 'Краснодар',
    'omsk.n1.ru' => 'Омск',
    'tumen.n1.ru' => 'Тюмень',
    'ekaterinburg.n1.ru' => 'Екатеринбург',
    'abakan.n1.ru' => 'Абакан',
    'anadyr.n1.ru' => 'Анадырь',
    'arhangelsk.n1.ru' => 'Архангельск',
    'astrahan.n1.ru' => 'Астрахань',
    'barnaul.n1.ru' => 'Барнаул',
    'belgorod.n1.ru' => 'Белгород',
    'birobidzhan.n1.ru' => 'Биробиджан',
    'bryansk.n1.ru' => 'Брянск',
    'velikiy-novgorod.n1.ru' => 'Великий Новгород',
    'vladivostok.n1.ru' => 'Владивосток',
    'vladimir.n1.ru' => 'Владимир',
    'volgograd.n1.ru' => 'Волгоград',
    'vologda.n1.ru' => 'Вологда',
    'voronezh.n1.ru' => 'Воронеж',
    'gorno-altaysk.n1.ru' => 'Горно-Алтайск',
    'ivanovo.n1.ru' => 'Иваново',
    'izhevsk.n1.ru' => 'Ижевск',
    'irkutsk.n1.ru' => 'Иркутск',
    'yoshkar-ola.n1.ru' => 'Йошкар-Ола',
    'kazan.n1.ru' => 'Казань',
    'kaliningrad.n1.ru' => 'Калининград',
    'kaluga.n1.ru' => 'Калуга',
    'kemerovo.n1.ru' => 'Кемерово',
    'kirov.n1.ru' => 'Киров',
    'kostroma.n1.ru' => 'Кострома',
    'krasnoyarsk.n1.ru' => 'Красноярск',
    'kurgan.n1.ru' => 'Курган',
    'kursk.n1.ru' => 'Курск',
    'kyzyl.n1.ru' => 'Кызыл',
    'lipeck.n1.ru' => 'Липецк',
    'magadan.n1.ru' => 'Магадан',
    'magas.n1.ru' => 'Магас',
    'mahachkala.n1.ru' => 'Махачкала',
    'murmansk.n1.ru' => 'Мурманск',
    'nalchik.n1.ru' => 'Нальчик',
    'nizhniy-novgorod.n1.ru' => 'Нижний Новгород',
    'orenburg.n1.ru' => 'Оренбург',
    'orel.n1.ru' => 'Орёл',
    'penza.n1.ru' => 'Пенза',
    'perm.n1.ru' => 'Пермь',
    'petrozavodsk.n1.ru' => 'Петрозаводск',
    'pskov.n1.ru' => 'Псков',
    'rostov.n1.ru' => 'Ростов-на-Дону',
    'ryazan.n1.ru' => 'Рязань',
    'salehard.n1.ru' => 'Салехард',
    'samara.n1.ru' => 'Самара',
    'sankt-peterburg.n1.ru' => 'Санкт-Петербург',
    'saransk.n1.ru' => 'Саранск',
    'saratov.n1.ru' => 'Саратов',
    'smolensk.n1.ru' => 'Смоленск',
    'stavropol.n1.ru' => 'Ставрополь',
    'syktyvkar.n1.ru' => 'Сыктывкар',
    'tambov.n1.ru' => 'Тамбов',
    'tver.n1.ru' => 'Тверь',
    'tomsk.n1.ru' => 'Томск',
    'tula.n1.ru' => 'Тула',
    'ulan-ude.n1.ru' => 'Улан-Удэ',
    'ufa.n1.ru' => 'Уфа',
    'habarovsk.n1.ru' => 'Хабаровск',
    'chelyabinsk.n1.ru' => 'Челябинск',
    'yaroslavl.n1.ru' => 'Ярославль',
];