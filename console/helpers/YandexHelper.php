<?php

namespace console\helpers;

use common\helpers\XpathHelper;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use yii\helpers\Json;
use yii\httpclient\Client;

class YandexHelper
{
    public static $_cities = [
        'ekaterinburg',
        'moskva',
        'abakan',
        'achinsk',
        'almetevsk',
        'armavir',
        'artem',
        'arzamas',
        'barnaul',
        'bataysk',
        'belgorod',
        'berdsk',
        'berezniki',
        'biysk',
        'bryansk',
        'cheboksary',
        'cherepovets',
        'chita',
        'derbent',
        'dimitrovgrad',
        'dzerzhinsk',
        'dzerzhinskiy',
        'elets',
        'elista',
        'engels',
        'essentuki',
        'ivanovo',
        'izhevsk',
        'kaluga',
        'kamensk-uralskiy',
        'kamyshin',
        'kaspiysk',
        'kemerovo',
        'kislovodsk',
        'komsomolsk-na-amure',
        'kostroma',
        'kovrov',
        'krasnodar',
        'krasnoyarsk',
        'kurgan',
        'kursk',
        'lipetsk',
        'maykop',
        'miass',
        'murom',
        'nevinnomyssk',
        'nizhnekamsk',
        'novocheboksarsk',
        'novocherkassk',
        'novokuybyshevsk',
        'novomoskovsk',
        'novorossiysk',
        'novosibirsk',
        'obninsk',
        'omsk',
        'orenburg',
        'pervouralsk',
        'petrozavodsk',
        'pskov',
        'pyatigorsk',
        'rostov-na-donu',
        'ryazan',
        'rybinsk',
        'samara',
        'saransk',
        'saratov',
        'seversk',
        'smolensk',
        'sochi',
        'stavropol',
        'sterlitamak',
        'syktyvkar',
        'taganrog',
        'tambov',
        'tolyatti',
        'tomsk',
        'tula',
        'tver',
        'tyumen',
        'ulan-ude',
        'ulyanovsk',
        'ussuriysk',
        'vladimir',
        'volgodonsk',
        'volgograd',
        'vologda',
        'volzhskiy',
        'voronezh',
        'yakutsk',
        'yaroslavl',
        'yoshkar-ola',
        'zheleznogorsk',
        'zlatoust',
    ];

    public static $_rooms = ["1", "2", "3", "4", "PLUS_4", "5", "6", "PLUS_7", "STUDIO", "OPEN_PLAN"];

    public static $_headers = [
        "scheme" => "https",
        "accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
//        "accept-encoding" => "gzip, deflate, br",
        "accept-language" => "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
//        "accept-charset" => "utf-8, iso-8859-1;q=0.5",
        "cache-control" => "max-age=0",
        'host' => 'realty.yandex.ru',
//        "cookie" => "_ym_uid=1537949481163297486; _ym_d=1537949481; mda=0; yandexuid=1236251451513665219; i=kSGMEtHDz6607UwS4UgT8+bkWay6//5aJDzUZAVe+yi8jSwklG+/cl0uF7PgtBJFm/cCO4iG8+5czl2pkyvypmb70zc=; L=c2BhclxGVwpWTV1UClxPQQoFYHoBUwxVBRA8CAAIJgM1JQFzXg==.1537949731.13635.374527.164bf06630a5f337252287e58a68a53d; yp=1853162094.yrts.1537802094#1853309628.yrtsi.1537949628#1853309720.multib.1#1853309731.udn.cDpxd2VydHl1aW9wMzA0#1572681280.as.1#1542786884.ysl.1#1557345286.szm.1:1920x1080:1886x928#1544255695.csc.1; my=YwA=; offer_map_transport=pedestrian; _ym_isad=2; Session_id=3:1550734659.5.0.1537949731556:CNjlWw:43.1|438337212.0.2|195234.263678.dHYGOihTIkJt9c9BSH0F8NwXOHE; sessionid2=3:1550734659.5.0.1537949731556:CNjlWw:43.1|438337212.0.2|195234.588144.eHEAslncpun1JXFbleDLzf_9XdE; yandex_login=qwertyuiop304; _ym_visorc_2119876=b; rheftjdd=rheftjddVal; suid=b24a4c95160b05fc6dd5f8dc0bf443a0.d63c7107d5999fe12419dac1bc0e786e; from_lifetime=1550734922195; from=direct; X-Vertis-DC=sas",
        "cookie" => "yandexuid=5264932711552980521; device_id=\"b33877cf92bed5e6480fef48d7d84506e3aecf721\"; i=2hzhDGLeamkTI9mekB0X1M7MLMvrJ7FDxmM5FgAqhT/593bA+ADLvGCxM7hGeLizKElbObenvVLGTtV8rO45AGG1wqU=; yp=1868340525.yrtsi.1552980525; _ym_wasSynced=%7B%22time%22%3A1552980536331%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; _ym_uid=1552980536405265374; _ym_d=1552980536; mda=0; _ym_visorc_1028356=b; _ym_isad=2; suid=1af2299d6485adfa63f6e8b6cf40fe78.4e26f6dabe0e3691115d70c6cfcf612e; from=direct; X-Vertis-DC=sas; rheftjdd=rheftjddVal; _ym_visorc_2119876=w; from_lifetime=1552980533679; cycada=+oJoNMmNYfSFxTYQ0MuBA4gAhzj591skYl/ED8yeQDQ=",
        "upgrade-insecure-requests" => "1",
        'pragma' => 'no-cache',
        "user-agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.122 Safari/537.36 Vivaldi/2.3.1440.61",
    ];

    public static $_propertyFields = [
        "offerType",
        "offerCategory",
        "creationDate",
        "updateDate",
        "roomsTotal",
        "floorsTotal",
        "floorsOffered",
        "flatType",
        "area",
        "livingSpace",
        "kitchenSpace",
        "apartment",
    ];

    /**
     * @param $data
     * @param Client $client
     * @return array
     */
    public static function parseBuilding($data, $client){
        $dom = new DomDocument;
        $domData = $data;
//        $domData = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data);
        $dom->loadHTML($domData);
        $xpath = new DomXPath($dom);
        $jsonNodes = $xpath->query(XpathHelper::cssToXpath("//script#initial_state_script"));
        if ($jsonNodes->length) {
            $jsonDataPattern = "/[\s\S]*window\.INITIAL_STATE\s?=\s?({[^<>]*});[\s\S]*/";
            if (preg_match($jsonDataPattern, $jsonNodes->item(0)->textContent)) {
                $jsonData = Json::decode(preg_replace($jsonDataPattern, "$1", $jsonNodes->item(0)->textContent));
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
        $result = [
            'specials' => [],
            'propertyLinks' => [],
            'companyData' => [],
        ];

//        // Данные компании
        if (!empty($jsonData['cards']['sites']['developer']['id'])) {
            $result['companyData']['id'] = $jsonData['cards']['sites']['developer']['id'];
        }
        if (!empty($jsonData['cards']['sites']['developer']['logo'])) {
            $result['companyData']['logo'] = "https:" . $jsonData['cards']['sites']['developer']['logo'];
        }
        $result['companyData']['name'] = $jsonData['cards']['sites']['developer']['legalName'] ?? $jsonData['cards']['sites']['developer']['name'] ?? null;

        // Общие данные недвижимости
        if (!empty($jsonData['cards']['sites']['fullName'])) {
            $result['title'] = $jsonData['cards']['sites']['fullName'];
        }
        if (!empty($jsonData['cards']['sites']['description'])) {
            $result['description'] = $jsonData['cards']['sites']['description'];
        }
        if (!empty($jsonData['cards']['sites']['images']['list']) && is_array($jsonData['cards']['sites']['images']['list'])) {
            $images = [];
            foreach($jsonData['cards']['sites']['images']['list'] as $imageItem) {
                if (!empty($imageItem['appLarge'])){
                    $images[] = "https:" . $imageItem['appLarge'];
                }
            }
            $result['imagesData'] = $images;
        }
        if (!empty($jsonData['cards']['sites']['location']['point']['latitude']) && !empty($jsonData['cards']['sites']['location']['point']['longitude'])) {
            $result['lat'] = $jsonData['cards']['sites']['location']['point']['latitude'];
            $result['lng'] = $jsonData['cards']['sites']['location']['point']['longitude'];
        }

        // Атрибуты
        $attributeNodes = $xpath->query(XpathHelper::cssToXpath("//div.SiteCardDescription/div.CardFeatures/div.CardFeatures__item"));
        if ($attributeNodes->length) {
            $items = [];
            foreach($attributeNodes as $key => $attributeNode) {
                /* @var DOMElement $attributeNode*/
                $children = $attributeNode->getElementsByTagName('div');
                if ($children->length === 2) {
                    $items[trim($children->item(1)->textContent)] = preg_replace('/\s+/', ' ', trim($children->item(0)->textContent));
                }
            }
            $result['attributes'] = $items;
        }

        // Документы
        if (!empty($jsonData['cards']['sites']['documents']) && is_array($jsonData['cards']['sites']['documents'])) {
            $result['documentsData'] = $jsonData['cards']['sites']['documents'];
        }

        // Ход Строительства
        if (!empty($jsonData['cards']['sites']['construction']) && is_array($jsonData['cards']['sites']['construction'])) {
            $lastPeriod = array_pop($jsonData['cards']['sites']['construction']);
            $lastPeriod['photos'] = array_filter(
                array_map(function($var){
                    return !empty($var['full']) ? ("https:" . preg_replace("/(.*?)\/large/", '$1/app_large', $var['full'])) : null;
                }, $lastPeriod['photos'])
            );
            $result['constructionData'] = [$lastPeriod];
        }

        // Даты сдачи
        if (!empty($jsonData['cards']['sites']['deliveryDates']) && is_array($jsonData['cards']['sites']['deliveryDates'])) {
            $result['deliveryDates'] = array_filter(array_map(function($var){
                if (empty($var['year']) || empty($var['quarter'])){
                    return null;
                }
                return [
                    'year' => $var['year'] ?? null,
                    'quarter' => $var['quarter'] ?? null,
                    'state' => $var['buildingState'] ?? null,
                    'housesInfo' => array_map(function($h){
                        return [
                            'id' => $h['id'] ?? null,
                            'maxFloor' => $h['maxFloor'] ?? null,
                            'lat' => !empty($h['polygon']['latitudes']) ? array_sum($h['polygon']['latitudes']) / count($h['polygon']['latitudes']) : null,
                            'lng' => !empty($h['polygon']['longitudes']) ? array_sum($h['polygon']['longitudes']) / count($h['polygon']['longitudes']) : null,
                        ];
                    }, $var['housesInfo']),
                ];
            }, $jsonData['cards']['sites']['deliveryDates']));
        }

        // Акции
        if (!empty($jsonData['cards']['sites']['mainSiteSpecialProposal']) && is_array($jsonData['cards']['sites']['mainSiteSpecialProposal'])) {
            $result['specials'][] = $jsonData['cards']['sites']['mainSiteSpecialProposal'];
        }
        if (!empty($jsonData['cards']['sites']['siteSpecialProposals']) && is_array($jsonData['cards']['sites']['siteSpecialProposals'])) {
            $result['specials'] = array_merge($result['specials'], $jsonData['cards']['sites']['siteSpecialProposals']);
        }

        // Достопримечательности
        foreach (["schools", "parks", "ponds", "airports", "cityCenter"] as $siteType) {
            if (!empty($jsonData['cards']['sites']['location'][$siteType]) && is_array($jsonData['cards']['sites']['location'][$siteType])) {
                foreach ($jsonData['cards']['sites']['location'][$siteType] as $site) {
                    $timeDistance = !empty($site['timeDistanceList']) ? array_values($site['timeDistanceList'])[0] : null;
                    $result['sitesData'][] = [
                        'type' => $siteType,
                        'name' => $site['name'] ?? null,
                        'lat' => $site['latitude'] ?? null,
                        'lng' => $site['longitude'] ?? null,
                        'transport' => $timeDistance['transport'] ?? null,
                        'time' => $timeDistance['time'] ?? null,
                        'distance' => $timeDistance['distance'] ?? null,
                    ];
                }
            }
        }
        if (!empty($jsonData['cards']['sites']['location']['metroList']) && is_array($jsonData['cards']['sites']['location']['metroList'])) {
            foreach ($jsonData['cards']['sites']['location']['metroList'] as $site) {
                $result['sitesData'][] = [
                    'type' => 'metro',
                    'name' => $site['name'] ?? null,
                    'transport' => $site['metroTransport'] ?? null,
                    'time' => $site['timeToMetro'] ?? null,
                ];
            }
        }

        // Ссылки на квартиры
        if (!empty($jsonData['cards']['sites']['id'])) {
            foreach (self::$_rooms as $roomParam) {
//                Console::output("......... loading links rooms - $roomParam");
                $page = 0;
                while(true) {
//                    Console::output("............ page - $page");
                    $success = false;
                    $propertiesData = $client->get("https://realty.yandex.ru/gate/site-offers/getOffers/", [
                        'siteId' => $jsonData['cards']['sites']['id'],
                        'roomsTotal' => $roomParam,
                        'page' => $page
                    ], self::$_headers)->send();
                    if ($propertiesData->isOk && !empty($propertiesData->content)) {
                        try {
                            $propertiesData = Json::decode($propertiesData->content);
                        }
                        catch (Exception $e) {
                            return null;
                        }
                        if (!empty($propertiesData['response']['items']) && is_array($propertiesData['response']['items'])) {
                            $links = array_filter(array_map(function($var){
                                return !empty($var['id']) ? "https://realty.yandex.ru/offer/{$var['id']}" : null;
                            }, $propertiesData['response']['items']));
                            if (!empty($links)) {
                                $result['propertyLinks'] = array_merge($result['propertyLinks'], $links);
                                $success = true;
                            }
                        }
                    }
                    if ($success !== true) {
                        break;
                    }
                    $page++;
                }
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @param $buildingData
     * @return array
     */
    public static function parseProperty($data, $buildingData){
        $dom = new DomDocument;
        $domData = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data);
        $dom->loadHTML($domData);
        $xpath = new DomXPath($dom);

        $reactNode = $xpath->query(XpathHelper::cssToXpath("//div.i-react-state"));
        if ($reactNode->length) {
            $bem = $reactNode->item(0)->getAttribute('data-bem');
            if (!empty($bem)) {
                $jsonData = Json::decode($bem);
            }
        }

        $result = [];

//        // Данные компании
        $result['companyData'] = $buildingData['companyData'] ?? [];
        $offerData = $jsonData['i-react-state']['state']['cards']['offers'] ?? [];

        // Общие данные недвижимости
        foreach (self::$_propertyFields as $field) {
            if (!empty($offerData[$field])) {
                $result[$field] = $offerData[$field];
            }
        }
        if (!empty($offerData['price']['value']) && !empty($offerData['price']['currency'])) {
            $result['price'] = [
                'value' => $offerData['price']['value'],
                'currency' => $offerData['price']['currency'],
            ];
        }
        if (!empty($offerData['appLargeImages']) && is_array($offerData['appLargeImages'])) {
            $result['imagesData'] = array_map(function($var){return "https:" . $var;}, $offerData['appLargeImages']);
        }
        $result['lat'] = $buildingData['lat'] ?? null;
        $result['lng'] = $buildingData['lng'] ?? null;

        return $result;
    }
}