<?php

namespace console\helpers;

use common\helpers\XpathHelper;
use DOMDocument;
use DOMElement;
use DOMXPath;
use yii\helpers\Json;
use yii\httpclient\Client;

class LuximmoHelper
{
    public static $_propertyActions = [
        'sell' => ['sadx' => 1],
        'rent' => ['sady' => 2],
    ];

    public static $_propertyCategories = [
        'flats' => [
            'mstip' => [10, 11, 12, 13, 49, 14, 15, 50, 24]
        ],
        'houses' => [
            'mstip' => [41, 19, 43, 46, 51, 52, 20, 34, 55, 53, 54]
        ],
        'land' => [
            'mstip' => [22, 28, 33, 35]
        ],
        'commercial-property' => [
            'mstip' => [2, 3, 16, 30, 17, 18, 27, 45, 44, 29, 32, 48, 42, 47, 36]
        ],
    ];
    public static $_countries = [
        20 => 'Australia',
        16 => 'Austria',
        1 => 'Bulgaria',
        6 => 'Czech Republic',
        30 => 'Dominican Republic',
        8 => 'Germany',
        2 => 'Greece',
        9 => 'Italy',
        10 => 'Montenegro',
        27 => 'Slovenia',
        11 => 'Spain',
        14 => 'Turkey',
        4 => 'United Arab Emirates',
        15 => 'United Kingdom',
        19 => 'USA',
    ];

    public static $_headers = [
        "scheme" => "https",
        "accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
//        "accept-encoding" => "gzip, deflate, br",
        "accept-language" => "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "accept-charset" => "utf-8, iso-8859-1;q=0.5",
        "cache-control" => "max-age=0",
        'host' => 'www.luximmo.com',
//        "cookie" => "PHPSESSID=ibverbljt517lda79vb85823n2; luximo=1599191680; _ym_uid=1548915173133148872; _ym_d=1548915173; cb-enabled=accepted; _ym_isad=2; _ga=GA1.2.1051970831.1548915174; _gid=GA1.2.1785108276.1549433240; __zlcmid=qdhyx17k3CAyht; _fbp=fb.1.1549433240849.321033728; def_currency_v=EUR; reklamaID=9",
        "cookie" => "PHPSESSID=ibverbljt517lda79vb85823n2; luximo=1599191680; _ym_uid=1548915173133148872; _ym_d=1548915173; cb-enabled=accepted; _ym_isad=2; def_currency_v=EUR; reklamaID=10; _ym_visorc_29783364=w; _ga=GA1.2.1051970831.1548915174; _gid=GA1.2.1785108276.1549433240; _dc_gtm_UA-2085903-5=1; _fbp=fb.1.1549433240849.321033728; __zlcmid=qdhyx17k3CAyht",
        "upgrade-insecure-requests" => "1",
        'pragma' => 'no-cache',
        "user-agent" => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.97 Safari/537.36 Vivaldi/1.94.1008.36",
    ];

    /**
     * @param $data
     * @param Client $client
     * @return array
     */
    public static function parseItem($data, $client, $itemUrl){
        $dom = new DomDocument;
        $domData = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data);
        $dom->loadHTML($domData);
        $xpath = new DomXPath($dom);
        $result = [
            'price' => null,
            'description' => [],
            'userData' => [
                'name' => '',
                'avatar' => null,
                'phone' => null,
                'email' => null,
            ],
//            'companyData' => [
//                'name' => '',
//                'phone' => null,
//                'email' => null,
//            ],
        ];

        // Данные автора
        $userNode = $xpath->query(XpathHelper::cssToXpath('//div.grid-container//div.content-right/div.panel.feedback.light/div.grid-x/span.cell/b.font-large'));
        if ($userNode->length) {
            $result['userData']['name'] = trim($userNode->item(0)->textContent);
        }
        $avatarNode = $xpath->query(XpathHelper::cssToXpath('//div.grid-container//div.content-right/div.panel.feedback.light/div.grid-x/span.cell/i.avatar'));
        if ($avatarNode->length) {
            $result['userData']['avatar'] = preg_replace("/[\s\S]*\[([^\[\],]*),[\s\S]*\][\s\S]*/", "$1", $avatarNode->item(0)->getAttribute('data-interchange'));
        }
        $phoneNode = $xpath->query(XpathHelper::cssToXpath('//div.grid-container//div.content-right/div.panel.feedback.light/ul.widget-contacts/li//span[@data-broker-type="phone"]'));
        if ($phoneNode->length) {
            $phoneData = $client->post("https://www.luximmo.com/get_broker_info.php", [
                'ID' => $phoneNode->item(0)->getAttribute('data-broker-id'),
                'type' => 'phone',
                'IID' => $phoneNode->item(0)->getAttribute('data-prop-id'),
                'url' => $itemUrl
            ], self::$_headers)->send();
            if ($phoneData->isOk && !empty($phoneData->content)) {
                $result['userData']['phone'] = "+" . preg_replace("/[^0-9]/", "", mb_convert_encoding($phoneData->content, 'utf-8', 'windows-1251'));
            }
        }
        $emailNode = $xpath->query(XpathHelper::cssToXpath('//div.grid-container//div.content-right/div.panel.feedback.light/ul.widget-contacts/li/span#email_wrapp'));
        if ($emailNode->length) {
            $result['userData']['email'] = preg_replace("/\.\./", "", $emailNode->item(0)->firstChild->textContent) . "@luximmo.com";
        }

//        // Данные компании
//        $companyNameNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-contacts__info//a.c-contacts__company-name'));
//        if ($companyNameNode->length) {
//            $result['companyData']['name'] = trim($companyNameNode->item(0)->textContent);
//            $result['companyData']['url'] = "https:" . $companyNameNode->item(0)->getAttribute('href');
//        }
//        $companyPhoneNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/a[contains(@href,"tel:")]'));
//        if ($companyPhoneNode->length) {
//            $result['companyData']['phone'] = "+" . preg_replace("/[^0-9]/", "", $companyPhoneNode->item(0)->getAttribute('href'));
//        }

        // Общие данные недвижимости
        $priceNode = $xpath->query(XpathHelper::cssToXpath("//div.grid-container/div.grid-x/div.large-cell-block-y/div.grid-y/div.cell.large-shrink/div.panel.light.mute/div.font-xlarge/big"));
        if ($priceNode->length) {
            $result['price'] = preg_replace("/[^0-9]/", "", $priceNode->item(0)->textContent);
        }
        $descriptionNodes = $xpath->query(XpathHelper::cssToXpath('//div.grid-container/div/p'));
        if ($descriptionNodes->length) {
            foreach ($descriptionNodes as $descriptionNode) {
                $descriptionPart = trim(strip_tags(preg_replace("#<table(.*?)>(.*?)</table>#is", '', $dom->saveHTML($descriptionNode)), '<p><ul><ol><li><br>'));
                $result['description']['en-GB'] = ($result['description']['en-GB'] ?? '') . $descriptionPart;
            }
        }
        $imagesPattern = "/[\s\S]*script[\s\S]*galleria_data[\s\S]*var data =\s?(\[[^\[\]]*\]);[\s\S]*script[\s\S]*/";
        if (preg_match($imagesPattern, $data)) {
            $imageData = preg_replace($imagesPattern, "$1", $data);
            if (!empty($imageData)) {
                $imageData = Json::decode($imageData);
                $images = [];
                foreach($imageData as $imageItem) {
                    if (!empty($imageItem['big'])){
                        $images[] = $imageItem['big'];
                    }
                }
                $result['imagesData'] = $images;
            }
        }
        $locationPattern = "/[\s\S]*" . preg_quote('https://www.google.com/maps/embed/v1/place?key=AIzaSyCfJlEKySsimvIalq_rPN0ZkjW2bpXlP1o&amp;q=', '/') . "(\d*\.\d*,\d*\.\d*)[\s\S]*/";
        if (preg_match($locationPattern, $data)) {
            $latlng = preg_replace("/[\s\S]*" . preg_quote('https://www.google.com/maps/embed/v1/place?key=AIzaSyCfJlEKySsimvIalq_rPN0ZkjW2bpXlP1o&amp;q=', '/') . "(\d*\.\d*,\d*\.\d*)[\s\S]*/", "$1", $data);
            if (!empty($latlng)){
                $latlng = explode(',', $latlng);
                if (count($latlng) === 2) {
                    $result['lat'] = $latlng[0];
                    $result['lng'] = $latlng[1];
                }
            }
        }

        // Аттрибуты недвижимости
        $attributeNodes = $xpath->query(XpathHelper::cssToXpath("//div.grid-container/div.grid-x/div.large-cell-block-y/div.grid-y/div.cell.large-auto/div.panel.light.mute/div.grid-x"));
        if ($attributeNodes->length) {
            $items = [];
            foreach($attributeNodes as $key => $attributeNode) {
                /* @var DOMElement $attributeNode*/
                $children = $attributeNode->getElementsByTagName('div');
                if ($children->length === 2) {
                    $items[trim($children->item(0)->textContent)] = preg_replace('/\s+/', ' ', trim($children->item(1)->textContent));
                }
            }
            $result['propertyData'] = $items;
        }
        $attributeNodes2 = $xpath->query(XpathHelper::cssToXpath("//div#content/div.grid-section/section.content-block//div.grid-container/div.grid-x/div.cell/div.grid-x"));
        if ($attributeNodes2->length) {
            $items = [];
            foreach($attributeNodes2 as $key => $attributeNode) {
                /* @var DOMElement $attributeNode*/
                $children = $attributeNode->getElementsByTagName('div');
                if ($children->length === 2) {
                    $items[trim($children->item(0)->textContent)] = preg_replace('/\s+/', ' ', trim($children->item(1)->textContent));
                }
            }
            $result['propertyData'] = array_merge($result['propertyData'], $items);
        }

//        $amenityGroupNodes = $xpath->query(XpathHelper::cssToXpath("//div.c-block_infrastr/div.c-block__content/div.c-characters"));
//        if ($amenityGroupNodes->length) {
//            foreach($amenityGroupNodes as $key => $amenityGroupNode) {
//                /* @var DOMElement $amenityGroupNode*/
//                $amenityNodes = $amenityGroupNode->getElementsByTagName('div');
//                $amenities = [];
//                foreach($amenityNodes as $amenityNode) {
//                    /* @var DOMElement $amenityNode*/
//                    $class = $amenityNode->getAttribute('class');
//                    $content = $amenityNode->textContent;
//                    if (preg_match("/c-characters__title/", $class)) {
//                        if ($content === 'Инфраструктура и оснащение дома или жилого комплекса') {
//                            $group = 'buildingData';
//                        }
//                        else {
//                            $group = 'propertyData';
//                        }
//                    }
//                    else {
//                        $amenities[] = preg_replace('/\s+/', ' ', trim($content));
//                    }
//                }
//                if (!empty($group)) {
//                    $result[$group]['amenities'] = array_merge($result[$group]['amenities'] ?? [], $amenities);
//                }
//            }
//        }
//        $newBuildingNode = $xpath->query(XpathHelper::cssToXpath("//div.c-block__content/div.c-block__div_diff/div.c-addparams/p/a/b"));
//        $result['propertyData']['newBuilding'] = $newBuildingNode->length > 0 && $newBuildingNode->item(0)->textContent === 'Новый дом';

        return $result;
    }
}