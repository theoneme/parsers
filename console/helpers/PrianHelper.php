<?php

namespace console\helpers;

use common\helpers\XpathHelper;
use DOMDocument;
use DOMElement;
use DOMXPath;

class PrianHelper
{
    public static $_propertyActions = [
        'buy',
        'rent',
    ];

    public static $_propertyCategories = [
        'apartments',
        'houses',
        'commercial_property',
        'land'
    ];

    public static $_headers = [
        ":authority" => "prian.ru",
        ":method" => "GET",
        ":path" => "/",
        "scheme" => "https",
        "accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "accept-encoding" => "gzip, deflate, br",
        "accept-language" => "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control" => "max-age=0",
        "cookie" => "_ym_uid=153958832929156330; _ym_d=1539588329; __gads=ID=6a7dca00e2e40b01:T=1539588321:S=ALNI_MbjX7YmDSxOtsbSw3G2mEHdEsHeiw; PHPSESSID=t3v5bo64pifij3ircfps8hhs00; _ym_wasSynced=%7B%22time%22%3A1543211932462%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; _ym_isad=2; saved_time_counter=120; go_to_detail=2678320%2C2694850%2C2695295%2C2967704%2C2970318%2C2970315%2C2511941%2C2751522; saving_searches=1; geoip_country_code=UA; check_cookie_eu=0; search_view=2; request_error_type=none; request_form_send=yes; _ym_visorc_230430=w; _dc_gtm_UA-12168497-2=1; tmr_detect=0%7C1543231889216; _ga=GA1.2.395684860.1539588329; _gid=GA1.2.266964726.1543211933; _gat_UA-12168497-2=1",
//        "dnt" => "1",
//        "referer" => "https://prian.ru/spain/valencia-and-murcia/benitachell/apartments/",
        "upgrade-insecure-requests" => "1",
        "user-agent" => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.97 Safari/537.36 Vivaldi/1.94.1008.36",
    ];

    /**
     * @param $data
     * @return array
     */
    public static function parseItem($data){
        $dom = new DomDocument;
        $domData = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data);
        $dom->loadHTML($domData);
        $xpath = new DomXPath($dom);
        $result = [
            'price' => null,
            'description' => [],
            'userData' => [
                'name' => '',
                'avatar' => null,
                'phone' => null,
                'email' => null,
            ],
            'companyData' => [
                'name' => '',
                'phone' => null,
                'email' => null,
            ],
        ];

        // Данные автора
        $userNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/div.c-contacts__author/div.c-contacts__capture/span.c-contacts__name'));
        if ($userNode->length) {
            $result['userData']['name'] = trim($userNode->item(0)->textContent);
        }
        $avatarNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/div.c-contacts__author/img.c-contacts__avatar'));
        if ($avatarNode->length) {
            $result['userData']['avatar'] = "https:" . $avatarNode->item(0)->getAttribute('src');
        }
        $phoneNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/div.c-contacts__socials/div/a.c-contacts__social[contains(@href,"tel:")]'));
        if ($phoneNode->length) {
            $result['userData']['phone'] = "+" . preg_replace("/[^0-9]/", "", $phoneNode->item(0)->getAttribute('href'));
        }
        $emailNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/div.c-contacts__socials/div/a.c-contacts__social[contains(@href,"mailto:")]'));
        if ($emailNode->length) {
            $result['userData']['email'] = preg_replace("/^mailto:/", "", $emailNode->item(0)->getAttribute('href'));
        }

        // Данные компании
        $companyNameNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-contacts__info//a.c-contacts__company-name'));
        if ($companyNameNode->length) {
            $result['companyData']['name'] = trim($companyNameNode->item(0)->textContent);
            $result['companyData']['url'] = "https:" . $companyNameNode->item(0)->getAttribute('href');
        }
        $companyPhoneNode = $xpath->query(XpathHelper::cssToXpath('//div.c-block_contacts/div.c-block__content/div.c-block__div_third/div.c-block__div/div.c-contacts__info/a[contains(@href,"tel:")]'));
        if ($companyPhoneNode->length) {
            $result['companyData']['phone'] = "+" . preg_replace("/[^0-9]/", "", $companyPhoneNode->item(0)->getAttribute('href'));
        }

        // Общие данные недвижимости
        $priceNode = $xpath->query(XpathHelper::cssToXpath("//div.c-header__container/p.c-header__price"));
        if ($priceNode->length) {
            $result['price'] = preg_replace("/[^0-9]/", "", $priceNode->item(0)->firstChild->textContent);
        }
        $descriptionNodes = $xpath->query(XpathHelper::cssToXpath('//div.c-block_description/div.c-block__content.c-description/div.translate_description'));
        if ($descriptionNodes->length) {
            foreach ($descriptionNodes as $descriptionNode) {
                $description = trim(strip_tags(preg_replace("#<table(.*?)>(.*?)</table>#is", '', $dom->saveHTML($descriptionNode)), '<p><ul><ol><li><br>'));
                $latinLetters = mb_strlen(preg_replace("/[^a-zA-Z]/", "", $description));
                $cyrilLetters = mb_strlen(preg_replace("/[^а-яА-ЯёЁ]/", "", $description));
                $locale = $latinLetters > $cyrilLetters ? 'en-GB' : 'ru-RU';
                $result['description'][$locale] = $description;
            }
        }
        $imageNodes = $xpath->query(XpathHelper::cssToXpath("//div.c-slider.c-slider_main/div.c-slider__inner/div.c-slider__item/a/img"));
        if ($imageNodes->length) {
            $images = [];
            foreach($imageNodes as $imageNode) {
                /* @var DOMElement $imageNode*/
                $src = $imageNode->getAttribute('src');
                if (!empty($src)){
                    $images[] = "https:" . $src;
                }
            }
            $result['imagesData'] = $images;
        }
        $latPattern = "/[\s\S]*var lat = parseFloat\(\"([^()]*)\"\);[\s\S]*/";
        if (preg_match($latPattern, $data)) {
            $result['lat'] = preg_replace($latPattern, "$1", $data);
        }
        $lngPattern = "/[\s\S]*var lat = parseFloat\(\"([^()]*)\"\);[\s\S]*/";
        if (preg_match($lngPattern, $data)) {
            $result['lng'] = preg_replace($lngPattern, "$1", $data);
        }

        // Аттрибуты недвижимости
        $attributeNodes = $xpath->query(XpathHelper::cssToXpath("//div.c-block__content/div.c-block__div_diff/table.c-params_commerc/tr.c-params__row"));
        if ($attributeNodes->length) {
            $items = [];
            foreach($attributeNodes as $key => $attributeNode) {
                /* @var DOMElement $attributeNode*/
                $children = $attributeNode->getElementsByTagName('td');
                if ($children->length === 2) {
                    $items[trim($children->item(0)->textContent)] = preg_replace('/\s+/', ' ', trim($children->item(1)->textContent));
                }
            }
            $result['propertyData'] = $items;
        }
        $amenityGroupNodes = $xpath->query(XpathHelper::cssToXpath("//div.c-block_infrastr/div.c-block__content/div.c-characters"));
        if ($amenityGroupNodes->length) {
            foreach($amenityGroupNodes as $key => $amenityGroupNode) {
                /* @var DOMElement $amenityGroupNode*/
                $amenityNodes = $amenityGroupNode->getElementsByTagName('div');
                $amenities = [];
                foreach($amenityNodes as $amenityNode) {
                    /* @var DOMElement $amenityNode*/
                    $class = $amenityNode->getAttribute('class');
                    $content = $amenityNode->textContent;
                    if (preg_match("/c-characters__title/", $class)) {
                        if ($content === 'Инфраструктура и оснащение дома или жилого комплекса') {
                            $group = 'buildingData';
                        }
                        else {
                            $group = 'propertyData';
                        }
                    }
                    else {
                        $amenities[] = preg_replace('/\s+/', ' ', trim($content));
                    }
                }
                if (!empty($group)) {
                    $result[$group]['amenities'] = array_merge($result[$group]['amenities'] ?? [], $amenities);
                }
            }
        }
        $newBuildingNode = $xpath->query(XpathHelper::cssToXpath("//div.c-block__content/div.c-block__div_diff/div.c-addparams/p/a/b"));
        $result['propertyData']['newBuilding'] = $newBuildingNode->length > 0 && $newBuildingNode->item(0)->textContent === 'Новый дом';

        return $result;
    }
}