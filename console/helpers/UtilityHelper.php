<?php

namespace console\helpers;

use Yii;
use yii\console\Application;

class UtilityHelper
{
    /**
     * @param $dir
     * @return bool
     * @throws \RuntimeException
     */
    public static function makeDir($dir)
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
            if (Yii::$app instanceof Application) {
                chown($dir, 'www-data');
                chgrp($dir, 'www-data');
            }
            return true;
        }
        return false;
    }
}