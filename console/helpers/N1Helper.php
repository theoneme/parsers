<?php

namespace console\helpers;

use common\helpers\XpathHelper;
use DOMDocument;
use DOMElement;
use DOMXPath;

class N1Helper
{
    public static $_propertyActions = [
        'kupit',
        'snyat',
    ];

    public static $_propertyCategories = [
        'kvartiry',
        'doma',
        'kommercheskaya',
        'dachi',
        'zemplya',
    ];

    public static $_headers = [
        "User-Agent" => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.97 Safari/537.36 Vivaldi/1.94.1008.36",
        "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding" => "gzip, deflate, br",
        "Accept-Language" => "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "Cache-Control" => "max-age=0",
        "Cookie" => "n1_cookie_cleanup=2; n1_uid=rBCFKVt6XWcANlrQAAMkYQ%3D%3D; _ym_uid=1534745966537828333; _ym_d=1534745966; owlUid=c9aefc03-99ad-4797-9422-1a47a08dda2a; ngs_uid=w127CVt6XWdpvkWhBJ6MAg==; locationSelectActiveTab=cities; n1_last_location=%7B%22id%22%3A42968%2C%22type%22%3A%22city%22%7D; new_site_features_showed=yes; site_features_closed_2=yes; __gads=ID=d925200810d4915e:T=1535365468:S=ALNI_Mb_LdEzqRplzYZWHaMv4NIIy9Q3Tg; current_user_roles_0=%7B%22user_type%22%3A%22consumer%22%2C%22user_role%22%3A%7B%22old_user%22%3A%227-30%20days%22%7D%7D; scarab.profile=%2225946552%7C1535527095%22; isLeadMagnetBlocked=true; card_map_buttons=%5B%22school%22%2C%22hospital%22%2C%22cafe%22%2C%22similar%22%5D; OFFERS_LIST_VIEW_TYPE=table; MapDrawTipShowed=true; LAST_OFFERS_RUBRIC_TYPE=living; LAST_OFFERS_DEAL_TYPE=sell; lastVisit=%7B%22last%22%3A1535527527%2C%22check%22%3A1535527527%2C%22version%22%3A1%7D; scarab.mayAdd=%5B%7B%22i%22%3A%2225946552%22%7D%2C%7B%22i%22%3A%2226472038%22%7D%2C%7B%22i%22%3A%2226990829%22%7D%5D; scarab.visitor=%223CB24E340A0F7C7A%22; _ga=GA1.1.249484517.1534745967; _ym_isad=2; _gaexp=GAX1.2.aSxzS0McRfexNiiPKMG2ow.17838.0!KxQ5xdMrQduLqzzfRBUkZQ.17852.1; CLASSIFIED-3702-house-number-search=blur street; owlSid=47a6b19d-3966-482b-9dc7-c3a3381730c5%3A1537428566; main_slider=video; _ga=GA1.2.249484517.1534745967; _gid=GA1.2.937266357.1535527101; _ga_cid=249484517.1534745967; _ym_visorc_25608281=w; ngs_avc=2; tmr_detect=0%7C1535629848016",
        "DNT" => 1,
        "Upgrade-Insecure-Requests" => 1,
        ":method" => "GET",
        ":scheme" => "https",
    ];

    /**
     * @param $data
     * @param $selector
     * @return array
     */
    public static function parseItem($data, $selector){
        $dom = new DomDocument;
        $dom->loadHTML($data);
        $xpath = new DomXPath($dom);
        $result = [];
        $price = null;
        $description = '';
        $name = '';
        $phone = null;

        $userNode = $xpath->query(XpathHelper::cssToXpath('//address.offer-card-contacts/div.offer-card-contacts__wrapper/div.offer-card-contacts__block/div.offer-card-contacts__person/*.offer-card-contacts__owner-name'));
        if ($userNode->length) {
            $name = $userNode->item(0)->textContent;
        }
        else {
            $userNode = $xpath->query(XpathHelper::cssToXpath('//address.offer-card-contacts/div.offer-card-contacts__wrapper/div.offer-card-contacts__block/div.offer-card-contacts__person/*.offer-card-contacts__agency-owner-name'));
            if ($userNode->length) {
                $name = $userNode->item(0)->textContent;
            }
        }
        $phoneNode = $xpath->query(XpathHelper::cssToXpath('//address.offer-card-contacts/div.offer-card-contacts__wrapper/div.offer-card-contacts__block//li.offer-card-contacts-phones__item/a.offer-card-contacts-phones__phone'));
        if ($phoneNode->length) {
            $phone = "+" . preg_replace("/[^0-9]/", "", $phoneNode->item(0)->getAttribute('href'));
        }

        $priceNode = $xpath->query(XpathHelper::cssToXpath("//div.card-price-and-statistics/div.price/span.price__val"));
        if ($priceNode->length) {
            $price = $priceNode->item(0)->textContent;
        }

        $descriptionNode = $xpath->query(XpathHelper::cssToXpath("//div.card-$selector-content__info/div.card-$selector-content__description/div.text[2]"));
        if ($descriptionNode->length) {
            $description = $descriptionNode->item(0)->textContent;
        }

        $flatNodes = $xpath->query(XpathHelper::cssToXpath("//div.card-$selector-content-params/div.card-$selector-content-params__col._first/ul.card-$selector-content-params-list/li.card-$selector-content-params-list__item"));
        if ($flatNodes->length) {
            $items = [];
            foreach($flatNodes as $key => $flatNode) {
                /* @var DOMElement $flatNode*/
                $children = $flatNode->getElementsByTagName('span');
                if ($children->length === 2) {
                    $items[$children->item(0)->textContent] = $children->item(1)->textContent;
                }
            }
            $result['flatData'] = $items;
        }

        $newBuildingNode = $xpath->query(XpathHelper::cssToXpath("//div.card-$selector-content-header/div.card-$selector-content-header__wrapper/h1.card-$selector-content-header__title._is-new-building"));

        $result['flatData']['newBuilding'] = $newBuildingNode->length > 0;

        $houseNodes = $xpath->query(XpathHelper::cssToXpath("//div.card-$selector-content-params/div.card-$selector-content-params__col._last/ul.card-$selector-content-params-list/li.card-$selector-content-params-list__item"));
        if ($houseNodes->length) {
            $items = [];
            foreach($houseNodes as $key => $houseNode) {
                /* @var DOMElement $houseNode*/
                $children = $houseNode->getElementsByTagName('span');
                if ($children->length === 2) {
                    $items[$children->item(0)->textContent] = $children->item(1)->textContent;
                }
            }
            $result['houseData'] = $items;
        }

        $imageNodes = $xpath->query("//meta[@property=\"og:image\"]");

        if ($imageNodes->length) {
            $items = [];
            foreach($imageNodes as $imageNode) {
                /* @var DOMElement $imageNode*/
                $src = $imageNode->getAttribute('content');
                if (!empty($src)){
                    $items[] = $src;
                }
            }
            $result['imagesData'] = $items;
        }

        $breadcrumbNodes = $xpath->query(XpathHelper::cssToXpath("//nav.card-$selector-content__breadcrumbs/div.breadcrumbs/ul.breadcrumbs-list/li.breadcrumbs-list__item/a.ui-kit-link"));
        if ($breadcrumbNodes->length) {
            $pattern = "/.*\/type-([^\/]*)\/.*/";
            foreach($breadcrumbNodes as $breadcrumbNode) {
                /* @var DOMElement $breadcrumbNode*/
                if (preg_match($pattern, $breadcrumbNode->getAttribute('href'))) {
                    $result['subcategory'] = preg_replace($pattern, "$1", $breadcrumbNode->getAttribute('href'));
                    break;
                }
            }
        }

        $result['description'] = $description;
        $result['price'] = $price;
        $result['userData']['name'] = $name;
        $result['userData']['phone'] = $phone;
        return $result;
    }

    /**
     * @param $category
     * @return string
     */
    public static function getSelector($category){
        switch ($category) {
            case 'kommercheskaya':
                return 'commercial';
                break;
            case 'kvartiry':
            case 'doma':
                return 'living';
                break;
            case 'dachi':
                return 'dacha';
                break;
            case 'zemplya':
                return 'land';
                break;
            default:
                return '';
        }
    }
}